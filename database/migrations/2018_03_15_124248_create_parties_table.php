<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('party_id')->default(0);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('bussiness_name')->nullable();
            $table->string('unit')->nullable();
            $table->string('street_number')->nullable();
            $table->string('street_name')->nullable(); 
            $table->string('lot')->nullable();
            $table->string('post_code')->nullable();
            $table->string('state')->nullable();   
            $table->string('suburb')->nullable();
            $table->string('phone_home')->nullable();
            $table->string('phone_mobile')->nullable(); 
            $table->string('email')->nullable();
            $table->string('image')->nullable();
            $table->string('type')->nullable();
            $table->string('property_type')->nullable();
            $table->enum('single_multi',array('single','multi'))->nullable();
            $table->string('number_of_premises')->nullable();
            $table->string('number_of_units')->nullable();
            $table->string('nmi')->nullable();
            $table->string('meter_number')->nullable();
            $table->string('electrical_retailer')->nullable();
            $table->string('electrical_distributer')->nullable();
            $table->string('inverter_service')->nullable();
            $table->string('electricity_supply')->nullable();
            $table->string('seperate_inverter')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parties');
    }
}
