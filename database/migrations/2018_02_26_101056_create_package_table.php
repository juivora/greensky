<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('price');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('package_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id');
            $table->integer('product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
        Schema::dropIfExists('package_product');
    }
}
/*
ALTER TABLE `package_product` ADD `quantity` INT NULL DEFAULT '1' AFTER `updated_at`;
ALTER TABLE `package_product` ADD `price` FLOAT(8,2) NULL DEFAULT '0.00' AFTER `quantity`;
*/