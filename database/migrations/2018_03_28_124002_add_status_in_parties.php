<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusInParties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parties', function (Blueprint $table) {
            //
            $table->string('status')->nullable();  //[0 => 'CRM (from our side)', 1 => 'Web Curl call (API)', 2 => 'Wordpress Website' , 3 => 'iPhone']
            $table->string('customer_status')->nullable()->default(0); //[ 0 => 'Lead' , 1 => 'Customer', 2 => 'Prospect', 3 => 'LOST']
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parties', function (Blueprint $table) {
            //
            $table->dropColumn('status');
            $table->dropColumn('customer_status');
        });
    }
}
