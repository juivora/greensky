<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->text('kw')->nullable();     
            $table->float('display_kw')->nullable();
            $table->string('number_of_phase')->nullable();  
            $table->string('image')->nullable();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}


/*
#ALTER TABLE `packages` ADD `number_of_phase` VARCHAR(191) NULL DEFAULT NULL AFTER `display_kw`;

#ALTER TABLE `packages` ADD `image` VARCHAR(191) NULL DEFAULT NULL AFTER `number_of_phase`;
*/