<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            //
            $table->enum('type', array('panel','solar_inverter','battery_inverter','battery'))->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('series')->nullable();
            $table->float('rated_power_output')->default(0)->comments('in kW');
            $table->float('ac_power_rating')->default(0);
            $table->float('solar_dc_rating')->default(0);
            $table->float('battery_rated_storage_capacity')->default(0);
            $table->enum('battery_type', array('lead_acid','lithium_ion','flow','salt_water'))->nullable();
            $table->enum('approved_inverter', array('yes','no'))->nullable();
            $table->string('export_limit')->nullable();
            $table->enum('sop3306_compliant', array('yes','no'))->nullable();
            $table->enum('as4777_compliant', array('yes','no'))->nullable();
            $table->enum('capacity', array('phase_A','phase_B','phase_C'))->nullable();
            $table->string('certifying_authority_certificate_number')->nullable();
            $table->enum('inverter_phase', array('1','2','3'))->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropColumn('brand');
            $table->dropColumn('model');
            $table->dropColumn('series');
            $table->dropColumn('rated_power_output');
            $table->dropColumn('ac_power_rating');
            $table->dropColumn('solar_dc_rating');
            $table->dropColumn('battery_rated_storage_capacity');
            $table->dropColumn('battery_type');
            $table->dropColumn('approved_inverter');
            $table->dropColumn('export_limit');
            $table->dropColumn('sop3306_compliant');
            $table->dropColumn('as4777_compliant');
            $table->dropColumn('capacity');
            $table->dropColumn('certifying_authority_certificate_number');
            $table->dropColumn('inverter_phase');
        });
    }
}
