<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolarCapsule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       

        Schema::create('solar_capsule', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('lead_id');
            $table->bigInteger('party_id')->nullable();
            $table->bigInteger('opportunity_id')->nullable();
            $table->bigInteger('track_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        Schema::dropIfExists('solar_capsule');
    }
}
