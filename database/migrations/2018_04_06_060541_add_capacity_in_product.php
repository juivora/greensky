<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCapacityInProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            //
            $table->string('capacity_phase_a')->nullable();
            $table->string('capacity_phase_b')->nullable();
            $table->string('capacity_phase_c')->nullable();
            $table->string('product_capacity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
            $table->dropColumn('capacity_phase_a');
            $table->dropColumn('capacity_phase_b');
            $table->dropColumn('capacity_phase_c');
            $table->dropColumn('product_capacity');
        });
    }
}
