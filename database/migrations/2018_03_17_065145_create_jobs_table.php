<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('rff')->nullable();
            $table->string('pvd')->nullable();
            $table->string('connection_type')->nullable();
            $table->string('alternative_supply')->nullable();
            $table->string('supply_required')->nullable();
            $table->float('length_of_overhead_cable')->nullable();
            $table->string('pit_installed')->nullable();
            $table->string('oh_to_ug_conversion')->nullable();
            $table->string('meter_phase')->nullable();
            $table->integer('no_of_phases')->nullable();
            $table->float('phase_size')->nullable();
            $table->string('max_demand_installation')->nullable();
            $table->float('max_amp_per_phase')->nullable();
            $table->string('max_demand_of_total_site')->nullable();
            $table->float('demands_amp_per_phase')->nullable();
            $table->string('sccd_installed')->nullable();
            $table->float('sccd_amps')->nullable();
            $table->string('truck_appointment')->nullable();
            $table->string('lei_name')->nullable();
            $table->string('access_to_meter')->nullable();
            $table->string('access_to_vpi_lock')->nullable();
            $table->longText('access_notes')->nullable();
            $table->integer('no_of_stc')->nullable();
            $table->string('battery_storage')->nullable();
            $table->string('solar')->nullable();
            $table->string('battery_storage_source')->nullable();
            $table->string('embd_gen_installed')->nullable();
            $table->float('existing_rating')->nullable();
            $table->string('existing_export')->nullable();
            $table->string('pre_approval_ref_no')->nullable();
            $table->string('manual_to_customer')->nullable();
            $table->string('instructed_customer')->nullable();
            $table->string('inverter_time_disconnect')->nullable();
            $table->string('disconnect_test')->nullable();
            $table->string('inverter_time_reconnect')->nullable();
            $table->string('reconnect_test')->nullable();
            $table->string('export_test_sop_3306')->nullable();
            $table->string('export_test_sheet_attached')->nullable();
            $table->longText('installation_comments')->nullable();
            $table->string('no_of_panels')->nullable();
            $table->integer('no_of_panels_value')->nullable();
            $table->string('replacing')->nullable();
            $table->integer('no_of_replacing')->nullable();
            $table->string('additional')->nullable();
            $table->integer('no_of_additional')->nullable();
            $table->string('more_than_one_system')->nullable();
            $table->string('loc_of_other_system')->nullable();
            $table->float('solar_capacity_installed')->nullable();
            $table->float('solar_capacity_installed_phase_a')->nullable();
            $table->float('solar_capacity_installed_phase_b')->nullable();
            $table->float('solar_capacity_installed_phase_c')->nullable();
            $table->float('solar_capacity_existing')->nullable();
            $table->float('solar_capacity_existing_phase_a')->nullable();
            $table->float('solar_capacity_existing_phase_b')->nullable();
            $table->float('solar_capacity_existing_phase_c')->nullable();
            $table->float('solar_capacity_removed')->nullable();
            $table->float('solar_capacity_removed_phase_a')->nullable();
            $table->float('solar_capacity_removed_phase_b')->nullable();
            $table->float('solar_capacity_removed_phase_c')->nullable();
            $table->float('battery_capacity_installed')->nullable();
            $table->float('battery_capacity_installed_phase_a')->nullable();
            $table->float('battery_capacity_installed_phase_b')->nullable();
            $table->float('battery_capacity_installed_phase_c')->nullable();
            $table->float('battery_capacity_existing')->nullable();
            $table->float('battery_capacity_existing_phase_a')->nullable();
            $table->float('battery_capacity_existing_phase_b')->nullable();
            $table->float('battery_capacity_existing_phase_c')->nullable();
            $table->float('battery_capacity_removed')->nullable();
            $table->float('battery_capacity_removed_phase_a')->nullable();
            $table->float('battery_capacity_removed_phase_b')->nullable();
            $table->float('battery_capacity_removed_phase_c')->nullable();
            $table->string('inverter_phase')->nullable();
            $table->float('capacity_phase_a')->nullable();
            $table->float('capacity_phase_b')->nullable();
            $table->float('capacity_phase_c')->nullable();
            $table->string('export_limit')->nullable();
            $table->float('export_limit_value')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}


/*
#ALTER TABLE `jobs` ADD `inverter_phase` VARCHAR(191) NULL DEFAULT NULL AFTER `battery_capacity_removed_phase_c`;

#ALTER TABLE `jobs`
    ADD `capacity_phase_a` FLOAT(8,2) NULL DEFAULT NULL AFTER `inverter_phase`, 
    ADD `capacity_phase_b` FLOAT(8,2) NULL DEFAULT NULL AFTER `capacity_phase_a`, 
    ADD `capacity_phase_c` FLOAT(8,2) NULL DEFAULT NULL AFTER `capacity_phase_b`, 
    ADD `export_limit` VARCHAR(191) NULL DEFAULT NULL AFTER `capacity_phase_c`,
    ADD `export_limit_value` FLOAT(8,2) NULL DEFAULT NULL AFTER `export_limit`;
*/
