<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomerOffer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_offer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->default(0);
            $table->integer('offer_id')->default(0);
            $table->integer('package_id')->default(0);
            $table->float('package_kw')->default(0);
            $table->float('package_price')->default(0);
            $table->float('package_discount')->default(0);
            $table->float('package_discount_value')->default(0);
            $table->float('package_net_value')->default(0);
            $table->longText('package_obj')->nullable();
            $table->longText('customer_obj')->nullable();
            $table->string('status')->default(0);
            $table->string('approved_by')->nullable();
            $table->string('approval_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_offer');
    }
}
