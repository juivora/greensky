<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Green Sky</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	
	</head>
	<body style="margin:30px auto; width:850px;font-family: 'Montserrat', sans-serif;">
		<div style="background:#000; padding:20px; text-align:center;">
			<img class="brand_icon" src="{{asset('assets/images/green-logo.png')}}" alt="logo" style="width: 75px;" />
		</div>
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			<h2 style="margin-bottom: 35px;font-size: 25px;color:#000; font-size: 20px; text-align: center;">Sales Representative Assigned</h2>
			<table class="item_detail" style="border-collapse: collapse; width: 100%; text-align: center; margin:0px auto 20px auto;display: table;">
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="3"  class="bottom-4" style="text-align: center;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/top.jpg')}}" style="width: 100%;margin-bottom:-4px;">
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6" class="item_tbl" style="text-align: center;padding: 25px 0px !important;background: #ededed;padding: 10px 20px;font-size: 15px;padding: 0;">
						<table class="tbl_order" style="background-color: #EDEDED;color: #000;border-bottom: 1px dashed #d3d3d3;">
							<div class="wrap_table" style="text-align: left;width: 80%;margin: 0 auto;">
								<h4 style="color: #333333; margin-left: 23px;margin: 10px 0px 20px;"><b>Hello {{$customer->first_name}} {{$customer->last_name}}</b></h4h4>
								<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;">
                                        {{$customer->sales->name}} will contact you soon for further information.</p>
										@if($customer->sales->name == "Keith Buxton")
										<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;">If you would like to schedule a preferable time for {{ $customer->sales->name }} to call and discuss further please follow this link  <a target="_blank" href="https://calendly.com/keith-4/15min">https://calendly.com/keith-4/15min</a> </p>
										@elseif ($customer->sales->name == "David Prince")
										<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;">If you would like to schedule a preferable time for David Prince to call and discuss further please follow this link <a target="_blank" href="https://calendly.com/david-1032/15min">https://calendly.com/david-1032/15min</a> </p>
										<!--<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;">David resides in Tasmania and can conduct a site inspection and assessment at a convenient time.  </p>-->
										@endif
								</br>
								<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;">Thanks</p>
								<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;"><b>Regards, Green Sky Australia</b></p>
							</div>
						</table>
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6"  class="top-4" style="text-align: center;top: -5px; position: relative;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/bottom.jpg')}}" style="width: 100%;margin-top:-4px;">
					</td>
				</tr>
			</table>
			
			<h4 style="margin-top: 35px;color: #333333; margin-left: 23px;"></h4>
			
		</div>
		<div class="footer" style="background:#333333; color:#ffffff; padding:10px;">
			<!-- <p>Copyright © 2017 All rights reserved</p> 
			<a href="https://www.be-help.com">www.be-help.com</a>-->
		</div>
	</body>
</html>
