<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Green Sky</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
		
		<link href="{!! asset('assets/stylesheets/signature-pad.css') !!}" media="all" rel="stylesheet" type="text/css"/>
	     <link href="{!! asset('assets/stylesheets/design.css') !!}" media="all" rel="stylesheet" type="text/css"/>
	      <link href="{!! asset('assets/stylesheets/style.css') !!}" media="all" rel="stylesheet" type="text/css"/>
	      
	</head>
	<script src="{!! asset('assets/javascripts/jsignature/jquery.js') !!}" type="text/javascript"></script>

<script src="{!! asset('assets/javascripts/jsignature/signature_pad.js') !!}" type="text/javascript"></script>

<script src="{!! asset('assets/javascripts/jsignature/modernizr.js') !!}" type="text/javascript"></script>


	<body style="margin:30px auto; width:850px;font-family: 'Montserrat', sans-serif;">
		<div style="background:#000; padding:20px; text-align:center;">
			<img class="brand_icon" src="{{asset('assets/images/green-logo.png')}}" alt="logo" style="width: 75px;" />
		</div>
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			<h2 style="margin-bottom: 35px;font-size: 25px;color:#000; font-size: 20px; text-align: center;"> Job Completion Form </h2>
			<table class="item_detail" style="border-collapse: collapse; width: 100%; text-align: center; margin:0px auto 20px auto;display: table;">
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="3"  class="bottom-4" style="text-align: center;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/top.jpg')}}" style="width: 100%;margin-bottom:-4px;">
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6" class="item_tbl" style="text-align: center;padding: 25px 0px !important;background: #ededed;padding: 10px 20px;font-size: 15px;padding: 0;">
						<table class="tbl_order" style="background-color: #EDEDED;color: #000;border-bottom: 1px dashed #d3d3d3;">
							<div class="wrap_table" style="text-align: left;width: 80%;margin: 0 auto;">
								<h4 style="color: #333333; margin-left: 23px;margin: 10px 0px 20px;"><b>Hello {{$customer->first_name}} {{ $customer->last_name }} </b></h4>
								@if ($customer->electrical_distributer == "Citipower" || $customer->electrical_distributer == "Powercor")  
						<p>
									<strong>
CUSTOMER ACKNOWLEDGEMENT, INDEMNITY AND RELEASE</strong></p>
	<p><strong>By signing this form, you acknowledge and represent that you have read, understand and agree to comply with the
connection obligations, and that you:</strong></p>
<p>
<ul>
<li>
  are the owner of the PV (Solar) System listed under the Supply Address in section 3 above; </li>
<li>  have received a Photovoltaic Embedded Generator operating manual from, and been instructed on the operation of the Photovoltaic Embedded Generator 
by, the Installation Company detailed in section 1;</li>
<li>  accept that approval will only be granted for the Photovoltaic Embedded Generator detailed in this form, and that you must obtain further prior approval 
from your Distributor to alter your Photovoltaic Embedded Generator in any way;</li>
<li>  release and indemnify and agree to keep indemnified your Distributor, its officers, employees and agents against all actions, proceedings, claims and 
demands whatsoever which may be brought, including any indirect or consequential loss or any other form of pure economic loss, made or prosecuted 
against them or any of them by any person in respect of the installation of your Photovoltaic Embedded Generator, particularly in relation to works 
completed by the Installation Company detailed in section 1 or the compliance certification provided by the Registered Electrical Contractor in section 2,
or in respect of connection of your Photovoltaic Embedded Generator to the Victorian electricity grid.</li>
									</p>

								@elseif ($customer->electrical_distributer == "Jemena") 
										<p><strong>	Expedited Offer and Acceptance(Optional) </strong></p>
							<ol><li>
This section is optional and only needs to be completed if your equire an expedited connection. An expedited connection 
is one where you agree to accept our deemed offer including the terms of our model standing offer and do not wish to go 
through the offer and acceptance process. A copy of our deemed offer can be viewed on our website or you can contact 
usto request a copy be sent to you. If youdo require an expedited connection youmust answer 'yes' to the question set 
out below. 
</li><li>
If you do complete this section (by answering 'yes' to the question set out below), then provided we are satisfied that the 
service requested by you(as indicated in this application) falls within the terms of our model standing offerfor that service
then you will be taken to have accepted an offer from us on the terms of that model standing offer on the date of this 
application.
</li></ol> 

<p><strong>Energisation </strong></p>
<p>Important Note: We will not energise (turn on) any new connection established by usunder any offerto provide a basic connection 
service until the  customer has appointed a  retailer and that  retailer has requested for energisation of the  customer’s premises in 
accordance with the terms of our offer. If the  customer requires the new connection to be energised (turned on) at the time of the 
connection installation the customer should make an application for a new connection through a retailer of their choice. </p>
<p>
Acknowledgement and authority 
<ol><li>
 You acknowledge that  we will base our offer on the information provided in this  application, confirm that the information 
provided in the application is true and correct, and agree to notify us if any of the information in the application changes. </li>
<li> You confirm that you are authorised to make this application on behalf of the customer at the supply address.</li>
<li> You consent  and  (if  applicable)  you confirm  that  you have  obtained  the  customer’s consent,  to  our collection,  use  and 
disclosure of the personal information included in  this  application according to our  model standing offer and  our privacy 
policy which are available on our website. Please contact us if  you would like a hard-copy of our privacy policy and we will 
post you a copy. </li>
<li> You confirm that the safety and technical requirements as defined in our model standing offer have been met. In particular: 
<ul><li>
The generator complies with the Electricity Safety Act 1998(Vic) and associated Safety Regulations, the 
Electricity Distribution Code, the Victorian Service & Installation Rules, AS/NZS3000 (Wiring Rules) and AS4777 
(Grid Connection of Energy Systems via Inverters), and any other relevant Acts, regulations, standardsor 
guidelines;</li>
<li> commissioning tests as specified in the Service & Installation Rules have been completed and passed;</li>
<li> the generator is connected to a dedicated circuit complete with lockable isolating switch at the switchboard;</li>
<li> the main switchboard, isolating fuse/switch/circuit breaker are labelled correctly;</li>
<li> alternative supply signage has been installed; and, </li>
<li>the customer has been advised that the generator should remains witched off until any metering upgrades are 
complete to avoid potential metering and billing issues.</li></ul> </li>
<li>  You have attached a signed prescribed Certificate of Electricity Safety supplied by a registered electrical contractor</li>
<li>  You have attached an Electrical Works Request suppliedby aregistered electrical contractor. (where applicable) </li>
<li>  You acknowledge that we will not commence the connection worksuntil the relevant connection charge has been paid. </li></ol></p>				
								@elseif ($customer->electrical_distributer == "United Energy") 
									
											<p><strong>CUSTOMER ACKNOWLEDGEMENT </strong></p>
<p><strong>Acceptance of Model Standing Offer</strong></p>

<p>By signing below, you acknowledge and represent that you have read, understand and agree to comply with the Basic 
Micro 
Embedded Generator Connection Model Standing Offer, and that you:
<ul>
<li> are the owner or have a contract with the owner of the inverter-based embedded generator system listed under the Supply 
Address in Section 1 above;</li>
<li>  have received an inverter-based embedded generator system operating manual from, and been instructed on the operation of 
the inverter-based embedded generator system by, the Installation Company detailed in Section 1;</li>
<li>  accept that approval will only be granted for the inverter-based embedded generator system detailed in this form, and that you 
must obtain further prior approval from United Energy to alter your inverter-based embedded generator system in any way; </li>
<li>  do not require a written offer.</li> </p>
								@elseif ($customer->electrical_distributer == "SP Ausnet") 
									<p><strong>CUSTOMER ACKNOWLEDGEMENT, INDEMNITY AND RELEASE </strong></p>
<p>By  signing  and  returning  this  agreement,  and  in  consideration  of  AusNet  Services  allowing  you  to  connect  your  EG  to  the  distribution  network,  you 
acknowledge and represent that you have read, understand and agree to comply with the following connection obligations, and specifically that you: </p>
<ul>
<li>are the owner of the EG listed under the Supply Address in section 3 above; </li>
<li>  confirm all installation details are correct as described in Section 1 above; </li>
<li>  have received an EG operating manual from, and been instructed on the operation of the EG by, the Installation Company detailed in section 1; </li>
<li>  will regularly maintain the EG to confirm its correct performance in accordance with this agreement and keep records of the maintenance performed; </li>
<li>  will ensure that the EG and any equipment within it that is connected to  the  Victorian electricity grid  complies with the Electricity Distribution Code, the  Electricity 
Safety Act 1998 (Vic) and associated Safety Regulations, all relevant Australian Standards and is maintained in a safe condition; </li>
<li>  accept that  this agreement allows you to connect  the  EG  detailed in this  agreement  to the  Victorian electricity grid, and that you must obtain prior  written approval 
from AusNet Services to alter your EG in any way; </li>
<li>  release and indemnify and agree to keep indemnified  AusNet Services,  its officers, employees and agents against all actions, proceedings, claims and demands 
whatsoever which may be brought, including any indirect or consequential loss or any other form of pure economic loss, made or prosecuted against them or any of 
them by  any  person in respect  of the  installation of  your  EG, particularly in  relation to  works completed  by the  Installation Company detailed in  section 1 or the 
compliance certification provided by the registered electrical contractor in section 2, or in respect of connection of your EG to the Victorian electricity grid; and </li>
<li>  in accordance with the Electricity Distribution Code will disconnect, or will allow AusNet Services to disconnect, your EG fr om the Victorian electricity grid if the EG or 
any equipment within it breaches the requirements of the Electricity Safety Act 1998 (Vic), any  associated Safety Regulations, is not in compliance with any relevant 
Australian Standards or breaches the agreed installed capacity, export limits or performance requirements set out in this agreement. </li></ul>
								
								@else 
						
								
								@endif	
								<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;">Please put your Signature & Confirm that the job was completed successfully. </p></br>
						    @php $id = md5($job->id); @endphp


						
						<form method="post" action="{{url('/complete-sign-form/'.$id)}}">
						    	{{ csrf_field() }}
						
                    
                    <input type="hidden" id="sign" name="sign" value="">
                    
                     <div id="signature-pad" class="signature-pad">
    <div class="signature-pad--body">
      <canvas width="650" height="250"></canvas>
    </div>
    <div class="signature-pad--footer">
      <div class="description">Sign above</div>

      <div class="signature-pad--actions">
        <div>
          <button type="button" class="button clear" data-action="clear">Clear</button>

        </div>
        
      </div>
    </div>
  </div>
   <input type="hidden" id="lat" name="lat" value="">
    <input type="hidden" id="long" name="long" value="">
    
                    <button type="submit" name="submit" id="submit" class="btn btn-success" value="submit"> Submit </button>
                    
                    </form>
                    
                    
								<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;" class="check">Thanks,</p>
								<p style="margin:0px; text-align:center; font-size:15px;line-height: 23px;text-align: left;"><b>Regards, Green Sky Australia</b></p>
							</div>
						</table>
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6"  class="top-4" style="text-align: center;top: -5px; position: relative;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/bottom.jpg')}}" style="width: 100%;margin-top:-4px;">
					</td>
				</tr>
			</table>
			
			<h4 style="margin-top: 35px;color: #333333; margin-left: 23px;"></h4>
			
		</div>
		<div class="footer" style="background:#333333; color:#ffffff; padding:10px;">
			<!-- <p>Copyright © 2017 All rights reserved</p> 
			<a href="https://www.be-help.com">www.be-help.com</a>-->
		</div>
	</body>
</html>


<script>
$(document).ready(function() {
    
    var wrapper = document.getElementById("signature-pad");
    var clearButton = wrapper.querySelector("[data-action=clear]");
    var canvas = wrapper.querySelector("canvas");
    var signaturePad = new SignaturePad(canvas, {
      backgroundColor: 'rgb(255, 255, 255)'
    });

    clearButton.addEventListener("click", function (event) {
      signaturePad.clear();
    });

    $( "#submit" ).click(function( event ) {
        
        var dataURL = signaturePad.toDataURL("image/jpeg");
        if (signaturePad.isEmpty()) {
            alert("Please provide a signature first.");
            return false;
        } else {
            $('#sign').val(dataURL);
        }
        
        function success(position) {
            var latitude = position.coords.latitude ;
            var longitude = position.coords.longitude ;
            $('#lat').val(latitude);
            $('#long').val(longitude);
        }

        function error(err) {
            console.warn(`ERROR(${err.code}): ${err.message}`);
        }

        navigator.geolocation.getCurrentPosition(success, error);
        
    
    });

})
</script>