
           @PHP $address = \App\Setting::give_value_of('Address');
            $address =  str_replace("<br/>", "\n", $address); @ENDPHP
          <div id="Q-12" class="tab">
            
            <p><label>Applicant details :  </label> </p>
            <p><label>Company: </label> <input type="text" oninput="this.className=''" name="company_name" id="company_name" value="{{ \App\Setting::give_value_of('Company_Name') }}" /> </p>
            <p><label>Address: </label>  {!! Form::textarea('address', $address ,['oninput' => "this.className=''",'disabled']) !!} </p>
            <p><label>Telephone: </label> <input type="text" oninput="this.className=''" name="Telephone" id="Telephone" value="{{ \App\Setting::give_value_of('Phone') }}"  /> </p>
            <p><label>E-mail: </label> <input type="text" oninput="this.className=''" name="Email" id="Email" value="{{ \App\Setting::give_value_of('Email') }}" /> </p>
            <p><label>Name: </label> <input type="text" oninput="this.className=''" name="REC_Name" id="REC_Name" value="{{ \App\Setting::give_value_of('REC_Name') }}" /> </p>
            <hr />
            <p><label>Are you, the applicant, the customer at the supply address:  </label>
              <label>{!! Form::radio('applicant_supply_address', 'Yes', isset($questionnaire_data->applicant_supply_address) && $questionnaire_data->applicant_supply_address == 'Yes'  ? true : null   ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('applicant_supply_address', 'No', isset($questionnaire_data->applicant_supply_address) && $questionnaire_data->applicant_supply_address == 'No'  ? true : true ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
           </div>

          <div id="Q-13" class="tab">
          <p><label>Customer details :  </label> </p>
            <p><label>Generator Owner Name: </label> <input type="text" oninput="this.className=''" name="customer_name" id="customer_name" value="{{ $customer->first_name.' '.$customer->last_name }} " /> </p>
            <p><label>E-mail Address: </label> <input type="text" oninput="this.className=''" name="customer_email" id="customer_email" value="{{ $customer->email }}" /> </p>
            <p><label>Telephone Number (Business Hours): </label> <input type="text" oninput="this.className=''" name="customer_cell" id="customer_cell" value="{{ $customer->phone_mobile }}" /> </p>
            <p><label>Telephone Number (After Hours): </label> <input type="text" oninput="this.className=''" name="customer_homephone" id="customer_homephone" value="{{ $customer->phone_home }}" /> </p>
            <p><label>Address: </label>
               <textarea name="customer_address"  rows="3" >{{$customer->unit.' '.$customer->street_number.' '.$customer->street_name.' '.$customer->lot.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code }}</textarea>
                </p>
            
          </div>

          <div id="Q-14" class="tab">
            <p><label>Registered Electrical contractor  </label> </p> 
              <p><label>Registered Electrical Contractor Name   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            <p><label>REC Number :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Number') }}" /></p>

            @PHP $address = \App\Setting::give_value_of('Address');
            $address =  str_replace("<br/>", "\n", $address); @ENDPHP

            <p><label>Registered address   :  </label> {!! Form::textarea('address', $address ,['oninput' => "this.className=''",'disabled']) !!} </p>
            

            <p><label>Phone number   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('Phone') }}" /></p>

            <p><label>Responsible person   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            @php $image=""; $image = \App\Setting::give_value_of('Signature'); @endphp

            <p><label> Signature :  </label> <img src="{{asset('/img/'.$image)}}" width="100px" height="50px" alt="signature" /> </p>
           
            <p><label>Date :  </label> <input type="text" class="completion_date" oninput="this.className=''" disabled /></p>
            <hr />
            <p><label>Supply Address:  </label> </p> 
             <p>{!! Form::textarea('supply_address', $customer->unit.' '.$customer->street_number.' '.$customer->street_name.' '.$customer->lot.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code ,['oninput' => "this.className=''",'disabled']) !!}</p> 
            
          </div>

          <div id="Q-15" class="tab">
          <p><label>Service required:  </label> </p> 
            <p><label>Inverter / embedded Generator:  </label>
              <label>{!! Form::radio('server_required_inverter', 'New',  isset($questionnaire_data->server_required_inverter) && $questionnaire_data->server_required_inverter == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New  </label>
              <label>{!! Form::radio('server_required_inverter', 'Alteration', isset($questionnaire_data->server_required_inverter) && $questionnaire_data->server_required_inverter == 'Alteration'  ? true : null ,['oninput' => "this.className=''"]) !!} Alteration </label>
            </p>
            <p><label>Electricity supply:  </label>
              <label>{!! Form::radio('service_required_electicity', 'New',  isset($questionnaire_data->service_required_electicity) && $questionnaire_data->service_required_electicity == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New  </label>
              <label>{!! Form::radio('service_required_electicity', 'Alteration',   isset($questionnaire_data->service_required_electicity) && $questionnaire_data->service_required_electicity == 'Alteration'  ? true : null ,['oninput' => "this.className=''"]) !!} Alteration </label>
              <p><label>Customer NMI: </label> <input type="text" oninput="this.className=''" name="customer_nmi" id="customer_nmi" value="{{ $customer->nmi }}" /> </p>
            </p>
           
          </div>

          <div id="Q-16" class="tab">
             <p><label>Inverter Installation details:  </label> </p> 
            <p><label>Are/will separate inverters be installed for generator and energy storage: </label> 
            <label>{!! Form::radio('seperate_inverter_installed', 'Yes',  isset($questionnaire_data->seperate_inverter_installed) && $questionnaire_data->seperate_inverter_installed == 'Yes'  ? true : null  ,['oninput' => "this.className=''"]) !!} Yes Separate   </label>
              <label>{!! Form::radio('seperate_inverter_installed', 'No',  isset($questionnaire_data->seperate_inverter_installed) && $questionnaire_data->seperate_inverter_installed == 'No'  ? true : null ,['oninput' => "this.className=''"]) !!} No Combined </label> </p>
          </div>

          <div id="Q-17" class="tab">
            <p><label>Solar PV (kW) </label> </p>
            <p><label>New</label> <input type="text" oninput="this.className=''" name="solar_new" id="solar_new" value="{{$questionnaire_data->solar_new or ''}}" /> </p>
            <p><label>Existing Retained</label> <input type="text" oninput="this.className=''" name="solar_retained" id="solar_retained" value="{{$questionnaire_data->solar_retained or 'N/A'}}" /> </p>
            <p><label>Existing Removed</label> <input type="text" oninput="this.className=''" name="solar_removed" id="solar_removed" value="{{$questionnaire_data->solar_removed or 'N/A'}}" /> </p>
            <p><label>Total capacity: </label> <input type="text" oninput="this.className=''" name="solar_total" id="solar_total" value="{{$questionnaire_data->solar_total or ''}}" /> </p>
            <hr />
            <p><label>Wind (kVA)</label> </p>
            <p><label>New</label> <input type="text" oninput="this.className=''" name="wind_new" id="wind_new" value="{{$questionnaire_data->wind_new or ''}}" /> </p>
            <p><label>Existing Retained</label> <input type="text" oninput="this.className=''" name="wind_retained" id="wind_retained" value="{{$questionnaire_data->wind_retained or ''}}" /> </p>
            <p><label>Existing Removed</label> <input type="text" oninput="this.className=''" name="wind_removed" id="wind_removed" value="{{$questionnaire_data->wind_removed or ''}}" /> </p>
            <p><label>Total capacity: </label> <input type="text" oninput="this.className=''" name="wind_total" id="wind_total" value="{{$questionnaire_data->wind_total or ''}}" /></p>
            <hr />
            <p><label>Other (kVA) </label> </p>
            <p><label>New</label> <input type="text" oninput="this.className=''" name="other_new" id="other_new" value="{{$questionnaire_data->other_new or ''}}" /> </p>
            <p><label>Existing Retained</label> <input type="text" oninput="this.className=''" name="other_retained" id="other_retained" value="{{$questionnaire_data->other_retained or ''}}" /> </p>
            <p><label>Existing Removed</label> <input type="text" oninput="this.className=''" name="other_removed" id="other_removed" value="{{$questionnaire_data->other_removed or ''}}" /> </p>
            <p><label>Total capacity: </label> <input type="text" oninput="this.className=''" name="other_total" id="other_total" value="{{$questionnaire_data->other_total or ''}}" /></p>
            <hr />
            <p><label>Energy Storage (kWh) </label> </p>
             <p><label>New</label> <input type="text" oninput="this.className=''" name="energy_storage_new" id="energy_storage_new" value="{{$questionnaire_data->energy_storage_new or 'N/A'}}" /> </p>
            <p><label>Existing Retained</label> <input type="text" oninput="this.className=''" name="energy_storage_retained" id="energy_storage_retained" value="{{$questionnaire_data->energy_storage_retained or 'N/A'}}" /> </p>
            <p><label>Existing Removed</label> <input type="text" oninput="this.className=''" name="energy_storage_removed" id="energy_storage_removed" value="{{$questionnaire_data->energy_storage_removed or 'N/A'}}" /> </p>
            <p><label>Total capacity: </label> <input type="text" oninput="this.className=''" name="energy_storage_total" id="energy_storage_total" value="{{$questionnaire_data->energy_storage_total or 'N/A'}}" /></p>


          </div>

          <div id="Q-18" class="tab">
            <p><label>Multi phase systems only  </label> </p>
            <hr />
            <p><label>Red  </label> </p>
            <p><label>Power rating </label> <input type="text" oninput="this.className=''" name="red_power_rating" id="red_power_rating" value="{{$questionnaire_data->red_power_rating or ''}}" /> kW</p>
            <p><label>Energy Storage capacity</label> <input type="text" oninput="this.className=''" name="red_energy_storage_capacity" id="red_energy_storage_capacity" value="{{ $questionnaire_data->red_energy_storage_capacity or ''}}" /> kWh</p>
            <hr />
            <p><label>White  </label> </p>
            <p><label>Power rating </label> <input type="text" oninput="this.className=''" name="white_power_rating" id="white_power_rating" value="{{$questionnaire_data->white_power_rating or ''}}" /> kW</p>
            <p><label>Energy Storage capacity</label> <input type="text" oninput="this.className=''" name="white_energy_storage_capacity" id="white_energy_storage_capacity" value="{{ $questionnaire_data->white_energy_storage_capacity or ''}}" /> kWh</p>
            <hr />
            <p><label>Blue  </label> </p>
            <p><label>Power rating </label> <input type="text" oninput="this.className=''" name="blue_power_rating" id="blue_power_rating" value="{{$questionnaire_data->blue_power_rating or ''}}" /> kW</p>
            <p><label>Energy Storage capacity</label> <input type="text" oninput="this.className=''" name="blue_energy_storage_capacity" id="blue_energy_storage_capacity" value="{{ $questionnaire_data->blue_energy_storage_capacity or ''}}" /> kWh</p>
            
          
          </div>

          <div id="Q-19" class="tab">
            <input type="hidden" id="inverter_count" name="inverter_count" value="1">
            <p><label>Inverter Manufacturer: </label> <input type="text" oninput="this.className=''" name="inverter_manufacturer_1" id="inverter_manufacturer" value="{{  isset($questionnaire_data->inverter_manufacturer_1) ? $questionnaire_data->inverter_manufacturer_1 : $inverter[0]->manufacturer  }}" /> </p>
            <p><label>Inverter Model Name: </label> <input type="text" oninput="this.className=''" name="inverter_model_1" id="inverter_model_1" value="{{ isset($questionnaire_data->inverter_model_1) ? $questionnaire_data->inverter_model_1 : $inverter[0]->model  }}" /> </p>
            <p><label>Rating of Inverter: </label> <input type="text" oninput="this.className=''" name="inverter_rating_1" id="inverter_rating_1" value="{{isset($questionnaire_data->inverter_rating_1) ? $questionnaire_data->inverter_rating_1 : $inverter[0]->rating_inverter   }}" /> kW</p>
            <p><label>Number of inverters: </label> <input type="text" oninput="this.className=''" name="inverter_number_1" id="inverter_number_1" value="{{ isset($questionnaire_data->inverter_number_1) ? $questionnaire_data->inverter_number_1 : $inverter[0]->quantity  }}" /> </p>
           
            <p><label>Status: </label><label>{!! Form::radio('inverter_status_1', 'New', isset($questionnaire_data->inverter_status_1) && $questionnaire_data->inverter_status_1 == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('inverter_status_1', 'Existing retained', isset($questionnaire_data->inverter_status_1) && $questionnaire_data->inverter_status_1 == 'Existing retained'  ? true : null  ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('inverter_status_1', 'Existing Removed', isset($questionnaire_data->inverter_status_1) && $questionnaire_data->inverter_status_1 == 'Existing Removed'  ? true : null  ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Number_1" id="CAC_Number_1" value="{{ isset($questionnaire_data->CAC_Number_1) ? $questionnaire_data->CAC_Number_1 : isset($inverter[0]->cac_number) ?  $inverter[0]->cac_number  : ''  }}" /> </p>
            <p><label>Additional Inverter : </label> <label>{!! Form::checkbox('additional_inverter_1','Yes', isset($questionnaire_data->additional_inverter_1) && $questionnaire_data->additional_inverter_1 == 'Yes'  ? true : false, ['oninput' => "this.className=''", 'id' => 'additional_inverter_1'] ) !!}  </label> </p>
            <div id="inverter2" style="display:none" >
            <hr />
            <p><label>Inverter Manufacturer: </label> <input type="text" oninput="this.className=''" name="inverter_manufacturer_2" id="inverter_manufacturer_2" value="{{ isset($questionnaire_data->inverter_manufacturer_2) ? $questionnaire_data->inverter_manufacturer_2 : ( isset($inverter[1]->manufacturer) ? $inverter[1]->manufacturer : '' ) }}" /> </p>
            <p><label>Inverter Model Name: </label> <input type="text" oninput="this.className=''" name="inverter_model_2" id="inverter_model_2" value="{{ isset($questionnaire_data->inverter_model_2) ? $questionnaire_data->inverter_model_2 : (isset($inverter[1]->model) ? $inverter[1]->model : '') }}" /> </p>
            <p><label>Rating of Inverter: </label> <input type="text" oninput="this.className=''" name="inverter_rating_2" id="inverter_rating_2" value="{{ isset($questionnaire_data->inverter_rating_2) ? $questionnaire_data->inverter_rating_2 : (isset($inverter[1]->rating_inverter) ? $inverter[1]->rating_inverter  : '')  }}" /> kW</p>
            <p><label>Number of inverters: </label> <input type="text" oninput="this.className=''" name="inverter_number_2" id="inverter_number_2" value="{{ isset($questionnaire_data->inverter_number_2) ? $questionnaire_data->inverter_number_2 : (isset($inverter[1]->quantity) ?  $inverter[1]->quantity  : '')  }}" /> </p>
            <p><label>Status: </label><label>{!! Form::radio('inverter_status_2', 'New',  isset($questionnaire_data->inverter_status_2) && $questionnaire_data->inverter_status_2 == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('inverter_status_2', 'Existing retained', isset($questionnaire_data->inverter_status_2) && $questionnaire_data->inverter_status_2 == 'Existing retained'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('inverter_status_2', 'Existing Removed', isset($questionnaire_data->inverter_status_2) && $questionnaire_data->inverter_status_2 == 'Existing Removed'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
           
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Number_2" id="CAC_Number_2" value="{{ isset($questionnaire_data->CAC_Number_2) ? $questionnaire_data->CAC_Number_2 : (isset($inverter[1]->cac_number) ?  $inverter[1]->cac_number  : '' ) }}" /> </p>
            <p><label>Additional Inverter : </label> <label>{!! Form::checkbox('additional_inverter_2','Yes', isset($questionnaire_data->additional_inverter_2) && $questionnaire_data->additional_inverter_2 == 'Yes'  ? true : false, ['oninput' => "this.className=''", 'id' => 'additional_inverter_2'] ) !!}  </label> </p> </p>
            </div>
            <div id="inverter3" style="display:none" >
            <hr />
            <p><label>Inverter Manufacturer: </label> <input type="text" oninput="this.className=''" name="inverter_manufacturer_3" id="inverter_manufacturer_3" value="{{ isset($questionnaire_data->inverter_manufacturer_3) ? $questionnaire_data->inverter_manufacturer_3 :  (isset($inverter[2]->manufacturer) ? $inverter[2]->manufacturer : '') }}" /> </p>
            <p><label>Inverter Model Name: </label> <input type="text" oninput="this.className=''" name="inverter_model_3" id="inverter_model_3" value="{{ isset($questionnaire_data->inverter_model_3) ? $questionnaire_data->inverter_model_3 :  (isset($inverter[2]->model) ? $inverter[2]->model : '') }}" /> </p>
            <p><label>Rating of Inverter: </label> <input type="text" oninput="this.className=''" name="inverter_rating_3" id="inverter_rating_3" value="
              {{ isset($questionnaire_data->inverter_rating_3) ? $questionnaire_data->inverter_rating_3 :  (isset($inverter[2]->rating_inverter) ? $inverter[2]->rating_inverter : '') }}" /> kW</p>
            <p><label>Number of inverters: </label> <input type="text" oninput="this.className=''" name="inverter_number_3" id="inverter_number_3" value="{{ isset($questionnaire_data->inverter_number_3) ? $questionnaire_data->inverter_number_3 :  (isset($inverter[2]->quantity) ? $inverter[2]->quantity : '') }}" /> </p>
            
            <p><label>Status: </label><label>{!! Form::radio('inverter_status_3', 'New', isset($questionnaire_data->inverter_status_3) && $questionnaire_data->inverter_status_3 == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('inverter_status_3', 'Existing retained',  isset($questionnaire_data->inverter_status_3) && $questionnaire_data->inverter_status_3 == 'Existing retained'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('inverter_status_3', 'Existing Removed',  isset($questionnaire_data->inverter_status_3) && $questionnaire_data->inverter_status_3 == 'Existing Removed'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Number_3" id="CAC_Number_3" value="{{ isset($questionnaire_data->CAC_Number_3) ? $questionnaire_data->CAC_Number_3 : (isset($inverter[2]->cac_number) ?  $inverter[2]->cac_number  : '')  }}" /> </p>
             </p>
            </div>
            <hr />
            <p><label>Energy Storage System : </label> 
            <label>Was storage installed? </label> 
             <label>{!! Form::checkbox('additional_storage_1','Yes', isset($questionnaire_data->additional_storage_1) && $questionnaire_data->additional_storage_1 == 'Yes'  ? true : false, ['oninput' => "this.className=''", 'id' => 'additional_storage_1'] ) !!}  </label>
            <div id="energyStorageSystem"  style="display:none">
               <input type="hidden" id="storage_count" name="storage_count" value="1">
            <p><label>Manufacturer: </label> <input type="text" oninput="this.className=''" name="storage_manufacture_1" id="storage_manufacture_1" value="{{ $questionnaire_data->storage_manufacture_1 or ''}}" /> </p>
            <p><label>Model Name: </label> <input type="text" oninput="this.className=''" name="storage_model_1" id="storage_model_1" value="{{ $questionnaire_data->storage_model_1 or ''}}" /> </p>
            <p><label>Rating of Storage system : </label> <input type="text" oninput="this.className=''" name="storage_rating_1" id="storage_rating_1" value="{{ $questionnaire_data->storage_rating_1 or ''}}" /> kWh </p>
            <p><label>Number of Storage systems: </label> <input type="text" oninput="this.className=''" name="storage_number_1" id="storage_number_1" value="{{ $questionnaire_data->storage_number_1 or ''}}" /> </p>
            <p><label>Status: </label><label>{!! Form::radio('storage_status_1', 'New',  isset($questionnaire_data->storage_status_1) && $questionnaire_data->storage_status_1 == 'New'  ? true : null ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('storage_status_1', 'Existing retained',  isset($questionnaire_data->storage_status_1) && $questionnaire_data->storage_status_1 == 'Existing retained'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('storage_status_1', 'Existing Removed',  isset($questionnaire_data->storage_status_1) && $questionnaire_data->storage_status_1 == 'Existing Removed'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Storage_Number_1" id="CAC_Storage_Number_1" value="{{ $questionnaire_data->CAC_Storage_Number_1 or ''}}" /> </p> 
            </div>
          </div>

          <div id="Q-20" class="tab">
            <p><label>Has the operating Manual been provided to the customer:  </label>
              <label>{!! Form::radio('manual_to_customer', 'Yes', (isset($job->manual_to_customer) && $job->manual_to_customer == "Yes" ) || $job->manual_to_customer == "" ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('manual_to_customer', 'No', isset($job->manual_to_customer) && $job->manual_to_customer == "No"  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Has the customer been instructed in the operation of the system:  </label> 
              <label>{!! Form::radio('instructed_customer', 'Yes', (isset($job->instructed_customer) && $job->instructed_customer == "Yes" ) || $job->instructed_customer == "" ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('instructed_customer', 'No', isset($job->instructed_customer) && $job->instructed_customer == "No"  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            
          </div>

          
          <div id="Q-21" class="tab">
            <p><label>Description of Inverter Alteration: </label> 
            {!! Form::textarea('description_inverter_alteration', "N/A" ,['oninput' => "this.className=''",'disabled']) !!}
             </p>
            
          </div>

          <div id="Q-22" class="tab">

            <p><label>Site Access / Special Instructions : </label>{!! Form::textarea('site_access_special_instruction', "Good Access" ,['oninput' => "this.className=''",'disabled']) !!}</p>
          </div>
          <div id="Q-23" class="tab">
          <p><label>Expedited offer:  </label> 
              <label>{!! Form::radio('expedited_offer', 'Yes',  isset($questionnaire_data->expedited_offer) && $questionnaire_data->expedited_offer == 'Yes'  ? true : true   ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('expedited_offer', 'No', isset($questionnaire_data->expedited_offer) && $questionnaire_data->expedited_offer == 'Yes'  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
          </div>
          <div id="Q-24" class="tab">
            <p>
              <label>Energisation  </label> </p><p>
             <strong> Important Note: </strong> We will not energise (turn on) any new connection established by us under any offer to provide a basic connection service  until  the customer  has  appointed  a retailer  and  that retailer  has  requested  for  energisation  of  the customer’s  premises  in accordance with the terms of our offer. If the customer requires the new connection to be energised (turned on) at the time of the connection installation the customer should make an application for a new connection through a retailer of their choice.
            </p>
            <p>
              <label>Acknowledgement and authority</label>
            </p>
            <p>
              <ol>  
                <li>You acknowledge that we will base our offer on the information provided in this application, confirm that the information provided in the application is true and correct, and agree to notify us if any of the information in the application changes. </li>
<li>You confirm that you are authorised to make this application on behalf of the customer at the supply address.</li>
<li>You consent and (if applicable) you confirm that you have obtained the customer’s consent, to our collection, use and disclosure of the personal information included in this application according to our model standing offer and our privacy policy which are available on our website. Please contact us if you would like a hard-copy of our privacy policy and we will post you a copy. </li>
<li>You confirm that the safety and technical requirements as defined in ourmodel standing offer have been met. In particular: 
<ul><li>The generator complies with the Electricity Safety Act 1998 (Vic) and associated Safety Regulations, the Electricity Distribution Code, the Victorian Service & Installation Rules, AS/NZS3000 (Wiring Rules) and AS4777 (Grid Connection of Energy Systems via Inverters), and any other relevant Acts, regulations, standards or guidelines; </li>
<li>commissioning tests as specified in the Service & Installation Rules have been completed and passed;</li>
<li>the generator is connected to a dedicated circuit complete with lockable isolating switch at the switchboard;</li>
<li>the main switchboard, isolating fuse/switch/circuit breaker are labelled correctly;</li>
<li>alternative supply signage has been installed; and, </li>
<li>the customer has been advised that the generator should remain switched off until any metering upgrades are complete to avoid potential metering and billing issues. </li></ul></li>
<li>You have attached a signed prescribed Certificate of Electricity Safety supplied by a registered electrical contractor</li>
<li>You have attached an Electrical Works Request supplied by a registered electrical contractor. (where applicable) </li>
<li> You acknowledge that we will not commence the connection works until the relevant connection charge has been paid</li> 
              </ol>
            </p>
             <p>
              <label>{!! Form::checkbox('acknowledge', 'Accept', isset($questionnaire_data->acknowledge) && $questionnaire_data->acknowledge == 'Accept'  ? true : false, ['oninput' => "this.className=''"] ) !!} Accept</label>
            </p>
            <p><label>Registered Electrical Contractor Name   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            <p><label>REC Number :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Number') }}" /></p>

            @PHP $address = \App\Setting::give_value_of('Address');
            $address =  str_replace("<br/>", "\n", $address); @ENDPHP

            <p><label>Registered address   :  </label> {!! Form::textarea('address', $address ,['oninput' => "this.className=''",'disabled']) !!} </p>
            

            <p><label>Phone number   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('Phone') }}" /></p>

            <p><label>Responsible person   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            @php $image=""; $image = \App\Setting::give_value_of('Signature'); @endphp

            <p><label> Signature :  </label> <img src="{{asset('/img/'.$image)}}" width="100px" height="50px" alt="signature" /> </p>
           
            <p><label>Date :  </label> <input type="text" class="completion_date" oninput="this.className=''" disabled /></p>
          </div>
        
         

          <!-- Circles which indicates the steps of the form: -->
          <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
           <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
              <span class="step"></span>
          </div>

        </div>



<script>

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";

  $('.question-no').html( "Question - "+ (n+1) );

  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");

  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("questionForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  calcPhases();
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");

  // validate the form through current tab
  
  y = x[currentTab].getElementsByTagName("input");
  
  // get the id of the current tab and validate the fields of this tab
  
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    // if (y[i].value == "") {
    //   // add an "invalid" class to the field:
    //   y[i].className += " invalid";
    //   // and set the current valid status to false
    //   valid = false;
    // }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

/*============================================================================*/

//for default values

function getTotalPackage() {
  var totalPackage = {{ $job->total_wt }} / 1000;
  
  $('#solar_new').val(totalPackage);
  $('#solar_total').val(totalPackage);
  
}

function calcPhases() {
  
  var inverter_count = {{ $inverter[0]->quantity }};
  var no_of_phases = {{ $job->no_of_phases }};
  console.log(inverter_count);
  console.log(no_of_phases);
  if (no_of_phases == 3 && inverter_count > 0) {
    var total_wt =  {{ $job->total_wt }};
    var result = (total_wt * inverter_count) / no_of_phases / 1000;

    $('#red_power_rating').val(result.toFixed(2));
    $('#white_power_rating').val(result.toFixed(2));
    $('#blue_power_rating').val(result.toFixed(2));
  } 
  
  
}

$( document ).ready(function() {

    getTotalPackage();

  
          if($('#additional_inverter_1').prop('checked')) {
                 $('#inverter2').show();
                 $('#inverter_count').val('2');
              }
          else {
                $('#inverter_count').val('1');
                $('#inverter2').hide();
                $('#inverter3').hide();
          };
             
          if($('#additional_inverter_2').prop('checked')) {
            $('#inverter3').show();
            $('#inverter_count').val('3');
          }
          else {
            $('#inverter_count').val('2');
            $('#inverter3').hide();
          };
         

          if($('#additional_storage_1').prop('checked')) {
            $('#energyStorageSystem').show();
          }
          else {
            $('#energyStorageSystem').hide();
          };
 
         if ({{ $job->no_of_phases }} < 2) {
            $('#Q-18').hide();
            $('#Q-18').removeClass('tab');
          };

         $('#additional_inverter_1').change(function() {
              if(this.checked) {
                 $('#inverter2').show();
                 $('#inverter_count').val('2');
              }
              else {
                $('#inverter_count').val('1');
                $('#inverter2').hide();
                $('#inverter3').hide();
              }
             
          });

          $('#additional_inverter_2').change(function() {

              if(this.checked) {
                 $('#inverter3').show();
                 $('#inverter_count').val('3');
              }
              else {
                $('#inverter_count').val('2');
                $('#inverter3').hide();
              }
             
          });

          $('#additional_storage_1').change(function() {
              if(this.checked) {
                 $('#energyStorageSystem').show();
              }
              else {
                $('#energyStorageSystem').hide();
              }
          });

          

  });



</script>

