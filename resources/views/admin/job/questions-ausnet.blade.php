
          @PHP $address = \App\Setting::give_value_of('Address');
            $address =  str_replace("<br/>", "\n", $address); @ENDPHP
          <div id="Q-12" class="tab">
            
            <p><label>Embedded Generation Type * :  </label> </p>
              <p>
              <label>{!! Form::checkbox('embedded_generation_type[]', 'Battery Storage', isset($questionnaire_data->embedded_generation_type) && in_array('Battery Storage', $questionnaire_data->embedded_generation_type)   ? true : null, ['oninput' => "this.className=''"] ) !!} Battery Storage   </label>
              <label>{!! Form::checkbox('embedded_generation_type[]', 'Solar', isset($questionnaire_data->embedded_generation_type) && in_array('Solar', $questionnaire_data->embedded_generation_type)  ? true : null, ['oninput' => "this.className=''"] ) !!} Solar    </label>
              <label>{!! Form::checkbox('embedded_generation_type[]', 'Wind', isset($questionnaire_data->embedded_generation_type) && in_array('Wind', $questionnaire_data->embedded_generation_type)  ? true : null, ['oninput' => "this.className=''"] ) !!} Wind    </label>
              <label>{!! Form::checkbox('embedded_generation_type[]', 'Hydro', isset($questionnaire_data->embedded_generation_type) && in_array('Hydro', $questionnaire_data->embedded_generation_type)  ? true : null, ['oninput' => "this.className=''"] ) !!} Hydro    </label>
              <label>{!! Form::checkbox('embedded_generation_type[]', 'Other', isset($questionnaire_data->embedded_generation_type) && in_array('Other', $questionnaire_data->embedded_generation_type)  ? true : null, ['oninput' => "this.className=''"] ) !!} Other   </label>
              </p>
           </div>

          <div id="Q-13" class="tab">
            <p><label>Supply number of Phases:  </label>
              <label>{!! Form::radio('no_of_phases_1', '1', isset($job->no_of_phases) && $job->no_of_phases == 1  ? true : null  ,['oninput' => "this.className=''", 'id' => 'no_of_phases_1']) !!} 1 </label>
              <label>{!! Form::radio('no_of_phases_1', '2',  isset($job->no_of_phases) && $job->no_of_phases == 2  ? true : null ,['oninput' => "this.className=''", 'id' => 'no_of_phases_1']) !!} 2 </label>
              <label>{!! Form::radio('no_of_phases_1', '3',  isset($job->no_of_phases) && $job->no_of_phases == 3  ? true : null ,['oninput' => "this.className=''", 'id' => 'no_of_phases_1']) !!} 3 </label>
            </p>
            <p><label>Inverter Phases:  </label>
              <label>{!! Form::radio('inverter_phase', '1', isset($job->no_of_phases) && $job->no_of_phases == 1  ? true : null  ,['oninput' => "this.className=''"]) !!} 1 </label>
              <label>{!! Form::radio('inverter_phase', '2',  isset($job->no_of_phases) && $job->no_of_phases == 2  ? true : null ,['oninput' => "this.className=''"]) !!} 2 </label>
              <label>{!! Form::radio('inverter_phase', '3',  isset($job->no_of_phases) && $job->no_of_phases == 3  ? true : null ,['oninput' => "this.className=''"]) !!} 3 </label>
            </p>
          </div>

          <div id="Q-14" class="tab">
            <p><label>Does the system include battery storage:  </label>
              <label>{!! Form::radio('include_battery_storage', 'Yes AC coupled',   isset($questionnaire_data->include_battery_storage) && $questionnaire_data->include_battery_storage == 'Yes AC coupled'  ? true : null  ,['oninput' => "this.className=''"]) !!} Yes AC coupled  </label>
              <label>{!! Form::radio('include_battery_storage', 'Yes DC coupled',  isset($questionnaire_data->include_battery_storage) && $questionnaire_data->include_battery_storage == 'Yes DC coupled'  ? true : null  ,['oninput' => "this.className=''"]) !!} Yes DC coupled </label>
              <label>{!! Form::radio('include_battery_storage', 'No',  isset($questionnaire_data->include_battery_storage) && $questionnaire_data->include_battery_storage == 'No'  ? true : null  ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            
          </div>

          <div id="Q-15" class="tab">
            <p><label>Is an existing embedded Generator installed:  </label>
              <label>{!! Form::radio('embedded_generator_installed', 'Yes', isset($questionnaire_data->embedded_generator_installed) && $questionnaire_data->embedded_generator_installed == 'Yes'  ? true : null  ,['oninput' => "this.className=''", 'id' => 'embedded_generator_installed']) !!} Yes  </label>
              <label>{!! Form::radio('embedded_generator_installed', 'No', isset($questionnaire_data->embedded_generator_installed) && $questionnaire_data->embedded_generator_installed == 'No'  ? true : null ,['oninput' => "this.className=''", 'id' => 'embedded_generator_installed_1']) !!} No </label>
            </p>
            <div id="embedded_generator" style="display:none">
            <p><label>Existing rating  </label> <input type="text" oninput="this.className=''" name="existing_rating" id="existing_rating" value=" {{$questionnaire_data->existing_rating or ''}}" /> kW</p>
            <p><label>Existing export limit </label> <input type="text" oninput="this.className=''" name="existing_export_limit" id="existing_export_limit" value="{{$questionnaire_data->existing_export_limit or ''}}" /> kW</p>
            </div>
          </div>
          
          <div id="Q-16" class="tab">
            <p><label>Pre-Approval Reference Number: </label> <input type="text" oninput="this.className=''" name="pre_approval_number" id="pre_approval_number" value="{{$job->pre_approval_number or ''}}" /> </p>
          </div>

          <div id="Q-17" class="tab">
            <p><label> Installer Details </label></p>
            <p><label>Company: </label> <input type="text" oninput="this.className=''" name="company_name" id="company_name" value="{{ \App\Setting::give_value_of('Company_Name') }}" /> </p>
            <p><label>Address: </label> {!! Form::textarea('address', $address ,['oninput' => "this.className=''",'disabled']) !!}  </p>
            <p><label>Telephone: </label> <input type="text" oninput="this.className=''" name="Telephone" id="Telephone" value="{{ \App\Setting::give_value_of('Phone') }}" /> </p>
            <p><label>E-mail: </label> <input type="text" oninput="this.className=''" name="Email" id="Email" value="{{ \App\Setting::give_value_of('Email') }}" /> </p>
            <p><label>Name: </label> <input type="text" oninput="this.className=''" name="REC_Name" id="REC_Name" value="{{ \App\Setting::give_value_of('REC_Name') }}" /> </p>


          </div>

          <div id="Q-18" class="tab">
            <p><label>Total Installed Capacity (Inverter rating): </label> <input type="text" oninput="this.className=''" name="total_capacity_installed" id="total_capacity_installed" value="{{ $inverter[0]->rating_inverter or ''}}" /> </p>
            <hr />
            <p><label>Total Installed capacity per phase: </label> </p>
            <p><label>Phase A: </label> <input type="text" oninput="this.className=''" name="phase_a" id="phase_a" value="{{$questionnaire_data->phase_a or ''}}" /> </p>
            <p><label>Phase B: </label> <input type="text" oninput="this.className=''" name="phase_b" id="phase_b" value="{{$questionnaire_data->phase_b or ''}}" /> </p>
            <p><label>Phase C: </label> <input type="text" oninput="this.className=''" name="phase_c" id="phase_c" value="{{$questionnaire_data->phase_c or ''}}" /> </p>
             <hr />
            <p><label>Export Limit : </label> <input type="number" oninput="this.className=''" name="export_limit" id="export_limit" value="{{$questionnaire_data->export_limit or ''}}" /> </p>
            <div id="confirm_sop_33_06_div" style="display:none">
            <p>
              <label>Confirm Compliance with Procedure SOP 33-06: </label>
              <label>{!! Form::radio('compliance_sop_33_06', 'Yes',  isset($questionnaire_data->compliance_sop_33_06) && $questionnaire_data->compliance_sop_33_06 == 'Yes'  ? true : null , ['oninput' => "this.className=''"]) !!} Yes </label>
              <label>{!! Form::radio('compliance_sop_33_06', 'No',  isset($questionnaire_data->compliance_sop_33_06) && $questionnaire_data->compliance_sop_33_06 == 'No'  ? true : null  , ['oninput' => "this.className=''"]) !!} No </label>
            </p>
            </div>
          
          </div>

          <div id="Q-19" class="tab">
            <p><label>Inverter Manufacturer: </label> <input type="text" oninput="this.className=''" name="inverter_manufacturer" id="inverter_manufacturer" value="{{ $inverter[0]->manufacturer or ''}}" /> </p>
            <p><label>Inverter Model Name: </label> <input type="text" oninput="this.className=''" name="inverter_model" id="inverter_model" value="{{ $inverter[0]->model or ''}}" /> </p>
            <p><label>Rating of Inverter: </label> <input type="text" oninput="this.className=''" name="inverter_rating" id="inverter_rating" value="{{ $inverter[0]->rated_power_output or ''}}" /> kW</p>
            <p><label>Total Watt: </label> <input type="text" oninput="this.className=''" name="inverter_total_watt" id="inverter_total_watt" value="{{ $inverter[0]->rating_inverter or ''}}" /> kW</p>

          
          </div>

          <div id="Q-20" class="tab">
            <p>
              <label>Is a battery being installed: </label>
              <label>{!! Form::radio('storage_installed', 'Yes',  isset($questionnaire_data->storage_installed) && $questionnaire_data->storage_installed == 'Yes'  ? true : null, ['oninput' => "this.className=''"]) !!} Yes </label>
              <label>{!! Form::radio('storage_installed', 'No',  isset($questionnaire_data->storage_installed) && $questionnaire_data->storage_installed == 'No'  ? true : null , ['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Battery details: </label>  </p>
            <p><label>Manufacturer: </label> <input type="text" oninput="this.className=''" name="storage_manufacture" id="storage_manufacture" value="{{$questionnaire_data->storage_manufacture or 'N/A'}}" /> </p>
            <p><label>Model Name: </label> <input type="text" oninput="this.className=''" name="storage_model" id="storage_model" value="{{$questionnaire_data->storage_model or 'N/A'}}" /> </p>
            <p><label>AC Power Rating : </label> <input type="text" oninput="this.className=''" name="storage_rating" id="storage_rating" value="{{$questionnaire_data->storage_rating or 'N/A'}}" /> kWh </p>
            <p><label>Rated Storage Capacity : </label> <input type="text" oninput="this.className=''" name="storage_capacity" id="storage_capacity" value="{{$questionnaire_data->storage_capacity or 'N/A'}}" /> kWh </p>
            <p><label>Type: </label><label>{!! Form::radio('storage_type', 'Lead-Acid',  isset($questionnaire_data->storage_type) && $questionnaire_data->storage_type == 'Lead-Acid'  ? true : null  ,['oninput' => "this.className=''"]) !!} Lead-Acid   </label>
              <label>{!! Form::radio('storage_type', 'Lithium-ion',  isset($questionnaire_data->storage_type) && $questionnaire_data->storage_type == 'Lithium-ion'  ? true : null ,['oninput' => "this.className=''"]) !!} Lithium-ion  </label>
              <label>{!! Form::radio('storage_type', 'Flow',  isset($questionnaire_data->storage_type) && $questionnaire_data->storage_type == 'Flow'  ? true : null ,['oninput' => "this.className=''"]) !!} Flow </label>
              <label>{!! Form::radio('storage_type', 'Salt Water',isset($questionnaire_data->storage_type) && $questionnaire_data->storage_type == 'Salt Water'  ? true : null ,['oninput' => "this.className=''"]) !!} Salt Water </label>
              <label>{!! Form::radio('storage_type', 'Other', isset($questionnaire_data->storage_type) && $questionnaire_data->storage_type == 'Other'  ? true : null ,['oninput' => "this.className=''"]) !!} Other </label>
            </p>
            <p><label>Other: </label> <input type="text" oninput="this.className=''" name="other_type" id="other_type" value="{{$questionnaire_data->other_type or 'N/A'}}" /> </p>
            
          </div>

          
          <div id="Q-21" class="tab">
            
            <p><label>Installer details: </label>  </p>
            <p><label>Name: </label> <input type="text" oninput="this.className=''" name="REC_ResponsiblePerson" id="REC_ResponsiblePerson" value="{{ $installer->name }}" /> </p>
            <p><label>Accreditation No.: </label> <input type="text" oninput="this.className=''" name="REC_Number" id="REC_Number" value="{{ $installer->accredition_number }}" /> </p>
            <p><label>Electrical license number.: </label> <input type="text" oninput="this.className=''" name="electrical_licence_number" id="electrical_licence_number" value="{{ $installer->electrical_licence_number }}" /> </p>
             @php $image=""; $image = $installer->image; @endphp

            <p><label>Signature: </label> <img src="{{url('/user/'.$image)}}" width="100px" height="50px" alt="signature" /> </p>
            <p><label>Date: </label> <input type="text" class="completion_date" oninput="this.className=''"  /> </p>
            
          </div>

          <div id="Q-22" class="tab">

            <p><label>Are all inverter ‘Approved inverters’ From Clean Energy Council Approved Inverter list? </label> 
              <label>{!! Form::radio('clean_energy_approved', 'Yes', isset($questionnaire_data->clean_energy_approved) && $questionnaire_data->clean_energy_approved == 'Yes'  ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('clean_energy_approved', 'No', isset($questionnaire_data->clean_energy_approved) && $questionnaire_data->clean_energy_approved == 'No'  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Operating Manual Provided to Customer: </label> 
              <label>{!! Form::radio('manual_to_customer', 'Yes', (isset($job->manual_to_customer) && $job->manual_to_customer == "Yes" ) || $job->manual_to_customer == "" ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('manual_to_customer', 'No', isset($job->manual_to_customer) && $job->manual_to_customer == "No"  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Instructed Customer in Operation of System: </label> 
              <label>{!! Form::radio('instructed_customer', 'Yes', (isset($job->instructed_customer) && $job->instructed_customer == "Yes" ) || $job->instructed_customer == "" ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('instructed_customer', 'No', isset($job->instructed_customer) && $job->instructed_customer == "No"  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
          </div>
          <div id="Q-23" class="tab">
           <p><label>Inverter Test Records </label> </p>
           <p><label>Test 1: Time for inverter/s to disconnect </label>
            {{ Form::select('test_1_time', [1 => '1',2,3,4,5,6,7,8,9,10], isset($questionnaire_data->test_1_time)  ? $questionnaire_data->test_1_time : null ) }}
           
           seconds (Must be < 2 seconds to pass) </p>
            <p><label>Result: </label> 
              <label>{!! Form::radio('test_1_result', 'Pass',  isset($questionnaire_data->test_1_result) && $questionnaire_data->test_1_result == 'Pass'  ? true : null  ,['oninput' => "this.className=''"]) !!} Pass  </label>
              <label>{!! Form::radio('test_1_result', 'Fail',  isset($questionnaire_data->test_1_result) && $questionnaire_data->test_1_result == 'Fail'  ? true : null ,['oninput' => "this.className=''"]) !!} Fail </label>
            </p>
            <p><label>Test 2: Time for inverter to reconnect  </label> 
              {{ Form::select('test_2_time', [1 => '1',2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70], isset($questionnaire_data->test_2_time)  ? $questionnaire_data->test_2_time : null) }}
              
              </select> seconds (Must be > 60 seconds to pass) </p>
            <p><label>Result: </label> 
              <label>{!! Form::radio('test_2_result', 'Pass',  isset($questionnaire_data->test_2_result) && $questionnaire_data->test_2_result == 'Pass'  ? true : null  ,['oninput' => "this.className=''"]) !!} Pass  </label>
              <label>{!! Form::radio('test_2_result', 'Fail',  isset($questionnaire_data->test_2_result) && $questionnaire_data->test_2_result == 'Fail'  ? true : null ,['oninput' => "this.className=''"]) !!} Fail </label>
            </p>
          </div>
          <div id="Q-24" class="tab">
            <p>
              <label>Limited Export Commissioning test has been conducted in accordance with SOP 33-06:  </label>
              <label>{!! Form::radio('limited_export_commissioning', 'Yes',  isset($questionnaire_data->limited_export_commissioning) && $questionnaire_data->limited_export_commissioning == 'Yes'  ? true : null, ['oninput' => "this.className=''", 'id' => 'limited_export_commissioning']) !!} Yes </label>
              <label>{!! Form::radio('limited_export_commissioning', 'No',  isset($questionnaire_data->limited_export_commissioning) && $questionnaire_data->limited_export_commissioning == 'No'  ? true : null , ['oninput' => "this.className=''", 'id' => 'limited_export_commissioning']) !!} No </label>
              <p><label>Measurement </label> <input type="text" oninput="this.className=''" name="SOP_Time" id="SOP_Time" value="{{$questionnaire_data->SOP_Time or ''}}" /> seconds</p>
            </p>
          </div>
          <div id="Q-25" class="tab">
            <p><label>By signing this agreement, you acknowledge and represent that the information provided is correct and that</label></p>
            <p>
              <ul>
                <li>the minimum requirements set out for EG systems up to 3.5 kW/phase (SWER System) up to 5kW/phase (on a Single Phase) and 5kW/phase (on a Three Phase System) have been met</li>
                <li>the EG complies with the Electricity Safety Act 1998 (Vic) and associated Safety Regulations, the Electricity Distribution Code, the Victorian Service & Installation Rules, AS/NZS3000 (Wiring Rules) and AS4777.2:2015 (Grid Connection of Energy Systems via Inverters), and any other relevant Acts, regulations, standards or guidelines;</li>
                <li>the EG is connected to a dedicated circuit complete with lockable isolating switch at the switchboard;</li>
                <li>the main switchboard, isolating fuse/switch/circuit breaker are labelled correctly</li>
                <li>alternative supply signage has been installed;</li>
                <li>commissioning tests as specified in the Service & Installation Rules, by AusNet Services, and in section 2 of this agreement have been completed and passed; </li>
                <li>a Prescribed Certificate of Electrical Safety (CES) has been obtained with copies of the Electrical Works Request and CES to be sent to the EG owner’s Retailer and a copy of this agreement is to be sent directly to AusNet Services; and</li>
                <li>the EG owner has been advised that the EG should remain switched off until any metering upgrades are complete to avoid potential metering and billing issues</li>
                
              </ul>
            </p>
            <p>
              <label>{!! Form::checkbox('max_demand_installation', 'New',(isset($job->max_demand_installation) && $job->max_demand_installation != '' && $job->max_demand_installation == 'New') ? true : false, ['oninput' => "this.className=''"] ) !!} Accept</label>
            </p>
          <p><label>Registered Electrical Contractor Name   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            <p><label>REC Number :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Number') }}" /></p>

            @PHP $address = \App\Setting::give_value_of('Address');
            $address =  str_replace("<br/>", "\n", $address); @ENDPHP

            <p><label>Registered address   :  </label> {!! Form::textarea('address', $address ,['oninput' => "this.className=''",'disabled']) !!} </p>
            

            <p><label>Phone number   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('Phone') }}" /></p>

            <p><label>Responsible person   :  </label> <input type="text"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            @php $image=""; $image = \App\Setting::give_value_of('Signature'); @endphp

            <p><label> Signature :  </label> <img src="{{asset('/img/'.$image)}}" width="100px" height="50px" alt="signature" /> </p>
           
            <p><label>Date :  </label> <input type="text" class="completion_date" oninput="this.className=''" disabled /></p>
          </div>
          <div id="Q-26" class="tab">
          <p><label>Customer Details </label> </p>
           <p><label>Supply Address: </label> {!! Form::textarea('customer_address', $customer->unit.' '.$customer->street_number.' '.$customer->street_name.' '.$customer->lot.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code ,['oninput' => "this.className=''",'disabled']) !!} </p>
           <p><label>Generator Owner Name: </label> <input type="text" oninput="this.className=''" name="customer_name" id="customer_name" value="{{ $customer->first_name.' '.$customer->last_name }} " /> </p>
            <p><label>Customer NMI: </label> <input type="text" oninput="this.className=''" name="customer_nmi" id="customer_nmi" value="{{ $customer->nmi }}" /> </p>
            <p><label>E-mail Address: </label> <input type="text" oninput="this.className=''" name="customer_email" id="customer_email" value="{{ $customer->email }}" /> </p>
           <p><label>Telephone Number (Business Hours): </label> <input type="text" oninput="this.className=''" name="customer_cell" id="customer_cell" value="{{ $customer->phone_mobile }}" /> </p>
            <p><label>Telephone Number (After Hours): </label> <input type="text" oninput="this.className=''" name="customer_homephone" id="customer_homephone" value="{{ $customer->phone_home }}" /> </p>
            <p><label>Mailing Address: </label> <input type="text" oninput="this.className=''" name="mail_address" id="mail_address" value="" /> </p>
          </div>

          

          <!-- Circles which indicates the steps of the form: -->
          <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
          </div>

        </div>


<script>

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";

  $('.question-no').html( "Question - "+ (n+1) );

  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");

  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("questionForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
  
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");

  // validate the form through current tab
  
  y = x[currentTab].getElementsByTagName("input");
  
  // get the id of the current tab and validate the fields of this tab
  
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    // if (y[i].value == "") {
    //   // add an "invalid" class to the field:
    //   y[i].className += " invalid";
    //   // and set the current valid status to false
    //   valid = false;
    // }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

/*============================================================================*/

//for default values

function getTotalPackage() {
  var totalPackage = {{ $inverter[0]->rating_inverter }};
  
  $('#total_capacity_installed').val(totalPackage);
  var radioValue = $("input[name='no_of_phases']:checked").val();

  if (radioValue == 3) {
    var result = (totalPackage/3);
    $('#phase_a').val(result.toFixed(2));
    $('#phase_b').val(result.toFixed(2));
    $('#phase_c').val(result.toFixed(2));
  }
  else if (radioValue == 2) {
    var result = (totalPackage/2);
    $('#phase_a').val(result.toFixed(2));
    $('#phase_b').val(result.toFixed(2));
    $('#phase_c').val("N/A");
  }
  else {
    $('#phase_a').val(totalPackage);
    $('#phase_b').val("N/A");
    $('#phase_c').val("N/A");
  }

}



$( document ).ready(function() {

          var contact_project_group =  $('input[name=contact_project_group]:checked').val();
          if(contact_project_group == "No"){
            $(".project_number_class").hide()          
          }else{
            $(".project_number_class").show()
          } 

          
          //getTotalPackage();

          var radioValue = $("input[name='embedded_generator_installed']:checked").val();
          console.log(radioValue);
          if(radioValue == "Yes") {
             $('#embedded_generator').show();
          }
          else {
            $('#embedded_generator').hide();
          }

          if ($.isNumeric($('#export_limit').val())) {
            console.log($('#export_limit').val());
             $('#confirm_sop_33_06_div').show();
          }
          else {
            $('input:radio[name=compliance_sop_33_06]').each(function () { $(this).prop('checked', false); });
            $('#confirm_sop_33_06_div').hide();

          }
  });

  $('#embedded_generator_installed').change(function() {
      var radioValue = $("input[name='embedded_generator_installed']:checked").val();
      console.log(radioValue);
      if(radioValue == "Yes") {
         $('#embedded_generator').show();
      }
      else {
        $('#embedded_generator').hide();
      }
     
  });

  $('#embedded_generator_installed_1').change(function() {
      var radioValue = $("input[name='embedded_generator_installed']:checked").val();
      console.log(radioValue);
      $('#embedded_generator').hide();
    
  });

  $('[id^=no_of_phases]').change(function() {
      //getTotalPackage();
  });

  $('[id^=limited_export_commissioning]').change(function() {
      var radioValue = $("input[name='limited_export_commissioning']:checked").val();
      if(radioValue == "Yes") {
          toastr.success("Please complete the SOP 33-06 and submit to Green Sky Australia Admin");
      }
    
     
  });

  

  $('#export_limit').change(function() {
      if ($.isNumeric($('#export_limit').val())) {

         $('#confirm_sop_33_06_div').show();
      }
      else {
        $('#confirm_sop_33_06_div').hide();
        $('input:radio[name=compliance_sop_33_06]').each(function () { $(this).prop('checked', false); });
      }

  });

  
  
 $('.completion_date').datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: new Date(),    
        });
    


</script>
