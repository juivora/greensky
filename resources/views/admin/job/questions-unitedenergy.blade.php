
                @PHP $address = \App\Setting::give_value_of('Address');
            $address =  str_replace("<br/>", "\n", $address); @ENDPHP

          <div id="Q-12" class="tab">
            
            <p><label>Installation Type * :  </label> </p>
              <p>
              <label>{!! Form::radio('connection_type', 'New', (isset($job->connection_type) && $job->connection_type == "New" ) || $job->connection_type == "" ? true : null  ,['oninput' => "this.className=''"]) !!} New  </label>
              <label>{!! Form::radio('connection_type', 'Alteration', isset($job->connection_type) && $job->connection_type == "Alteration"  ? true : null ,['oninput' => "this.className=''"]) !!} Alteration </label>
              <label>{!! Form::radio('connection_type', 'Abolishment', isset($job->acceptance_of_charge) && $job->connection_type == "Abolishment"  ? true : null ,['oninput' => "this.className=''"]) !!} Abolishment </label> 
              </p>
               <p><label>Battery storage? * :  </label> </p>
              <p>
                
              <label>{!! Form::radio('connection_type_1', 'Yes / Seperate',isset($questionnaire_data->connection_type_1) && $questionnaire_data->connection_type_1 == 'Yes / Seperate'  ? true : null ,['oninput' => "this.className=''"]) !!} Yes / Seperate  </label>
              <label>{!! Form::radio('connection_type_1', 'No / Combined',isset($questionnaire_data->connection_type_1) && $questionnaire_data->connection_type_1 == 'No / Combined'  ? true : null ,['oninput' => "this.className=''"]) !!} No / Combined </label>
              <label>{!! Form::radio('connection_type_1', 'No Storage System', isset($questionnaire_data->connection_type_1) && $questionnaire_data->connection_type_1 == 'No Storage System'  ? true : null,['oninput' => "this.className=''"]) !!} No Storage System </label></p>
            <!--<p><label>CES Number  * :  </label> <input type="text" oninput="this.className=''" name="ces_number" value="{{ \App\Setting::give_value_of('CES_Number')  }}" disabled/></p>
            <p><label>CES Completion Date * :  </label> <input type="text" id="ces_completion_date" oninput="this.className=''" name="ces_completion_date" value="{{ $job->ces_completion_date or null }}" /></p>-->

          </div>

          <div id="Q-13" class="tab">
            <p><label>Operating Manual Provided to customer *:  </label>
              <label>{!! Form::radio('manual_to_customer', 'Yes', (isset($job->manual_to_customer) && $job->manual_to_customer == "Yes" ) || $job->manual_to_customer == "" ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('manual_to_customer', 'No', isset($job->manual_to_customer) && $job->manual_to_customer == "No"  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Instructed customer in Operating of System *:  </label> 
              <label>{!! Form::radio('instructed_customer', 'Yes', (isset($job->instructed_customer) && $job->instructed_customer == "Yes" ) || $job->instructed_customer == "" ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('instructed_customer', 'No', isset($job->instructed_customer) && $job->instructed_customer == "No"  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>


          </div>

          <div id="Q-14" class="tab">
            <p><label>Solar PV </label> </p>
            
            <p><label>Net new capacity installed / removed:</label> <input type="text" oninput="this.className=''" name="solar_pv_net" id="solar_pv_net" value="{{$questionnaire_data->solar_pv_net or ''}}" /> kW</p>
            <p><label>Total installed capacity after all works: </label> <input type="text" oninput="this.className=''" name="solar_total_cap" id="solar_total_cap" value="{{$questionnaire_data->solar_total_cap or ''}}" /> kW</p>
            <hr />
            <p><label>Wind </label> </p>
            <p><label>Net new capacity installed / removed:</label> <input type="text" oninput="this.className=''" name="wind_pv_net" id="wind_pv_net" value="{{$questionnaire_data->wind_pv_net or '0.00'}}" /> kW</p>
            <p><label>Total installed capacity after all works: </label> <input type="text" oninput="this.className=''" name="wind_total_cap" id="wind_total_cap" value="{{$questionnaire_data->wind_total_cap or '0.00'}}" /> kW</p>
            <hr />
            <p><label>Other </label> </p>
            <p><label>Net new capacity installed / removed:</label> <input type="text" oninput="this.className=''" name="Other_pv_net" id="Other_pv_net" value="{{$questionnaire_data->Other_pv_net or '0.00'}}" /> kW</p>
            <p><label>Total installed capacity after all works: </label> <input type="text" oninput="this.className=''" name="Other_total_cap" id="Other_total_cap" value="{{$questionnaire_data->Other_total_cap or '0.00'}}" /> kW</p>
            <hr />
            <p><label>Energy Storage </label> </p>
            <p><label>Net new capacity installed / removed:</label> <input type="text" oninput="this.className=''" name="energy_storage_pv_net" id="energy_storage_pv_net" value="{{$questionnaire_data->energy_storage_pv_net or ''}}" /> kW</p>
            <p><label>Total installed capacity after all works: </label> <input type="text" oninput="this.className=''" name="energy_storage_total_cap" id="energy_storage_total_cap" value="{{$questionnaire_data->energy_storage_total_cap or ''}}" /> kW</p>
      
          </div>

          <div id="Q-16" class="tab">
            <input type="hidden" id="inverter_count" name="inverter_count" value="1">
            <p><label>Inverter Manufacturer: </label> <input type="text" oninput="this.className=''" name="inverter_manufacturer_1" id="inverter_manufacturer_1" value="{{ isset($questionnaire_data->inverter_manufacturer_1) ? $questionnaire_data->inverter_manufacturer_1 : ( isset($inverter[0]->manufacturer) ? $inverter[0]->manufacturer : '' ) }}" /> </p>
            <p><label>Inverter Model Name: </label> <input type="text" oninput="this.className=''" name="inverter_model_1" id="inverter_model_1" value="{{ isset($questionnaire_data->inverter_model_1) ? $questionnaire_data->inverter_model_1 : ( isset($inverter[0]->model) ? $inverter[0]->model : '' ) }}" /> </p>
            <p><label>Rating of Inverter: </label> <input type="text" oninput="this.className=''" name="inverter_rating_1" id="inverter_rating_1" value="{{ isset($questionnaire_data->inverter_rating_1) ? $questionnaire_data->inverter_rating_1 : ( isset($inverter[0]->rating_inverter) ? $inverter[0]->rating_inverter : '' ) }}" /> kW</p>
            <p><label>Inverter AS4777 Compliant  : </label> 
             
              <label>{!! Form::radio('AS4777_complaint_1', 'Yes',  isset($questionnaire_data->AS4777_complaint_1) ? ($questionnaire_data->AS4777_complaint_1 == "Yes" ? true : null) : ((isset($inverter[0]->as4777_compliant) && $inverter[0]->as4777_compliant == "Yes" ) || $inverter[0]->as4777_compliant == "" ? true : null) ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('AS4777_complaint_1', 'No', isset($questionnaire_data->AS4777_complaint_1) ? ($questionnaire_data->AS4777_complaint_1 == "No" ? true : null) : ((isset($inverter[0]->as4777_compliant) && $inverter[0]->as4777_compliant == "No" ) || $inverter[0]->as4777_compliant == "" ? true : null)  ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Status: </label><label>{!! Form::radio('inverter_status_1', 'New', isset($questionnaire_data->inverter_status_1) && $questionnaire_data->inverter_status_1 == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('inverter_status_1', 'Existing retained', isset($questionnaire_data->inverter_status_1) && $questionnaire_data->inverter_status_1 == 'Existing retained'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('inverter_status_1', 'Existing Removed', isset($questionnaire_data->inverter_status_1) && $questionnaire_data->inverter_status_1 == 'Existing Removed'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            <p><label>Inverter Energy Source: </label><label>{!! Form::radio('inverter_energy_source_1', 'Solar Only', isset($questionnaire_data->inverter_energy_source_1) && $questionnaire_data->inverter_energy_source_1 == 'Solar Only'  ? true : null  ,['oninput' => "this.className=''"]) !!} Solar Only   </label>
              <label>{!! Form::radio('inverter_energy_source_1', 'Storage only', isset($questionnaire_data->inverter_energy_source_1) && $questionnaire_data->inverter_energy_source_1 == 'Storage only'  ? true : null ,['oninput' => "this.className=''"]) !!} Storage only   </label>
              <label>{!! Form::radio('inverter_energy_source_1', 'Solar & Storage', isset($questionnaire_data->inverter_energy_source_1) && $questionnaire_data->inverter_energy_source_1 == 'Solar & Storage'  ? true : null ,['oninput' => "this.className=''"]) !!} Solar & Storage </label>
            </p>
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Number_1" id="CAC_Number_1" value="{{ isset($questionnaire_data->CAC_Number_1) ? $questionnaire_data->CAC_Number_1 : (isset($inverter[0]->cac_number) ?  $inverter[0]->cac_number  : '')  }}" /> </p>
            <p><label>Additional Inverter : </label> <label>{!! Form::checkbox('additional_inverter_1','Yes', isset($questionnaire_data->additional_inverter_1) && $questionnaire_data->additional_inverter_1 == 'Yes'  ? true : null , ['oninput' => "this.className=''", 'id' => 'additional_inverter_1'] ) !!}  </label> </p>
            <div id="inverter2" style="display:none" >
            <hr />
            <p><label>Inverter Manufacturer: </label> <input type="text" oninput="this.className=''" name="inverter_manufacturer_2" id="inverter_manufacturer_2" value="{{ isset($questionnaire_data->inverter_manufacturer_2) ? $questionnaire_data->inverter_manufacturer_2 : ( isset($inverter[1]->manufacturer) ? $inverter[1]->manufacturer : '' ) }}" /> </p>
            <p><label>Inverter Model Name: </label> <input type="text" oninput="this.className=''" name="inverter_model_2" id="inverter_model_2" value="{{ isset($questionnaire_data->inverter_model_2) ? $questionnaire_data->inverter_model_2 : ( isset($inverter[1]->model) ? $inverter[1]->model : '' ) }}" /> </p>
            <p><label>Rating of Inverter: </label> <input type="text" oninput="this.className=''" name="inverter_rating_2" id="inverter_rating_2" value="{{ isset($questionnaire_data->inverter_rating_2) ? $questionnaire_data->inverter_rating_2 : ( isset($inverter[1]->rating_inverter) ? $inverter[1]->rating_inverter : '' ) }}" /> kW</p>
            <p><label>Inverter AS4777 Compliant  : </label> 
              <label>{!! Form::radio('AS4777_complaint_2', 'Yes',isset($questionnaire_data->AS4777_complaint_2) ? ($questionnaire_data->AS4777_complaint_2 == "Yes" ? true : null) : (isset($inverter[1]) ? ((isset($inverter[1]->as4777_compliant) && $inverter[1]->as4777_compliant == "Yes" ) || $inverter[1]->as4777_compliant == "" ? true : null) : null) ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('AS4777_complaint_2', 'No', isset($questionnaire_data->AS4777_complaint_2) ? ($questionnaire_data->AS4777_complaint_2 == "No" ? true : null) :  (isset($inverter[1]) ? ((isset($inverter[1]->as4777_compliant) && $inverter[1]->as4777_compliant == "No" ) || $inverter[1]->as4777_compliant == "" ? true : null) : null)  ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Status: </label><label>{!! Form::radio('inverter_status_2', 'New',  isset($questionnaire_data->inverter_status_2) && $questionnaire_data->inverter_status_2 == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('inverter_status_2', 'Existing retained', isset($questionnaire_data->inverter_status_2) && $questionnaire_data->inverter_status_2 == 'Existing retained'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('inverter_status_2', 'Existing Removed', isset($questionnaire_data->inverter_status_2) && $questionnaire_data->inverter_status_2 == 'Existing Removed'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            <p><label>Inverter Energy Source: </label><label>{!! Form::radio('inverter_energy_source_2', 'Solar Only',  isset($questionnaire_data->inverter_energy_source_2) && $questionnaire_data->inverter_energy_source_2 == 'Solar Only'  ? true : null  ,['oninput' => "this.className=''"]) !!} Solar Only   </label>
              <label>{!! Form::radio('inverter_energy_source_2', 'Storage only', isset($questionnaire_data->inverter_energy_source_2) && $questionnaire_data->inverter_energy_source_2 == 'Storage only'  ? true : null  ,['oninput' => "this.className=''"]) !!} Storage only   </label>
              <label>{!! Form::radio('inverter_energy_source_2', 'Solar & Storage', isset($questionnaire_data->inverter_energy_source_2) && $questionnaire_data->inverter_energy_source_2 == 'Solar & Storage'  ? true : null  ,['oninput' => "this.className=''"]) !!} Solar & Storage </label>
            </p>
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Number_2" id="CAC_Number_2" value="{{ isset($questionnaire_data->CAC_Number_2) ? $questionnaire_data->CAC_Number_2 :( isset($inverter[1]->cac_number) ?  $inverter[1]->cac_number  : '')  }}" /> </p>
            <p><label>Additional Inverter : </label> <label>{!! Form::checkbox('additional_inverter_2','Yes', isset($questionnaire_data->additional_inverter_2) && $questionnaire_data->additional_inverter_2 == 'Yes'  ? true : null, ['oninput' => "this.className=''", 'id' => 'additional_inverter_2'] ) !!}  </label> </p> </p>
            </div>
            <div id="inverter3" style="display:none" >
            <hr />
            <p><label>Inverter Manufacturer: </label> <input type="text" oninput="this.className=''" name="inverter_manufacturer_3" id="inverter_manufacturer_3" value="{{ isset($questionnaire_data->inverter_manufacturer_3) ? $questionnaire_data->inverter_manufacturer_3 : ( isset($inverter[2]->manufacturer) ? $inverter[2]->manufacturer : '' ) }}" /> </p>
            <p><label>Inverter Model Name: </label> <input type="text" oninput="this.className=''" name="inverter_model_3" id="inverter_model_3" value="{{ isset($questionnaire_data->inverter_model_3) ? $questionnaire_data->inverter_model_3 : ( isset($inverter[2]->model) ? $inverter[2]->model : '' ) }}" /> </p>
            <p><label>Rating of Inverter: </label> <input type="text" oninput="this.className=''" name="inverter_rating_3" id="inverter_rating_3" value="{{ isset($questionnaire_data->inverter_rating_3) ? $questionnaire_data->inverter_rating_3 : ( isset($inverter[2]->rating_inverter) ? $inverter[2]->rating_inverter : '' ) }}" /> kW</p>
            <p><label>Inverter AS4777 Compliant  : </label> 
              <label>{!! Form::radio('AS4777_complaint_3', 'Yes', isset($questionnaire_data->AS4777_complaint_3) ? ($questionnaire_data->AS4777_complaint_3 == "Yes" ? true : null) : (isset($inverter[2]) ? ((isset($inverter[2]->as4777_compliant) && $inverter[2]->as4777_compliant == "Yes" ) || $inverter[2]->as4777_compliant == "" ? true : null) : null)  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('AS4777_complaint_3', 'No', isset($questionnaire_data->AS4777_complaint_3) ? ($questionnaire_data->AS4777_complaint_3 == "No" ? true : null) : (isset($inverter[2]) ? ((isset($inverter[2]->as4777_compliant) && $inverter[2]->as4777_compliant == "No" ) || $inverter[2]->as4777_compliant == "" ? true : null) : null) ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Status: </label><label>{!! Form::radio('inverter_status_3', 'New', isset($questionnaire_data->inverter_status_3) && $questionnaire_data->inverter_status_3 == 'New'  ? true : null   ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('inverter_status_3', 'Existing retained',  isset($questionnaire_data->inverter_status_3) && $questionnaire_data->inverter_status_3 == 'Existing retained'  ? true : null  ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('inverter_status_3', 'Existing Removed',  isset($questionnaire_data->inverter_status_3) && $questionnaire_data->inverter_status_3 == 'Existing Removed'  ? true : null  ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            <p><label>Inverter Energy Source: </label><label>{!! Form::radio('inverter_energy_source_3', 'Solar Only',  isset($questionnaire_data->inverter_energy_source_3) && $questionnaire_data->inverter_energy_source_3 == 'Solar Only'  ? true : null  ,['oninput' => "this.className=''"]) !!} Solar Only  </label>
              <label>{!! Form::radio('inverter_energy_source_3', 'Storage only', isset($questionnaire_data->inverter_energy_source_3) && $questionnaire_data->inverter_energy_source_3 == 'Storage only'  ? true : null   ,['oninput' => "this.className=''"]) !!} Storage only   </label>
              <label>{!! Form::radio('inverter_energy_source_3', 'Solar & Storage', isset($questionnaire_data->inverter_energy_source_3) && $questionnaire_data->inverter_energy_source_3 == 'Solar & Storage'  ? true : null   ,['oninput' => "this.className=''"]) !!} Solar & Storage </label>
            </p>
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Number_3" id="CAC_Number_3" value="{{ isset($questionnaire_data->CAC_Number_3) ? $questionnaire_data->CAC_Number_3 : (isset($inverter[2]->cac_number) ?  $inverter[2]->cac_number  : '' ) }}" /> </p>
             </p>
            </div>
          </div>

          <div id="Q-17" class="tab">
            <input type="hidden" id="storage_count" name="storage_count" value="1">
            <p><label>Was storage installed? </label> 
              <label>{!! Form::radio('storage_installed_1', 'Yes',  isset($questionnaire_data->storage_installed_1) && $questionnaire_data->storage_installed_1 == 'Yes'  ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('storage_installed_1', 'No',  isset($questionnaire_data->storage_installed_1) && $questionnaire_data->storage_installed_1 == 'No'  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Manufacturer: </label> <input type="text" oninput="this.className=''" name="storage_manufacture_1" id="storage_manufacture_1" value="{{$questionnaire_data->storage_manufacture_1 or ''}}" /> </p>
            <p><label>Model Name: </label> <input type="text" oninput="this.className=''" name="storage_model_1" id="storage_model_1" value="{{$questionnaire_data->storage_model_1 or ''}}" /> </p>
            <p><label>Size of Storage system : </label> <input type="text" oninput="this.className=''" name="storage_size_1" id="storage_size_1" value="{{$questionnaire_data->storage_size_1 or ''}}" /> kWh </p>
            <p><label>Status: </label><label>{!! Form::radio('storage_status_1', 'New',  isset($questionnaire_data->storage_status_1) && $questionnaire_data->storage_status_1 == 'New'  ? true : null   ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('storage_status_1', 'Existing retained',  isset($questionnaire_data->storage_status_1) && $questionnaire_data->storage_status_1 == 'Existing retained'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('storage_status_1', 'Existing Removed',  isset($questionnaire_data->storage_status_1) && $questionnaire_data->storage_status_1 == 'Existing Removed'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Storage_Number_1" id="CAC_Storage_Number_1" value="{{$questionnaire_data->CAC_Storage_Number_1 or ''}}" /> </p>
            <p><label>Additional Storage : </label><label>{!! Form::checkbox('additional_storage_1','Yes', isset($questionnaire_data->additional_storage_1) && $questionnaire_data->additional_storage_1 == 'Yes'  ? true : null, ['oninput' => "this.className=''", 'id' => 'additional_storage_1'] ) !!}  </label> </p>
            <div id="storage2" style="display:none">
            <p><label>Was storage installed? </label> 
              <label>{!! Form::radio('storage_installed_2', 'Yes',  isset($questionnaire_data->storage_installed_2) && $questionnaire_data->storage_installed_2 == 'Yes'  ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('storage_installed_2', 'No',   isset($questionnaire_data->storage_installed_2) && $questionnaire_data->storage_installed_2 == 'No'  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Manufacturer: </label> <input type="text" oninput="this.className=''" name="storage_manufacture_2" id="storage_manufacture_2" value="{{$questionnaire_data->storage_manufacture_2 or ''}}" /> </p>
            <p><label>Model Name: </label> <input type="text" oninput="this.className=''" name="storage_model_2" id="storage_model_2" value="{{$questionnaire_data->storage_model_2 or ''}}" /> </p>
            <p><label>Size of Storage system : </label> <input type="text" oninput="this.className=''" name="storage_size_2" id="storage_size_2" value="{{$questionnaire_data->storage_size_2 or ''}}" /> kWh </p>
            <p><label>Status: </label><label>{!! Form::radio('storage_status_2', 'New',  isset($questionnaire_data->storage_status_2) && $questionnaire_data->storage_status_2 == 'New'  ? true : null  ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('storage_status_2', 'Existing retained', isset($questionnaire_data->storage_status_2) && $questionnaire_data->storage_status_2 == 'Existing retained'  ? true : null  ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('storage_status_2', 'Existing Removed', isset($questionnaire_data->storage_status_2) && $questionnaire_data->storage_status_2 == 'Existing Removed'  ? true : null  ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Storage_Number_2" id="CAC_Storage_Number_2" value="{{$questionnaire_data->CAC_Storage_Number_2 or ''}}" /> </p>
            <p><label>Additional Storage : </label> <label>{!! Form::checkbox('additional_storage_2','Yes', isset($questionnaire_data->additional_storage_2) && $questionnaire_data->additional_storage_2 == 'Yes'  ? true : null, ['oninput' => "this.className=''", 'id' => 'additional_storage_2'] ) !!}  </label></p>
            </div>
             <div id="storage3" style="display:none">
            <p><label>Was storage installed? </label> 
              <label>{!! Form::radio('storage_installed_3', 'Yes', isset($questionnaire_data->storage_installed_3) && $questionnaire_data->storage_installed_3 == 'Yes'  ? true : null  ,['oninput' => "this.className=''"]) !!} Yes  </label>
              <label>{!! Form::radio('storage_installed_3', 'No', isset($questionnaire_data->storage_installed_3) && $questionnaire_data->storage_installed_3 == 'No'  ? true : null ,['oninput' => "this.className=''"]) !!} No </label>
            </p>
            <p><label>Manufacturer: </label> <input type="text" oninput="this.className=''" name="storage_manufacture_3" id="storage_manufacture_3" value="{{$questionnaire_data->storage_manufacture_3 or ''}}" /> </p>
            <p><label>Model Name: </label> <input type="text" oninput="this.className=''" name="storage_model_3" id="storage_model_3" value="{{$questionnaire_data->storage_model_3 or ''}}" /> </p>
            <p><label>Size of Storage system : </label> <input type="text" oninput="this.className=''" name="storage_size_3" id="storage_size_3" value="{{$questionnaire_data->storage_size_3 or ''}}" /> kWh </p>
            <p><label>Status: </label><label>{!! Form::radio('storage_status_3', 'New',  isset($questionnaire_data->storage_status_3) && $questionnaire_data->storage_status_3 == 'New'  ? true : null   ,['oninput' => "this.className=''"]) !!} New   </label>
              <label>{!! Form::radio('storage_status_3', 'Existing retained', isset($questionnaire_data->storage_status_3) && $questionnaire_data->storage_status_3 == 'Existing retained'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing retained  </label>
              <label>{!! Form::radio('storage_status_3', 'Existing Removed', isset($questionnaire_data->storage_status_3) && $questionnaire_data->storage_status_3 == 'Existing Removed'  ? true : null ,['oninput' => "this.className=''"]) !!} Existing Removed </label>
            </p>
            <p><label>Certifying Authority Certificate Number: </label> <input type="text" oninput="this.className=''" name="CAC_Storage_Number_3" id="CAC_Storage_Number_3" value="{{$questionnaire_data->CAC_Storage_Number_3 or ''}}" /> </p>
            
            </div>
      
          </div>

          <div id="Q-15" class="tab">
            <p><label>Multi phase systems only  </label> </p>
            <hr />
            <p><label>Red  </label> </p>
            <p><label>Power rating </label> <input type="text" oninput="this.className=''" name="red_power_rating" id="red_power_rating" value="{{$questionnaire_data->red_power_rating or ''}}" /> kW</p>
            <p><label>Energy Storage capacity</label> <input type="text" oninput="this.className=''" name="red_energy_storage_capacity" id="red_energy_storage_capacity" value="{{$questionnaire_data->red_energy_storage_capacity or ''}}" /> kWh</p>
            <hr />
            <p><label>White  </label> </p>
            <p><label>Power rating </label> <input type="text" oninput="this.className=''" name="white_power_rating" id="white_power_rating" value="{{$questionnaire_data->white_power_rating or ''}}" /> kW</p>
            <p><label>Energy Storage capacity</label> <input type="text" oninput="this.className=''" name="white_energy_storage_capacity" id="white_energy_storage_capacity" value="{{$questionnaire_data->white_energy_storage_capacity or ''}}" /> kWh</p>
            <hr />
            <p><label>Blue  </label> </p>
            <p><label>Power rating </label> <input type="text" oninput="this.className=''" name="blue_power_rating" id="blue_power_rating" value="{{$questionnaire_data->blue_power_rating or ''}}" /> kW</p>
            <p><label>Energy Storage capacity</label> <input type="text" oninput="this.className=''" name="blue_energy_storage_capacity" id="blue_energy_storage_capacity" value="{{$questionnaire_data->blue_energy_storage_capacity or ''}}" /> kWh</p>
            <!--<p><label>Description of all works * : </label> {!! Form::textarea('installation_comments', isset($job->installation_comments) && ($job->installation_comments != "") ? $job->installation_comments : "New Solar System Installed" ,['oninput' => "this.className=''"]) !!} </p>-->
          </div>

            @PHP $address = \App\Setting::give_value_of('Address');
            $address =  str_replace("<br/>", "\n", $address); @ENDPHP

          <div id="Q-18" class="tab">
            <p><label>Other information (if required): </label> <input type="textarea" oninput="this.className=''" name="other_info" id="other_info" value="{{$questionnaire_data->other_info or ''}}" /> </p>
            <hr />
            <label>Company and Installer Details </label> 
            <p><label>Company: </label> <input type="text" oninput="this.className=''" name="company_name" id="company_name" value="{{ \App\Setting::give_value_of('Company_Name') }}" /> </p>
            <p><label>Address: </label> {!! Form::textarea('address', $address ,['oninput' => "this.className=''",'disabled']) !!}  </p>
            <p><label>Telephone: </label> <input type="text" name="Telephone" id="Telephone"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('Phone') }}" /> </p>
            <p><label>E-mail: </label> <input type="text" oninput="this.className=''" name="Email" id="Email" value="{{ \App\Setting::give_value_of('Email') }}" /> </p>
            <p><label>Name: </label> <input type="text" oninput="this.className=''" name="REC_Name" id="REC_Name" value="{{ \App\Setting::give_value_of('REC_Name') }}" /> </p>
            
           
          </div>

          <div id="Q-19" class="tab">
             <p><label>Registered Electrical Contractor Name   :  </label> <input type="text" name="REC_ContractorName" id="REC_ContractorName"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            <p><label>REC Number :  </label> <input type="text" name="REC_Number" id="REC_Number"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Number') }}" /></p>

            <p><label>Telephone: </label> <input type="text" name="REC_Telephone" id="REC_Telephone"  oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('Phone') }}" /> </p>

            <p><label>Responsible person   :  </label> <input type="text" name="REC_ResponsiblePerson" id="REC_ResponsiblePerson" oninput="this.className=''" disabled value="{{ \App\Setting::give_value_of('REC_Name') }}" /></p>

            @php $image=""; $image = \App\Setting::give_value_of('Signature'); @endphp

            <p><label> Signature :  </label> <img src="{{asset('/img/'.$image)}}" width="100px" height="50px" alt="signature" /> </p>
           
            <p><label>Date :  </label> <input type="text" class="completion_date" oninput="this.className=''" disabled /></p>
            
           
          </div>

          <div id="Q-20" class="tab">
            <p><label>Supply Address: </label> {!! Form::textarea('customer_address', $customer->unit.' '.$customer->street_number.' '.$customer->street_name.' '.$customer->lot.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code ,['oninput' => "this.className=''",'disabled']) !!}</p>
            <p><label>Generator Owner Name: </label> <input type="text" oninput="this.className=''" name="customer_name" id="customer_name" value="{{ $customer->first_name.' '.$customer->last_name }} " /> </p>
            <p><label>Customer NMI: </label> <input type="text" oninput="this.className=''" name="customer_rmi" id="customer_rmi" value="{{ $customer->nmi }}" /> </p>
            <p><label>E-mail Address: </label> <input type="text" oninput="this.className=''" name="customer_email" id="customer_email" value="{{ $customer->email }}" /> </p>
            <p><label>Telephone Number (Business Hours): </label> <input type="text" oninput="this.className=''" name="customer_cell" id="customer_cell" value="{{ $customer->phone_mobile }}" /> </p>
            <p><label>Telephone Number (After Hours): </label> <input type="text" oninput="this.className=''" name="customer_homephone" id="customer_homephone" value="{{ $customer->phone_home }}" /> </p>
            <p><label>Mailing Address: </label> <input type="text" oninput="this.className=''" name="mail_address" id="mail_address" value="" /> </p>
          
          </div>

      

          
          <!-- Circles which indicates the steps of the form: -->
          <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
          </div>



<script>

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";

  $('.question-no').html( "Question - "+ (n+1) );

  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");

  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("questionForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
  
  calcPhases();
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");

  // validate the form through current tab
  
  y = x[currentTab].getElementsByTagName("input");
  
  // get the id of the current tab and validate the fields of this tab
  
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    // if (y[i].value == "") {
    //   // add an "invalid" class to the field:
    //   y[i].className += " invalid";
    //   // and set the current valid status to false
    //   valid = false;
    // }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

/*============================================================================*/

//for default values
function getTotalPackage() {
  var totalPackage = {{ $job->total_wt }} / 1000;
  
  $('#solar_pv_net').val(totalPackage);
  $('#solar_total_cap').val(totalPackage);
  
}

function calcPhases() {
  
  var inverter_count = $('#inverter_count').val();
  var no_of_phases = {{ $job->no_of_phases }};
  console.log(inverter_count);
  console.log(no_of_phases);
  if (no_of_phases == 3 && inverter_count > 0) {
    var total_wt =  {{ $job->total_wt }};
    var result = (total_wt * inverter_count) / no_of_phases / 1000;

    $('#red_power_rating').val(result.toFixed(2));
    $('#white_power_rating').val(result.toFixed(2));
    $('#blue_power_rating').val(result.toFixed(2));
  } 
  
  
}



$( document ).ready(function() {
          getTotalPackage();

          if ({{ $job->no_of_phases }} < 2) {
            $('#Q-15').hide();
            $('#Q-15').removeClass('tab');
          };

          if($('#additional_inverter_1').prop('checked')) {
              $('#inverter2').show();
              $('#inverter_count').val('2');
          }
          else {
            $('#inverter_count').val('1');
            $('#inverter2').hide();
            $('#inverter3').hide();
          }
             
          if( $('#additional_inverter_2').prop('checked')) {
                 $('#inverter3').show();
                 $('#inverter_count').val('3');
              }
              else {
                $('#inverter_count').val('2');
                $('#inverter3').hide();
          }

          if($('#additional_storage_1').prop('checked')) {
                 $('#storage2').show();
                 $('#storage_count').val('2');
                 
          }
          else {
                $('#storage_count').val('1');
                $('#storage2').hide();
                $('#storage3').hide();
          }
          

          if($('#additional_storage_2').prop('checked')) {
                 $('#storage3').show();
                 $('#storage_count').val('3');
          }
          else {
                $('#storage_count').val('2');
                $('#storage3').hide();
          }
             
          
          

          
          $('#additional_inverter_1').change(function() {
              if(this.checked) {
                 $('#inverter2').show();
                 $('#inverter_count').val('2');
              }
              else {
                $('#inverter_count').val('1');
                $('#inverter2').hide();
                $('#inverter3').hide();
              }
             
          });

          $('#additional_inverter_2').change(function() {

              if(this.checked) {
                 $('#inverter3').show();
                 $('#inverter_count').val('3');
              }
              else {
                $('#inverter_count').val('2');
                $('#inverter3').hide();
              }
             
          });


          $('#additional_storage_1').change(function() {
              if(this.checked) {
                 $('#storage2').show();
                 $('#storage_count').val('2');
                 
              }
              else {
                $('#storage_count').val('1');
                $('#storage2').hide();
                $('#storage3').hide();
              }
             
          });

          $('#additional_storage_2').change(function() {

              if(this.checked) {
                 $('#storage3').show();
                 $('#storage_count').val('3');
              }
              else {
                $('#storage_count').val('2');
                $('#storage3').hide();
              }
             
          });
          

          

  });
 
  


</script>

