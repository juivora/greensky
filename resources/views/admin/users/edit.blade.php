@extends('layouts.backend')

@section('title','Edit User')
@section('pageTitle','Edit User')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Edit User # {{$user->name}}
                    </div>
                    <div class="actions">
                        {{--  @include('partials.page_tooltip',['model' => 'user','page'=>'form'])  --}}
                    </div>
                </div>
                <div class="box-content panel-body">
                    <a href="{{ url('/admin/users') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($user, [
                        'method' => 'PATCH',
                        'url' => ['/admin/users', $user->id],
                        'class' => 'form-horizontal',
                        'files' => true,
                        'autocomplete'=>'off'
                    ]) !!}

                    @include ('admin.users.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}               

                </div>
            </div>
        </div>
    </div>
@endsection