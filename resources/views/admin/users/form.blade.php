@push('css')
<style>
    .intl-tel-input {
        display: block;
    }
</style>


@endpush


<div class="row ">
    <div class="col-md-6 col-sm-offset-1">

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="">
                <span class="field_compulsory">*</span>Name
            </label> 
            {!! Form::text('name', null, ['class' => 'form-control']) !!} 
            {!! $errors->first('name','<p class="help-block">:message</p>') !!}
        </div>
        @if(Auth::user()->can('access.role.edit'))
        <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role">
                <span class="field_compulsory">*</span>Role
            </label>
            {!! Form::select('roles',$roles,isset($user_roles) ? $user_roles : '', ['class' => 'form-control select2', 'multiple' =>false ,'id'=> 'role' ]) !!}
            {!! $errors->first('roles','<p class="help-block">:message</p>') !!}
        </div>
        @endif

        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="">
                <span class="field_compulsory">*</span>Email
            </label> 
            {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <label for="password" class="">
                @if(!isset($user_roles))
                    <span class="field_compulsory">*</span>
                @endif
               Password
            </label> 
            {!! Form::password('password', ['class' => 'form-control']) !!} 
            {!! $errors->first('password','<p class="help-block">:message</p>') !!}
        </div>

        {{--  <div class="form-group{{ $errors->has('states') ? ' has-error' : ''}}">
                <label for="states">
                    <span class="field_compulsory">*</span>States
                </label>
                {!! Form::select('states[]',$states,isset($user_states) ? $user_states : [], ['class' => 'form-control select2', 'multiple' =>true ]) !!}
                {!! $errors->first('states','<p class="help-block">:message</p>') !!}
        </div>  --}}
        
   

        <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
            <label for="phone" class="">Phone</label> 
            {!! Form::text('phone', null , ['class' => 'form-control']) !!}
            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>

         <div class="sales_fields">
            <div class="form-group {{ $errors->has('sales_message') ? 'has-error' : ''}}">
                <label for="sales_message" class="">Sales message</label> 
                {!! Form::textarea('sales_message', null , ['class' => 'form-control']) !!}
                {!! $errors->first('sales_message', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {{ $errors->has('post_codes') ? 'has-error' : ''}}">
                <label for="post_codes" class="">Postal codes responsible for</label> 
                {!! Form::textarea('post_codes', null , ['class' => 'form-control']) !!}
                {!! $errors->first('post_codes', '<p class="help-block">:message</p>') !!}
            </div>

            
         </div>

        <div class="installer_fields">
         
        <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
            {!! Form::label('image', 'Add Signature Image ( Please upload this resolution image for better display in Report [ 100px X 75px ] )') !!}
            {!! Form::file('image',null, ['class' => 'form-control']) !!} 
        </div>
        <div class="form-group">
            @if(isset($user) && isset($user->image))
                @if( file_exists(public_path('/user/'.$user->image) )  )
                <img  src="{{url('/user/'.$user->image)}}" width="125" height="100" alt="{{ $user->name }}"/>  
                @endif          
            @endif 
            {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                <label for="address" class="">Address</label> 
                {!! Form::text('address', null , ['class' => 'form-control']) !!}
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
        </div>

       

        <div class="form-group {{ $errors->has('suburb') ? 'has-error' : ''}}">
                <label for="suburb" class="">Suburb</label> 
                {!! Form::text('suburb', null , ['class' => 'form-control']) !!}
                {!! $errors->first('suburb', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('post_code') ? 'has-error' : ''}}">
                <label for="post_code" class="">Post Code</label> 
                {!! Form::text('post_code', null , ['class' => 'form-control']) !!}
                {!! $errors->first('post_code', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('accredition_number') ? 'has-error' : ''}}">
                <label for="accredition_number" class="">Accredition Number</label> 
                {!! Form::text('accredition_number', null , ['class' => 'form-control']) !!}
                {!! $errors->first('accredition_number', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('electrical_licence_number') ? 'has-error' : ''}}">
                <label for="electrical_licence_number" class="">Electrical license number </label> 
                {!! Form::text('electrical_licence_number', null , ['class' => 'form-control']) !!}
                {!! $errors->first('electrical_licence_number', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
                <label for="company_name" class="">Company Name</label> 
                {!! Form::text('company_name', null , ['class' => 'form-control']) !!}
                {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('company_address') ? 'has-error' : ''}}">
                <label for="company_address" class="">Company Address</label> 
                {!! Form::text('company_address', null , ['class' => 'form-control']) !!}
                {!! $errors->first('company_address', '<p class="help-block">:message</p>') !!}
        </div>
        </div>

 
        <div class="form-group">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
        </div>
    
    
    </div>
</div>

@push('js')
<script>
        $( document ).ready(function() {

            var roles = $('#role').val();
            
            if(roles != '' || roles != null){
  
                    if(roles == "Installers"){
                        $(".installer_fields").show();
                        $(".sales_fields").hide();
                    }else{
                        $(".installer_fields").hide();
                        $(".sales_fields").show();
                    }
            }   
        });
        $('#role').on('change',function(){
            var roles = $(this).val();
      
            if( roles != null){
                    if(roles == "Installers"){
                        $(".installer_fields").show()
                          $(".sales_fields").hide();
                    }else{
                        $(".installer_fields").hide()
                        $(".sales_fields").show();
                    } 
            }else{
                $(".installer_fields").hide()
                  $(".sales_fields").show();
            }
   
       });
</script>
@endpush
