@extends('layouts.backend')

@section('title',trans('user.label.show_user'))


@section('content')

 <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#people">User  # {{$user->name}}</a></li>
        
    </ul>

    <div class="tab-content">
        <div id="people" class="tab-pane fade in active">
            <div class="row">
                <div class="col-md-12">
                    <div class="box bordered-box blue-border">
                        
                        <a href="{{ url('/admin/users') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Back
                        </button>
                    </a>
                    @if($user->id != 1)
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                           edit
                        </button>
                    </a>
                    @endif

                    @if(Auth::user()->can('access.user.delete'))
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/users', $user->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete User',
                            'onclick'=>"return confirm('Confirm delete?')"
                    ))!!}
                    {!! Form::close() !!}
                    @endif
                    @endif


                    <div class="next_previous">
                    @if($user->previous() !== null)
                       <a class="btn btn-warning btn-xs" href="{{ $user->previous()->id }}"> Previous </a>
                    @endif 
                    @if($user->next() !== null)
                       <a class="btn btn-warning btn-xs" href="{{ $user->next()->id }}"> Next </a>
                    @endif  
                    </div>  
                    <br/>
                    <br/>

                    
                    <?php
                    $role = join(' + ', $user->roles()->pluck('label')->toArray());
                    ?>
                    
                    
                        <div class="box-content ">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td> Id </td>
                                <td>{{ $user->id }}</td>
                            </tr>

                            <tr>
                                <td> Name </td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            
                            <tr>
                                <td> Email </td>
                                <td>{{ $user->email }}</td>
                            </tr>

                            <tr>
                                <td>Role </td>
                                <td>{{ $role }}</td>
                            </tr>

                             <tr>
                                <td>Postal Codes </td>
                                <td>{{ $user->post_codes }}</td>
                            </tr>
                            <tr>
                                <td>Sales Message </td>
                                <td>{{ $user->sales_message }}</td>
                            </tr>

                            <tr>
                                <td>States</td>
                                
                                <td>@foreach($user->state as $states)
                                    @if($states->state_name)
                                    {{ $states->state_name->state}} ,
                                    @endif
                                    @endforeach
                                </td>
                            </tr>

                          
                            
                           
                            </tbody>
                        </table>                 
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>  </div>

@if(isset($opportunities) && count($opportunities)>0)
   
    <div class="row">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle-blank"></i>
                           Opportunities
                        </div>
                    </div>
                    
                    <div class="box-content ">
                        
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Opportunity Id</th>
                                    <th>Opportunity Name</th>
                                   </tr>
                                </thead>
                                <tbody>
                               <form name="add_transaction"  method="get">
                               
                                    @foreach($opportunities as $opp) 
                                <tr>
                                  
                                       <td>{{$opp->opp_id}}</td>
                                       <td>{{$opp->opp_name}}</td>
                                       
                                    
                                   
                                    </tr>
                                        @endforeach
                                  
                            </form>
                        </tbody>
                            </table>
                          
                        </div>
    
                    </div>
                </div>
              
            </div>
        </div>
   
@endif                      



@endsection



