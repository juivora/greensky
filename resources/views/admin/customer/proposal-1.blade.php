<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proposal Page</title>
	<style>
	<style type="text/css">
    .responsive {
      width: 80%;
      max-width: 200px;
      min-width: 200px;
      max-height: 200px;
      height: 80%;
    }
      
    .responsive_1 {
      width: 100%;
      max-width: 340px;
      max-height: 340px;
      height: 100%;
    }

	</style>
</head>
<body>
   <img src="{{asset('/offer/'.$offer->id.'/GreenSky_SalesBrochure_1_'.$offer->id.'.jpg')}}" alt="">
   <img src="{{asset('/offer/'.$offer->id.'/GreenSky_SalesBrochure_2_'.$offer->id.'.jpg')}}" alt="">  
   <!--<img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_02.png')}}" />-->
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_03.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_04.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_05.png')}}" />
  
 
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_06_top.png')}}" />
  
  @if( $offer->offer_detail->count() == 1 )
		@include('admin.customer.price-section-1a')
	@endif
	@if( $offer->offer_detail->count() == 2 )
		@include('admin.customer.price-section-2a')
	@endif
	@if( $offer->offer_detail->count() == 3 )
		@include('admin.customer.price-section-3a')
	@endif

   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_06_bottom.png')}}" />

 	@if( file_exists(public_path('/offer/'.$offer->id.'/'.$solar_panel_placement) )  ||   file_exists(public_path('/offer/'.$offer->id.'/'.$system_params) )  || file_exists(public_path('/offer/'.$offer->id.'/'.$daily_average) ) )
    <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_07_top.png')}}" />
	<!-- New starting Page -->
	
	 <table style="background:#fff;width:850px;padding:10px 10px;">
        <tr>
           <td>
                <table style="padding:0 0 20px 0;">
                    <tr>
                        <td style="width:350px;margin:0;padding:0 20px 0 0px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="font-size:20px;padding:0 0 10px 10px;color:#0000cc;font-weight:700;font-family: Arial, Helvetica, sans-serif;letter-spacing:1px;"><b>PROPOSED SOLAR <br>  PANEL PLACEMENT</b></td>
                                </tr>
                            </table>
                           <table>
                                <tr>
                                    <td style="width:350px;font-size:13px;padding:20px 0 25px 10px;line-height:20px;text-align:justify;font-family: Arial, Helvetica, sans-serif;">
                    1. System efficiency is estimated by the solar installer to account for losses that may include shading, inverter efficiency for DC to AC conversion battery efficiency, cable losses, dirt, manufacturer tolerances, grid-tie system outages, maintenance downtime, and other factors.
                    
                    <br><br>
                  
                    2. Energy Output is calculated based on historical solar irradiance and temperature data at this location, factoring in panel tilt, orientation, and all of the System Parameters including System Efficiency.ors.
                    
                    <br><br>
                  
                    3. Emission reduction assumes full output usage and 1.17 kg CO2 / kWh based on Victoria average (National Greenhouse and Energy Reporting (Measurement) Determination 2008).
                    
                    <br><br>
                    
                    4. Assumes full year-round utilisation of generated electricity and will change based on usage and feed-in tariffs.
                  </td>
                </tr>
                                
                            </table>
                        </td> 
                        <td style="width:450px;margin:0;vertical-align:bottom;">
              <table>
              <tr>
                <td>
                @if($solar_panel_placement != '' &&  file_exists(public_path('/offer/'.$offer->id.'/'.$solar_panel_placement) )  )
                  <img src="{{asset('/offer/'.$offer->id.'/'.$solar_panel_placement)}}" style="width:380px;"  width="380px">
                @else
                <img src="{{ asset('assets/images/default.jpg') }}" style="width:380px;" width="380px"/>
                @endif
                </td>
              </tr>
              <tr>
                <td>
                  @if($daily_average != '' &&  file_exists(public_path('/offer/'.$offer->id.'/'.$daily_average) )  )
                    <img src="{{asset('/offer/'.$offer->id.'/'.$daily_average)}}" style="width:380px;padding-top:20px;"  width="380px">
                  @else
                    <img src="{{ asset('assets/images/default.jpg') }}" style="width:380px;padding-top:20px;"  width="380px"/>
                  @endif

                    
                </td>
              </tr>
                  
              </table>  
                
            </td> 
          </tr>
       </table><!-- end top page - 1  -->            
        
        <table>
          <tr>
            <td style="width:900px;margin:0;padding:0 10px 0 10px;vertical-align:top;">
               @if($system_params != '' &&  file_exists(public_path('/offer/'.$offer->id.'/'.$system_params) )  )
              <table>
                <tr>
                  <td style="font-size:20px;line-height:20px;text-align:left;font-family: Arial, Helvetica, sans-serif;color:#595959;letter-spacing:1px;"> SYSTEM PARAMETERS </td>
                </tr>
                <tr>
                  <td style="font-size:20px;line-height:20px;text-align:left;font-family: Arial, Helvetica, sans-serif;color:#595959;letter-spacing:1px;">
                   
                      <a href="{{asset('/offer/'.$offer->id.'/'.$system_params)}}" target="_blank" >Access system parameter report details</a>
                   
                  </td>
                </tr>
              </table>
              @endif
            </td>
          </tr>
        </table>
        <td>
        </tr>
    </table><!-- End of page - 2 -->
		<img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_07_bottom.png')}}" />
    <div style="page-break-after: always;" ></div>
  @else
    <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_07.png')}}" />
	@endif
 

   
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_08.png')}}" />
    @if($offer->sign == 1)
      <img src="{{asset('/offer/'.$offer->id.'/GreenSky_SalesBrochure_09_'.$offer->id.'.jpg')}}" alt="">
    @else
      <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_09.png')}}" />
    @endif
   
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_10.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_11.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_12.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_13.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_14.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_15.png')}}" />
   <img src="{{ asset('public/assets/images/4/GreenSky_SalesBrochure_16.png')}}" />
  
</body>
</html>

