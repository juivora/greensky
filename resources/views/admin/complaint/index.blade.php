@extends('layouts.backend')
@section('title','Complaints')
@section('pageTitle','Complaints')
@section('content')

      <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                    Complaints
                                                  </div>

                               </div>
                               <div class="box-content ">
                                    <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                       
                        <div class="row">
                               
                    <div class="col-md-6">
                        @if (Auth::user()->can('access.complaints.create') == 1)
                        <a href="{{ url('/admin/complaints/create') }}" class="btn btn-success btn-sm" title="Add New Complaint"><i class="fa fa-plus" aria-hidden="true"></i> Add New Complaint</a>
                        @endif
                    </div>

</div>	
						<div class="col-md-12 col-sm-12 col-xs-12">	


							<div class="navbar-form navbar-right col-md-4 col-sm-4">
								
								{!! Form::label('filter', 'Filter By: ') !!} 
								@if(Auth::user()->roles[0]->name == "Installers")
								{!! Form::select('filter',array('All'=>'All','New'=>'New
								','In progress'=>'In progress','To be resolved'=>'To be resolved','Action required'=>'Action required','Action URGENT'=>'Action URGENT','Await customer feedback'=>'Await customer feedback','Resolution required'=>'Resolution required','Resolution URGENT'=>'Resolution URGENT','Closed'=>'Closed'),'',['class'=>'form-control','id' => 'filter']) !!}
								@else
								{!! Form::select('filter',array('All'=>'All','New'=>'New
                                ','In progress'=>'In progress','To be resolved'=>'To be resolved','Action required'=>'Action required','Action URGENT'=>'Action URGENT','Await customer feedback'=>'Await customer feedback','Resolution required'=>'Resolution required','Resolution URGENT'=>'Resolution URGENT','Closed'=>'Closed'),'',['class'=>'form-control','id' => 'filter']) !!}
								@endif
								
							</div>
							
						</div>	

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="complaints-table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Date Of Complaint</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Contact Number</th>
                                        <th>Email</th>
                                        <th>Date Of Install</th>
                                        <th>Category</th>
                                        <th>Nature of complaint</th>
                                        <th>Status</th>
                                        <th>Company Rep assigned</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                            </table>
                          
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@push('js')
<script>

        var url ="{{ url('admin/complaints-data') }}";
        var edit_url = "{{ url('admin/complaints') }}";
        var customer_url = "{{ url('admin/customer') }}";
        var email_url = "{{ url('admin/email_to_retailer') }}";
        var preview_email_url = "{{ url('admin/preview_email_to_retailer') }}";
        var forms_url = "{{ url('admin/regenerate_forms') }}";
        var img_path = "{{asset('assets/images')}}";
        var q_url = "{{ url('admin/questions') }}";

        //Permissions
        var edit_job = "<?php echo Auth::user()->can('access.complaints.edit'); ?>";
        var delete_job = "<?php echo Auth::user()->can('access.job.delete'); ?>";
        
        datatable = $('#complaints-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                    d.filter = $('#filter').val();
                }
            },
                columns: [
                    { data: 'id',name:'id',visible:false},
                    { 
                        "data" : null,
                        "name" : 'created_at',
                        "searchable" : true,
                        "orderable" : true,
                        render : function(o){
                            var createdate = o.created_at;
                          return moment(createdate).format('DD-MM-YYYY') + '<br/>' + moment(createdate).format('HH:mm:ss');
                        }
                    },
                    { "data" : 'firstname',
                        "name" : 'firstname',
                        "searchable" : true,
                        "orderable" : true
                    },
                    { "data" : 'lastname',
                        "name" : 'lastname',
                        "searchable" : true,
                        "orderable" : true
                    },
                    { data: 'contactnumber',name:'contactnumber'},
                    { data: 'email',name:'email'},
                    { data: 'dateofinstall',name:'dateofinstall'},
                    { data: 'category',name:'category'},
                    { data: 'complaintmessage',name:'complaintmessage'},
                    { data: 'status',name:'status'},
                    { "data" : null,
                        "name" : 'assigneduser',
                        "searchable" : true,
                        "orderable" : true,
                        render : function(o){
                        	var assigneduser = "";
                        	if (o.assigneduser) {
                        		assigneduser = o.assigneduserdetails.name ;
                        	}
                        	
                            return assigneduser;
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var v="" ; var e=""; var d=""; var q= ""; var f="";

                            v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-primary btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>View</button></a>&nbsp;";

                            if(edit_job){
                                e= "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";
                            }
                               
                            
                            return v+e;
                        }
                    
                    
                    }
                   
                ],
                "rowCallback": function( row, data, index ) {
                    $row_color = "";
                    if ( data.status == 'New' ) {
                        //Highlight the row
                        $(row).addClass("success ");
                    }
                    else if ( data.status == 'In progress' ) {
                        //Highlight the row
                        $(row).addClass("success ");
                    }
                    else if ( data.status == 'To be resolved' ) {
                        //Highlight the row
                        $(row).addClass("warning ");
                    }
                    else if ( data.status == 'Action required' ) {
                        //Highlight the row
                        $(row).addClass("warning ");
                    }
                    else if ( data.status == 'Action URGENT' ) {
                        //Highlight the row
                        $(row).addClass("danger ");
                    }
                    else if ( data.status == 'Resolution required' ) {
                        //Highlight the row
                        $(row).addClass("danger ");
                    }
                    else if ( data.status == 'Resolution urgent' ) {
                        //Highlight the row
                        $(row).addClass("danger ");
                    }
                    else {
                        //Highlight the row
                        $(row).addClass("");
                    }
                },
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/job')}}/" + id;
            var r = confirm("Are you sure you want to delete Job?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Job Deleted Successfully', data.message)
                    },
                    error: function (xhr, status, error) {
                        datatable.draw();
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

        $('#filter').change(function() {
            datatable.draw();
        });
        
    
        $(document).on('click', '#number_modal', function (e) {
            var job_id = $(this).val();
            var data_value = $(this).attr('data-value');
            var formid = $("#number_form");
            resetupdateform(formid);
            $('#myModal').modal('show');
            $('#job_id').val(job_id);
            $('#pre_approval_number').val(data_value);
        }); 

        function resetupdateform(formid) {
            $(formid)[0].reset();
            $('#error').html("");
        }

$("#number_form").validate({
    
    submitHandler: function (form) {
        var pre_approval_number = $('#pre_approval_number').val();
        var job_id = $('#job_id').val();
        if(pre_approval_number == ""){
            document.getElementById("error").innerHTML = "Field cannot be blank";
            return false;
        }
        var url = "{{url('admin/pre_approval_number')}}/"+job_id;
        var method = "get";
        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {
                $('#myModal').modal('hide');
                toastr.success(result.message)
                datatable.draw();
               
            },
            error: function (error) {
                
                toastr.error(error.responseJSON.message);
                datatable.draw();
                $('#myModal').modal('hide');
                
            }
        }); 
        return false;
    }
});
    
</script>
@endpush