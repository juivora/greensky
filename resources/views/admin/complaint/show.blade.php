@extends('layouts.backend')
@section('title','Complaints')
@section('pageTitle','Complaints')
@section('content')
  
   
          <style>
        .csscontainer {
            display: grid;
            grid-template-columns: 2fr 2fr 2fr;
            grid-row-gap: 20px;
            grid-column-gap: 20px;
            grid-template-areas: 
            "complaintname dateofcomplaint ."
            "complaintnumber complaintemail ."
            "complaintaddress complaintaddress ."
            "category status ."
            "complaintmessage complaintmessage ."
            "assigneduser . ."
            "firstActionDate actiontake ."
            "dateresolved outcome ."
            "companychanges furtheractions ."
            ". submitarea ."
        }
        .dateofcomplaint {
            grid-area: dateofcomplaint;
        }
        .complaintname {
            grid-area: complaintname;
        }
        .complaintnumber {
            grid-area: complaintnumber;
        }
        .complaintemail {
            grid-area: complaintemail;
        }
        .complaintaddress {
            grid-area: complaintaddress;
        }
        .category {
            grid-area: category;
        }
        .status {
            grid-area: status;
        }
        .complaintmessage {
            grid-area: complaintmessage
        }
        .assigneduser {
            grid-area: assigneduser
        }
        .firstActionDate {
            grid-area: firstActionDate
        }
        .actiontake {
            grid-area: actiontake
        }
        .dateresolved {
            grid-area: dateresolved
        }
        .outcome {
            grid-area: outcome
        }
        .companychanges {
            grid-area: companychanges
        }
        .furtheractions {
            grid-area: furtheractions
        }
        .submitarea {
            grid-area: submitarea
        }
       </style>
         <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                            View Complaint
                    </div>
                </div>
                <div class="box-content ">
                	 <a href="{{url('/admin/complaints') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                        </a>
                        @if(Auth::user()->can('access.complaints.edit'))
                        <a href="{{ url('/admin/complaints/' . $complaint->id . '/edit') }}" title="Edit Complaint"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        @endif
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::model($complaint, [
                                        'method' => 'POST',
                                        'url' => ['/admin/complaint', $complaint->id],
                                        'files' => true
                                    ]) !!}

                    <div class="csscontainer">
                          <input type="hidden" name="_method" value="POST">
                        <div class="dateofcomplaint">Date Of Initial Complaint: {{ date('d-m-Y h:m:s', strtotime($complaint->created_at)) }}</div>
                        <div class="complaintname">Complainant Name: {{ $complaint->firstname.' '.$complaint->lastname }}</div>
                        <div class="complaintnumber">Complainant Contact Number: {{ $complaint->contactnumber }}</div>
                        <div class="complaintemail">Complainant Email: {{ $complaint->email }}</div>
                        <div class="complaintaddress">Complainant Address: {{ $complaint->address.' '.$complaint->suburb.' '.$complaint->state }}</div>
                        <div class="status">Status: {{ $complaint->status }} 
                                           
                        </div>
                        <div class="category">Category: {{ $complaint->category }}
                                        </div>
                        <div class="complaintmessage">Nature of complaint: {{ $complaint->complaintmessage }}</div>
                        <div class="assigneduser">Company Rep : 
                            @if (isset($complaint->assigneduserdetails))
                                {{ $complaint->assigneduserdetails->name }} 
                            @else
                                To be assigned
                            @endif
                        </div>
                        <div class="firstActionDate"><strong>Date first acted on:</strong> 
                         <input type="text" name="firstActionDate" id="firstActionDate"  readonly="true" class="form-control" value="{{ ($complaint->firstActionDate ? date('d-m-Y', strtotime($complaint->firstActionDate)) : '') }} " ></div>
                        <div class="actiontake"><strong>Action Taken:</strong> <br/>
                            {!! $complaint->actiontaken !!}
                        </div>
                        <div class="dateresolved"><strong>Date resolved: </strong>
                         <input type="text" id="dateresolved" name="dateresolved" readonly="true" value="{{ ($complaint->dateresolved ? date('d-m-Y', strtotime($complaint->dateresolved)) : '') }}"  class="form-control" ></div>
                        <div class="outcome"><strong>Outcome:</strong> <br/>
                            {!! $complaint->outcome !!}
                        </div>
                        <div class="companychanges"><strong>Any company changes to take place as a result of the outcome:</strong> <br/>
                            {!! $complaint->companychanges !!}
                        </div>
                        <div class="furtheractions"><strong>Further action taken by complainant:</strong> <br/>
                            {!! $complaint->furtheractions !!}
                         </div>

                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
            </div>

          
      
        <!-- <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              
            </div>
        </div>-->
   
   
@endsection

