@extends('layouts.backend')
@section('title','Complaints')
@section('pageTitle','Complaints')
@section('content')
  
   
          <style>
        .csscontainer {
            display: grid;
            grid-template-columns: 2fr 2fr 2fr;
            grid-row-gap: 20px;
            grid-column-gap: 20px;
            grid-template-areas: 
            "complaintname dateofcomplaint ."
            "complaintnumber complaintemail ."
            "complaintaddress complaintaddress ."
            "category status ."
            "complaintmessage complaintmessage ."
            "assigneduser . ."
            "firstActionDate actiontake ."
            "dateresolved outcome ."
            "companychanges furtheractions ."
            ". submitarea ."
        }
        .dateofcomplaint {
            grid-area: dateofcomplaint;
        }
        .complaintname {
            grid-area: complaintname;
        }
        .complaintnumber {
            grid-area: complaintnumber;
        }
        .complaintemail {
            grid-area: complaintemail;
        }
        .complaintaddress {
            grid-area: complaintaddress;
        }
        .category {
            grid-area: category;
        }
        .status {
            grid-area: status;
        }
        .complaintmessage {
            grid-area: complaintmessage
        }
        .assigneduser {
            grid-area: assigneduser
        }
        .firstActionDate {
            grid-area: firstActionDate
        }
        .actiontake {
            grid-area: actiontake
        }
        .dateresolved {
            grid-area: dateresolved
        }
        .outcome {
            grid-area: outcome
        }
        .companychanges {
            grid-area: companychanges
        }
        .furtheractions {
            grid-area: furtheractions
        }
        .submitarea {
            grid-area: submitarea
        }
       </style>
         <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                            Edit Complaint
                    </div>
                </div>
                <div class="box-content ">
                     <a href="{{url('/admin/complaints') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                        </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::model($complaint, [
                                        'method' => 'POST',
                                        'url' => ['/admin/complaint', $complaint->id],
                                        'files' => true
                                    ]) !!}

                    <div class="csscontainer">
                          <input type="hidden" name="_method" value="POST">
                        <div class="dateofcomplaint">Date Of Initial Complaint: {{ $complaint->created_at }}</div>
                        <div class="complaintname">Complainant Name: {{ $complaint->firstname.' '.$complaint->lastname }}</div>
                        <div class="complaintnumber">Complainant Contact Number: {{ $complaint->contactnumber }}</div>
                        <div class="complaintemail">Complainant Email: {{ $complaint->email }}</div>
                        <div class="complaintaddress">Complainant Address: {{ $complaint->address.' '.$complaint->suburb.' '.$complaint->state }}</div>
                        <div class="status">Status: 
                            @if (Auth::user()->can('access.complaints.editstatus') == 1)
                            <select name='status' id='status' data-id='{{$complaint->id}}' class="form-control">
                                <option value='New' @if($complaint->status == "New") selected @else "" @endif >New</option>
                                <option value='In progress' @if($complaint->status == "In progress") selected @else "" @endif >In progress</option>
                                <option value='To be resolved' @if($complaint->status == "To be resolved") selected @else "" @endif >To be resolved</option>
                                <option value='Action required' @if($complaint->status == "Action required") selected @else "" @endif >Action required</option>
                                <option value='Action URGENT' @if($complaint->status == "Action URGENT") selected @else "" @endif >Action URGENT</option>
                                <option value='Await customer feedback' @if($complaint->status == "Await customer feedback") selected @else "" @endif >Await customer feedback</option>
                                <option value='Resolution required' @if($complaint->status == "Resolution required") selected @else "" @endif >Resolution required</option>
                                <option value='Resolution URGENT' @if($complaint->status == "Resolution URGENT") selected @else "" @endif >Resolution URGENT</option>
                                <option value='Closed' @if($complaint->status == "Closed") selected @else "" @endif >Closed</option>
                            </select>
                            @else
                             {{ $complaint->status }} 
                            @endif
                        </div>
                        <div class="category">Category: {{ $complaint->category }}
                        <select name='category' id='category' data-id='{{$complaint->id}}' class="form-control">
                            <option value="System not generating as expected" @if($complaint->category == "System not generating as expected") selected @else "" @endif>System not generating as expected</option>
                            <option value="System not working" @if($complaint->category == "System not working") selected @else "" @endif>System not working</option>
                            <option value="Warranty issue" @if($complaint->category == "Warranty issue") selected @else "" @endif>Warranty issue</option>
                            <option value="Payment issue" @if($complaint->category == "Payment issue") selected @else "" @endif>Payment issue</option>
                            <option value="Connection to grid" @if($complaint->category == "Connection to grid") selected @else "" @endif>Connection to grid</option>
                            <option value="Issue with product" @if($complaint->category == "Issue with product") selected @else "" @endif>Issue with product</option>
                            <option value="Sales or complaints process" @if($complaint->category == "Sales or complaints process") selected @else "" @endif>Sales or complaints process</option>

                                            </select>
                                        </div>
                        <div class="complaintmessage">Nature of complaint: {{ $complaint->complaintmessage }}</div>
                        <div class="assigneduser">Company Rep : 
                            <select id="assigneduser" class="form-control" name="assigneduser">
                                @foreach($sales as $sale)
                                <option value="{{$sale->id}}" @if($complaint->assigneduser == $sale->id) selected @else "" @endif>{{$sale->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="firstActionDate"><strong>Date first acted on:</strong>
                         <input type="text" name="firstActionDate" id="firstActionDate"  class="form-control" value="{{ ($complaint->firstActionDate ? date('d-m-Y', strtotime($complaint->firstActionDate)) : '') }}" placeholder="dd-MM-yyyy"></div>
                        <div class="actiontake"><strong>Action Taken:</strong> 
                            {!! $complaint->actiontaken !!}
                         <textarea class="form-control" id="actiontaken" style="height:150px" name="actiontaken" placeholder="Action taken detail"></textarea></div>
                        <div class="dateresolved"><strong>Date resolved: </strong>
                         <input type="text" id="dateresolved" name="dateresolved" value="{{ ($complaint->dateresolved ? date('d-m-Y', strtotime($complaint->dateresolved)) : '') }}"  class="form-control" placeholder="dd-MM-yyyy"></div>
                        <div class="outcome"><strong>Outcome:</strong> 
                            {!! $complaint->outcome !!}
                         <textarea class="form-control" style="height:150px" id="outcome" name="outcome" placeholder="Outcome detail"></textarea></div>
                        <div class="companychanges"><strong>Any company changes to take place as a result of the outcome:</strong> 
                            {!! $complaint->companychanges !!}
                         <textarea class="form-control" style="height:150px" name="companychanges" id="companychanges" placeholder="Changes detail"></textarea></div>
                        <div class="furtheractions"><strong>Further action taken by complainant:</strong> 
                            {!! $complaint->furtheractions !!}
                         <textarea class="form-control" style="height:150px" name="furtheractions" id="furtheractions" placeholder=""></textarea></div>

                         <div class="submitarea"><button type="submit" class="btn btn-primary">Update</button></div>
                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
            </div>

          
      
        <!-- <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              
            </div>
        </div>-->
   
   
@endsection