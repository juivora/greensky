@extends('layouts.backend')
@section('title','Complaints')
@section('pageTitle','Complaints')
@section('content')
  
   
          <style>
        .csscontainer {
            display: grid;
            grid-template-columns: 2fr 2fr 2fr;
            grid-row-gap: 20px;
            grid-column-gap: 20px;
            grid-template-areas: 
            "complaintname dateofcomplaint ."
            "complaintnumber complaintemail ."
            "complaintaddress complaintaddress ."
            "category status ."
            "complaintmessage complaintmessage ."
            "assigneduser . ."
            "firstActionDate actiontake ."
            "dateresolved outcome ."
            "companychanges furtheractions ."
            ". submitarea ."
        }
        .dateofcomplaint {
            grid-area: dateofcomplaint;
        }
        .complaintname {
            grid-area: complaintname;
        }
        .complaintnumber {
            grid-area: complaintnumber;
        }
        .complaintemail {
            grid-area: complaintemail;
        }
        .complaintaddress {
            grid-area: complaintaddress;
        }
        .category {
            grid-area: category;
        }
        .status {
            grid-area: status;
        }
        .complaintmessage {
            grid-area: complaintmessage
        }
        .assigneduser {
            grid-area: assigneduser
        }
        .firstActionDate {
            grid-area: firstActionDate
        }
        .actiontake {
            grid-area: actiontake
        }
        .dateresolved {
            grid-area: dateresolved
        }
        .outcome {
            grid-area: outcome
        }
        .companychanges {
            grid-area: companychanges
        }
        .furtheractions {
            grid-area: furtheractions
        }
        .submitarea {
            grid-area: submitarea
        }
       </style>
         <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                            New Complaint
                    </div>
                </div>
                <div class="box-content ">
                     <a href="{{url('/admin/complaints') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                        </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::model( [
                                        'method' => 'POST',
                                        'url' => ['/admin/complaint'],
                                        'files' => true
                                    ]) !!}

                    <div class="csscontainer">
                           <input type="hidden" name="_method" value="POST">
                        <div class="dateofcomplaint">Date Of Initial Complaint: <input type="text" name="created_at" id="created_at"  class="form-control" placeholder="dd-MM-yyyy"></div>
                        <div class="complaintname">Complainant Name: <input type="text" name="firstname" id="firstname"  class="form-control" placeholder="First name"><input type="text" name="lastname" id="lastname"  class="form-control" placeholder="Last name"></div>
                        <div class="complaintnumber">Complainant Contact Number: <input type="text" name="contactnumber" id="contactnumber"  class="form-control" placeholder="000-000-0000"></div>
                        <div class="complaintemail">Complainant Email: <input type="text" name="email" id="email"  class="form-control" placeholder="Email"></div>
                        <div class="complaintaddress">Complainant Address: <input type="text" name="address" id="address"  class="form-control" placeholder="Address"><br><input type="text" name="suburb" id="suburb"  class="form-control" placeholder="Suburb"><br>
                            <input type="text" name="state" id="state"  class="form-control" placeholder="State"><br></div>
                        <div class="status">Status: 
                                            <select name='status' id='status'  class="form-control">
                                                <option value='New' >New</option>
                                                <option value='In progress'  >In progress</option>
                                                <option value='Await customer feedback'  >Await customer feedback</option>
                                                <option value='Closed'  >Closed</option>
                                            </select>
                        </div>
                        <div class="category">Category: 
                        <select name='category' id='category'  class="form-control">
                            <option value="System not generating as expected" >System not generating as expected</option>
                            <option value="System not working">System not working</option>
                            <option value="Warranty issue" >Warranty issue</option>
                            <option value="Payment issue">Payment issue</option>
                            <option value="Connection to grid">Connection to grid</option>
                            <option value="Issue with product">Issue with product</option>
                            <option value="Sales or complaints process">Sales or complaints process</option>

                                            </select>
                                        </div>
                        <div class="complaintmessage">Nature of complaint: <textarea style="height:150px" name="complaintmessage" id="complaintmessage"  class="form-control" placeholder="Message"></textarea></div>
                        <div class="assigneduser">Company Rep : 
                            <select id="assigneduser" class="form-control" name="assigneduser">
                                @foreach($sales as $sale)
                                <option value="{{$sale->id}}" >{{$sale->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="firstActionDate">Date first acted on: 
                         <input type="text" name="firstActionDate" id="firstActionDate"  class="form-control" value="" placeholder="dd-MM-yyyy"></div>
                        <div class="actiontake">Action Taken: 
                         <textarea class="form-control" id="actiontaken" style="height:150px" name="actiontaken" placeholder="Action taken detail"></textarea></div>
                        <div class="dateresolved">Date resolved: 
                         <input type="text" id="dateresolved" name="dateresolved" value=""  class="form-control" placeholder="dd-MM-yyyy"></div>
                        <div class="outcome">Outcome: 
                         <textarea class="form-control" style="height:150px" id="outcome" name="outcome" placeholder="Outcome detail"></textarea></div>
                        <div class="companychanges">Any company changes to take place as a result of the outcome: 
                         <textarea class="form-control" style="height:150px" name="companychanges" id="companychanges" placeholder="Changes detail"></textarea></div>
                        <div class="furtheractions">Further action taken by complaintant: 
                         <textarea class="form-control" style="height:150px" name="furtheractions" id="furtheractions" placeholder=""></textarea></div>

                         <div class="submitarea"><button type="submit" class="btn btn-primary">Add</button></div>
                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
            </div>

          
      
        <!-- <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              
            </div>
        </div>-->
   
   
@endsection