<nav id='main-nav'>
    <div class='navigation' style="font-size: 10px">

        <ul class='nav nav-stacked'>
            <?php
            $active = function ($menu) {
                $active = 0;
                if(str_contains(Request::url(), $menu->url ) && ($menu->url!='' && $menu->url != "/admin" || (Route::getCurrentRoute()->uri() =="admin"))){
                    $active = 1;
                }
                if(!$active && isset($menu->items)){
                    foreach($menu->items as $me){
                        if(str_contains(Request::url(), $me->url) && ($me->url!='' && $me->url != "/admin" || (Route::getCurrentRoute()->uri() =="admin"))){
                            $active = 1;
                            break;
                        }
                    }
                }
                return $active;
            };

            $accessible = function ($menu) {
                $isaccessible = 0;
                // if($menu->allow_site == "master" && !_MASTER){
                //     $isaccessible = 0;
                // }else{
                //     $isaccessible = 1;
                // }
                if(isset($menu->permission) && Auth::user()->can($menu->permission)){
                    $isaccessible = 1;
                }else{
                    $isaccessible = 0;
                }
              //  $isaccessible = 1;
                return $isaccessible;
            };

            ?>
<?php //print_r($laravelAdminMenus);exit;?>

            @foreach($laravelAdminMenus->menus as $section)
                
               @if($accessible($section))
               <li class="{{ ($active($section))? "active": "" }}">
                    <a class="{{ ($section->url)? "":"dropdown-collapse" }} {{ ($active($section))? "": "in" }}" href="{{ ($section->url) ? url($section->url) : "#"  }}"><i class="{{ $section->icon }}"></i>
                        <span> {{$section->title}} </span>
                        @if(isset($section->items) && count($section->items)>0) <i class="icon-angle-down angle-down"></i> @endif
                    </a>
                    @if(isset($section->items) && count($section->items)>0)
                        <ul class="in nav nav-stacked {{ ($active($section))? "": "in" }}" style="display: {{ ($active($section))? "block": 'none' }}">
                            @foreach($section->items as $menu)
                                @if($accessible($menu))
                                <li class="{{ ($active($menu))? "active": "" }}">
                                    <a href="{{ ($menu->url) ? url($menu->url) : "#"  }}"><i class="{{ $menu->icon }}"></i>
                                        <span> {{$menu->title}}</span>
                                    </a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </li>
                @endif


            @endforeach


        </ul>
    </div>
</nav>