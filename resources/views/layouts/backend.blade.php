<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Gesmansys' name='description'>
    {{--  <link href='{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}' rel='shortcut icon' type='image/x-icon'>  --}}

    <link rel="stylesheet" href="{!! asset('/assets/stylesheets/plugins/fuelux/wizard.css') !!}">

    <!-- / END - page related stylesheets [optional] -->
    <!-- / bootstrap [required] -->
    <link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all" rel="stylesheet"  type="text/css"/>

    <link href="{!! asset('css/bootstrap-datetimepicker.css') !!}" media="all" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <!-- / theme file [required] -->
    <link href="{!! asset('assets/stylesheets/light-theme.css') !!}" media="all" id="color-settings-body-color"
          rel="stylesheet" type="text/css"/>
    <!-- / coloring file [optional] (if you are going to use custom contrast color) -->
    <link href="{!! asset('assets/stylesheets/theme-colors.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!-- / demo file [not required!] -->
    <link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/design.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/style.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!--<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('assets/build/css/intlTelInput.css')!!}">
     <!-- Range selector CSS --> 
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Date Time Picker -->
    <link href="{!! asset('css/bootstrap-datetimepicker.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}" type="text/javascript"></script>
    

  


   


    <style>
        a:hover {
            text-decoration: none;
        }
    </style>
    <style>
        #main-nav .navigation > .nav > li .nav {
            background: #333244 !important;
        }
        #main-nav .navigation > .nav > li .nav > li > a {
            color: #fff;
            font-size: 16px;
            padding-left: 25px;
            border-top: none;
            padding: 0px 0px 0px 25px;
        }
        #main-nav .navigation > .nav > li .nav > li.active > a , #main-nav .navigation > .nav > li .nav > li > a:hover, #main-nav .navigation > .nav > li .nav > li > a:focus {
            background: transparent;
            color: #17A9A8;
        }

        #main-nav .navigation > .nav > li > .nav > li {
            padding: 10px;
        }
    </style>

    @yield('headExtra')

    @stack('css')

    {{-- Data Table--}}
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

</head>
<body class='contrast-red   '>
<header>
    <nav class='navbar navbar-default'>
        <a class='navbar-brand' href='{!! url('/') !!}'>
            {{--  @if(_WEBSITE_LOGO)
                <img height="31" src="{!! asset('uploads/website/'._WEBSITE_LOGO) !!}"/>
            @else
                <img height="31" src="{!! asset('assets/images/logo_lg.png') !!}"/>
            @endif  --}}
            <img class="logo" src="{!! asset('assets/images/new_logo.png') !!}"/>
        </a>
        <a class='toggle-nav btn pull-left' href='#'>
            <i class='icon-reorder'></i>
        </a>
        <ul class='nav'>


            {{--@include('partials.language')--}}

            {{--  @include('partials.notifications')  --}}

            <li class='dropdown dark user-menu'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>

                    @if(isset(Auth::user()->people->photo))
                        <img width="23" height="23" alt="{!! Auth::user()->name !!}"
                             src="{!! asset('uploads/'.Auth::user()->people->photo) !!}"/>
                    @endif
                    <span class='user-name'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                    <li>
                        <a href='{!! url('admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class='icon-signout'></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
        </ul>

    </nav>
</header>
<div id='wrapper'>
    <div id='main-nav-bg'></div>

    @include('partials.sidebar')

    <section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                {{--<h1 class="pull-left">
                                    @yield('title','<i class="icon-tint"></i>
                                    <span>Home</span>')

                                </h1>--}}
                            </div>
                        </div>
                    </div>

                    @if (Session::has('flash_message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif

                    @include('flash::message')

                    @yield('content')


                </div>
            </div>
            <footer id='footer'>
                <div class='footer-wrapper'>
                    <div class='row'>
                        <div class='col-sm-6 text'>
                            Copyright Â© 2017 {{ config('app.name') }}
                        </div>
                        <div class='col-sm-6 buttons'>

                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </section>
</div>



<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>


<script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"
        type="text/javascript"></script>

<script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
<!-- / modernizr -->


<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>

<!--select 2 js for multiple selection-->
<script src="{!! asset('assets/javascripts/select2.js') !!}" type="text/javascript"></script>


{{-- Data Table--}}
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="{!! asset('assets/build/js/intlTelInput.js')!!} " type="text/javascript"></script>

<!--jquery validate -->
<script src="{!! asset('assets/javascripts/plugins/validate/jquery.validate.min.js')!!}"></script>


<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>

<script>
    //checkbox
    /*
            $("input:checkbox").click(function() {
                if ($(this).is(":checked")) {
                    var group = "input:checkbox[name='" + $(this).attr("name") + "']";
                    $(group).prop("checked", false);
                    $(this).prop("checked", true);
                } else {
                    $(this).prop("checked", false);
                }
            });
            */
    

    //Generator add button script
    $( document ).ready(function() {
        $(document).on('click', '.btn-add', function(e) {
            e.preventDefault();

            var tableFields = $('.table-fields'),
                currentEntry = $(this).parents('.entry:first'),
                newEntry = $(currentEntry.clone()).appendTo(tableFields);

            newEntry.find('input').val('');
            tableFields.find('.entry:not(:last) .btn-add')
                .removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="fa fa-minus"></span>');
        }).on('click', '.btn-remove', function(e) {
            $(this).parents('.entry:first').remove();

            e.preventDefault();
            return false;
        });

    });


    $(document).ready(function ($) {


        $('#flash-overlay-modal').modal();

        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery('.selectTag').select2({
            tokenSeparators: [",", " "],
        });


        $('.time_pick').datetimepicker({
            format: 'LT'
        });
    });


</script>

@yield('footerExtra')
@stack('js')

<!-- / START - page related files and scripts [optional] -->

<!-- / END - page related files and scripts [optional] -->
</body>
</html>