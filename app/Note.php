<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //
    protected $table = 'notes';

    /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'id';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['customer_id','note','user_id','created_at'];


    public function user(){
        return $this->hasOne('App\User','id','user_id'); 
    }

    public function customer(){
        return $this->hasOne('App\Party','id','customer_id');
    }
}
