<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    //
    protected $table = 'complaints';

    /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'id';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['firstname','lastname','email','contactnumber','dateofinstall','complaintmessage','assigneduser','firstActionDate','actiontaken','dateresolved','outcome','companychanges','furtheractions','status','created_at','address','state','suburb','category'];


        public function assigneduserdetails(){
        return $this->hasOne('App\User','id','assigneduser');
    }
}
