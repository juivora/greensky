<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FollowUp extends Notification
{
    use Queueable;

    public $offer_data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($offer)
    {
        //
        $this->offer_data = $offer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {   
        
        return (new MailMessage)
                        ->subject('GreenSky Follow Up')
                        ->greeting('Hello, ' . $notifiable->name)
                        ->line('Offer-id - '.$this->offer_data->id.' for Customer - '.$this->offer_data->customer->first_name.' '.$this->offer_data->customer->last_name.' was created on '.$this->offer_data->created_at->format('d-m-Y') .' and is still in Awaiting status. 
                        Please do the needful.')
                        ->action('View Offer', url('/admin/viewoffer/'.$this->offer_data->id))
                        ->line('Thank you for using our application!');

                    /*
	Hello <username>, 
	
	Offer <link><offername></link> for <customername> was created on <date> and is still in Awaiting status. 
	Please do the needful. 
	
	Regards
    GreenSky Admin. 
    */
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
