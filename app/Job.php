<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{   
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['rff', 'pvd', 'connection_type', 'alternative_supply', 'supply_required', 'length_of_overhead_cable', 'pit_installed', 'oh_to_ug_conversion', 'meter_phase', 'no_of_phases', 'phase_size', 'max_demand_installation', 'max_amp_per_phase', 'max_demand_of_total_site', 'demands_amp_per_phase', 'sccd_installed', 'sccd_amps', 'truck_appointment', 'lei_name', 'access_to_meter', 'access_to_vpi_lock', 'access_notes', 'no_of_stc', 'battery_storage', 'solar', 'battery_storage_source', 'embd_gen_installed', 'existing_rating', 'existing_export', 'pre_approval_ref_no', 'manual_to_customer', 'instructed_customer', 'inverter_time_disconnect', 'disconnect_test', 'inverter_time_reconnect', 'reconnect_test', 'export_test_sop_3306', 'export_test_sheet_attached', 'installation_comments', 'no_of_panels', 'replacing', 'no_of_replacing', 'additional', 'no_of_additional', 'more_than_one_system', 'loc_of_other_system', 'solar_capacity_installed', 'solar_capacity_installed_phase_a', 'solar_capacity_installed_phase_b', 'solar_capacity_installed_phase_c', 'solar_capacity_existing', 'solar_capacity_existing_phase_a', 'solar_capacity_existing_phase_b', 'solar_capacity_existing_phase_c', 'solar_capacity_removed', 'solar_capacity_removed_phase_a', 'solar_capacity_removed_phase_b', 'solar_capacity_removed_phase_c', 'battery_capacity_installed', 'battery_capacity_installed_phase_a', 'battery_capacity_installed_phase_b', 'battery_capacity_installed_phase_c', 'battery_capacity_existing', 'battery_capacity_existing_phase_a', 'battery_capacity_existing_phase_b', 'battery_capacity_existing_phase_c', 'battery_capacity_removed', 'battery_capacity_removed_phase_a', 'battery_capacity_removed_phase_b', 'battery_capacity_removed_phase_c','deleted_at','inverter_phase','capacity_phase_a','capacity_phase_b','capacity_phase_c','export_limit','export_limit_value','customer_id','email_to_retailer','pre_approval_number','deposit_or_finance','offer_id','total_wt','installer_id','status','installation_date','package_id','no_of_panels_value', 'electrical_safety' , 'ces_number', 'ces_completion_date', 'ces_completion_date', 'contact_project_group','project_number','contact_project_group_name','premise_type','public_land','epv','traffic_control', 'solar_panel_value','inverter_value','meter_phase_other','electrical_name','acceptance_of_charge', 'termination' ,'embd_network','Questionnaire','bridgeselectID' ];


    public function customer(){
        return $this->hasOne('App\Party','id','customer_id');
    }

    public function installer(){
        return $this->hasOne('App\User','id','installer_id');
    }

    public function electrical(){
        return $this->hasOne('App\ED','id','electrical_id');
    }

    public function designer(){
        return $this->hasOne('App\ED','id','designer_id');
    }
    
}
