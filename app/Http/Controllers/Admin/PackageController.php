<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\Product;
use App\PackageProduct;
use Session;
use Yajra\Datatables\Datatables;
use App\Folders;
use File;

class PackageController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.packages');
        $this->middleware('permission:access.package.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.package.create')->only(['create', 'store']);
        $this->middleware('permission:access.package.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $keyword = $request->get('search');
        // $perPage = 25;
        // if (!empty($keyword)) {
        //     $packages = Package:: where('title', 'LIKE', "%$keyword%")            
        //     ->orWhere('price', 'LIKE', "%$keyword%")
        //     ->latest()
        //     ->paginate($perPage);
        // }else{
        //     $packages = Package::latest()->paginate($perPage);
        // }
        return view('admin.package.index');
    }

      public function datatable(Request $request){

        $packages = Package::with('package_product');

        return Datatables::of($packages)->addColumn('package_product', function($packages) {
            $pname = array();
            foreach($packages->package_product as $products){
                    $pname[] = $products->name;
            }
            return $pname;
        })->make(true);


    }

    /**
    Used for the export of all the packages for price updates
    */
    public function exportPackages() {
        $packages = Package::with('package_product')->get();
        $tot_record_found=0;
        if(count($packages)>0){
            $tot_record_found=1;
            //First Methos          
            $export_data="PriceID (DONOT CHANGE),Price,Package Title, 1st ProductID (DONOT CHANGE),1st Brand, 1st Name,1st Power Output,1st Type,1st  QTY, 2nd ProductID (DONOT CHANGE),2nd Brand, 2nd Name,2nd Power Output,2nd Type,2nd  QTY , 3rd ProductID (DONOT CHANGE),3rd Brand, 3rd Name,3rd Power Output,3rd Type,3rd  QTY, 4th ProductID (DONOT CHANGE),4th Brand, 4th Name,4th Power Output,4th Type,4th  QTY,5th ProductID (DONOT CHANGE),5th Brand, 5th Name,5th Power Output,5th Type,5th  QTY, 6th ProductID (DONOT CHANGE),6th Brand, 6th Name,6th Power Output,6th Type,6th  QTY\n";
            foreach($packages as $value){
                $export_data.=$value->id.','.$value->price.',"'.$value->title.'"';
                $packageproduct = $value->package_product;

                foreach ($packageproduct as $product) {
                    $export_data.=','.$product->product_id.','.$product->brand.','.$product->name.','.$product->kw.','.$product->category_id.','.$product->quantity;
                }
                $export_data.="\n";
                 
                //return $export_data;
            }
            return response($export_data)
                ->header('Content-Type','application/csv')               
                ->header('Content-Disposition', 'attachment; filename="package_export.csv"')
                ->header('Pragma','no-cache')
                ->header('Expires','0');                     
        }
        return view('download',['record_found' =>$tot_record_found]);    
    }

    /**
    Used for the export of all the packages for price updates
    */
    public function uploadPackagePrices(Request $request) {
        //*
        if($request->hasFile('packages_price_file') && $request->packages_price_file != '' ){
                   
            $image = $request->file('packages_price_file');         
            $filename =  'packagesupdate'.'.'.$image->getClientOriginalExtension();
            $image->move(public_path('package/'), $filename);
                    
            $packageArr = $this->csvToArray(public_path('package/'.$filename));

            for ($i = 0; $i < count($packageArr); $i ++)
            {
                $data[] = $packageArr[$i];
                
                $package = Package::where('id',$packageArr[$i][0])->first();
                if($package){
                    $package->price = $packageArr[$i][1];
                    $package->updated_at = \Carbon\Carbon::now();
                    $package->save();
                }
            }
            Session::flash('flash_message', 'Update completed successfully. ');
        }

        return view('admin.package.index');

    } 

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = $row;
            }
            fclose($handle);
        }

        return $data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::with('category')->where('feature',1)->get(); 

		return view('admin.package.create',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       // dd($request->all());
        $this->validate($request, [
            'title' => 'required',
            'products' => 'required', 
            'image' => 'sometimes|mimes:jpeg,jpg,png,gif', 
            'package_image' => 'sometimes',        
        ]);
        
        $data = $request->all();

        $package = Package::create($data);

        if($package){
             if ($request->file('image')) {

                $fimage = $request->file('image');           
                $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
                $fimage->move(public_path('/package/'.$package->id), $filename);
                $package->image = $filename;
                $package->save();

            }
            if ($request->file('package_image')) {

                $package_image = $request->file('package_image');           
                $filename = uniqid(time()) . '.' . $package_image->getClientOriginalExtension();
                $package_image->move(public_path('/package/'.$package->id), $filename);
                $package->package_image = $filename;
                $package->save();

            }
        }

       
        //echo '<pre>';print_r($data);exit;
       

        $products = $request->products;    
        if ($request->has('products')) {
            foreach($products as $key => $product){
                $productData['package_id'] = $package->id;
                $productData['product_id'] = $product;
                $productData['quantity'] = $request->quantity[$key];
                $productData['kw'] = $request->rated_power_output[$key];
                PackageProduct::create($productData); 
            }
        }

        Session::flash('flash_message', 'Package added!');
        return redirect('admin/packages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::where('id',$id)->first();
        if($package){
            return view('admin.package.show',compact('package'));
        }else{
            return redirect('admin/packages');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::where('id',$id)->first();
        if($package){
            $products = Product::with('category')->where('feature',1)->get(); 

            $packageproduct = PackageProduct::with('product')->where('package_id',$id)->get();
            
            return view('admin.package.edit', compact('package','products','packageproduct'));
        }else{
            return redirect('admin/packages');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        $this->validate($request, [
            'title' => 'required',
            'products' => 'required',   
            'image' => 'sometimes|mimes:jpeg,jpg,png,gif', 
            'package_image' => 'sometimes',          
        ]);

        $requestData = $request->all(); 

        $package = Package::findOrFail($id);
       

         if ($request->file('image')) {

            if($package->image){
                $file = public_path('/package/'.$id.'/'.$package->image); 
                if (file_exists($file)) {
                    unlink($file);
                }
            }

            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/package/'.$id), $filename);
            $requestData['image'] = $filename;

        }
        if ($request->file('package_image')) {

            if($package->package_image){
                $file = public_path('/package/'.$id.'/'.$package->package_image);
                if (file_exists($file)) {
                    unlink($file);
                }
            }
            
            $fimage = $request->file('package_image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/package/'.$id), $filename);
            $requestData['package_image'] = $filename;

        }
        // echo '<pre>';print_r($requestData);exit; 
       
        $products = $request->products;   

        if ($request->has('products')) {

            $packageProduct = PackageProduct::where('package_id',$id);
            $packageProduct->delete();
          
            if(count($products) > 0){
                
                foreach($products as $key => $product){
                    
                    if($product == "Select"){

                        Session::flash('flash_message','Your Selected Package is not featured, Please Select another Package!');
		
                        return redirect()->back();
                    }
                    if($product && $product != ''){

                        $productData['package_id'] = $package->id;
                        $productData['product_id'] = $product;
                        $productData['quantity'] = $request->quantity[$key];
                        $productData['kw'] = $request->rated_power_output[$key];
                        PackageProduct::create($productData); 
                    } 
                }
                
            }
        }

        $package->update($requestData);

        Session::flash('flash_message','Package Updated Successfully!');
		
        return redirect('admin/packages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $package =  Package::find($id);
        if($package->image){
            $file = public_path('/package/'.$id.'/'.$package->image); 
            if (file_exists($file)) {
                unlink($file);
            }
        }

        if($package->package_image){
            $file = public_path('/package/'.$id.'/'.$package->package_image);
            if (file_exists($file)) {
                unlink($file);
            }
        }
        $package_product = PackageProduct::where('package_id',$id)->get();
        foreach($package_product as $pk_pr){
            $pk_pr->delete();
        }
        $package_folder = Folders::where('package_id',$id)->get();
        foreach($package_folder as $pk_fl){
            if($pk_fl->path){
                $file = public_path('/package/'.$id.'/'.$pk_fl->path);
                if (file_exists($file)) {
                    unlink($file);
                }
            }
            $pk_fl->delete();
        }

        $package->delete();

        $message = "Package Deleted Successfully !!";

        return response()->json(['messages' => $message],200);
    }

    public function addfolder(Request $request,$id){
        
        $package = Package::where('id',$id)->with('folders')->first();

        if(!$package){
            return redirect()->back();
        }
     
        if ( $request->hasFile('document')) {

            $documents = $request->file('document');
            foreach ($documents as $document){
              
                $filename = uniqid(time()) . '.' . $document->getClientOriginalExtension();
                $path = public_path()."/package/".$id;
                \File::makeDirectory($path, $mode = 0777, true, true);
                $document->move(public_path('package/'.$id), $filename);

                $folder = new Folders;
                $folder->package_id = $id;
                if($request->has('name')){
                    $folder->name = $request->get('name').'.'.$document->getClientOriginalExtension();
                }
                else{
                    $folder->name = $filename;
                }
                $folder->path = $filename;
                $folder->save();
            }    
            return redirect()->back();
            
        }   
        
        return view('admin/package/addfolder',compact('package'));
    }


    public function deletefolder($id){

        $folder = Folders::find($id);
        if($folder->path != ""){
            $file = public_path('/package/'.$folder->package_id.'/'.$folder->path);
            if (file_exists($file)) {
                unlink($file);
            }
        }

        $folder->delete();

        Session::flash('flash_message','File Deleted Successfully!');            
        return redirect()->back();

    }
  

    public function duplicate($id){
        
        $package = Package::find($id);
        $new_package =  $package->replicate();
        $new_package->save();
        if($package->image){
            $filename = $package->image;
            
            $file = public_path('/package/'.$id.'/'.$filename);
            
            $path = public_path()."/package/".$new_package->id;
            \File::makeDirectory($path, $mode = 0777, true, true);
            
            $new_file = public_path('/package/'.$new_package->id.'/' .$filename);
            if (file_exists($file)) {
                copy($file, $new_file);
            }
        }
        if($package->package_image){
            $filename = $package->package_image;
            
            $file = public_path('/package/'.$id.'/'.$filename);
            
            $path = public_path()."/package/".$new_package->id;
            \File::makeDirectory($path, $mode = 0777, true, true);
            
            $new_file = public_path('/package/'.$new_package->id.'/' .$filename);
            if (file_exists($file)) {
                copy($file, $new_file);
            }
        }

        $package_product = PackageProduct::where('package_id',$id)->get();
        foreach($package_product as $pk_pr){
            $new_pk_pr  =  $pk_pr->replicate();
            $new_pk_pr->package_id = $new_package->id;
            $new_pk_pr->save();
        }

        $package_folder = Folders::where('package_id',$id)->get();
        foreach($package_folder as $pk_fl){
            $new_pk_fl  =  $pk_fl->replicate();
            $new_pk_fl->package_id = $new_package->id;
            $new_pk_fl->save();
            $filename = $pk_fl->path;

            $file = public_path('/package/'.$id.'/'.$filename);

            $path = public_path()."/package/".$new_package->id;
            \File::makeDirectory($path, $mode = 0777, true, true);

            $new_file = public_path('/package/'.$new_package->id.'/' .$filename);
            if (file_exists($file)) {
                copy($file, $new_file);
            }
           
        }

        return redirect()->back();
    }



}
