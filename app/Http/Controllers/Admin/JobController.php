<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use App\States;
use App\Party;
use App\Setting;
use App\Folders;
use App\Job;
use App\User;
use App\Offer;
use App\UserStates;
use App\PackageProduct;
use App\CustomerOffer;
use App\Providers;
use App\Package;
use App\ED;
use Mail;
use File;
use Auth;
use mPDF;
use DB;
use Illuminate\Http\Response;


class JobController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.jobs');
        $this->middleware('permission:access.job.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.job.create')->only(['create', 'store']);
        $this->middleware('permission:access.job.delete')->only('destroy');
    }

    public function save_pdf( $htmlview, $filename ){
        
        $mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $mPdf->SetDisplayMode('fullpage');
        $mPdf->list_indent_first_level = 0;
        $mPdf->WriteHTML($htmlview);
        return $mPdf->Output($filename);

    }

    public function download_pdf( $htmlview, $filename ) {

        $mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $mPdf->SetDisplayMode('fullpage');
        $mPdf->list_indent_first_level = 0;
        $mPdf->WriteHTML($htmlview);
        return $mPdf->Output($filename, 'D');
    }

    public function download_file($id, $filename ) {

        $file= public_path(). "/documents/".$id."/".$filename;   
        $headers = array(
        'Content-Type: ' . File::mimeType($file),
        'pragma : no-cache',
        'Cache-Control : no-store,no-cache, must-revalidate, post-check=0, pre-check=0'
        );

        //return $file;
        return response()->download($file, $filename,$headers );
    }

    public function view_pdf( $htmlview, $filename ){

        $mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $mPdf->SetDisplayMode('fullpage');
        $mPdf->list_indent_first_level = 0;
        $mPdf->WriteHTML($htmlview);
        return $mPdf->Output($filename, 'I');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   

         $customer_id = 0;
         $customer_id = $request->customer_id;
         $customer= Party::find($customer_id);
        
        return view('admin.job.index', compact('customer','customer_id'));
    }

    public function datatable(Request $request){

      
        $job = Job::with('customer')
        ->select(DB::raw("CONCAT(parties.first_name,' ',parties.last_name) AS customer_name"),'jobs.*','parties.first_name','parties.last_name as last_name','parties.id as customer_id')
        ->leftjoin('parties','parties.id','=','jobs.customer_id');
        
        if(\Auth::user()->roles[0]->name == "Installers" ){
            $job->where('installer_id',\Auth::user()->id);
        }

        if ($request->has('customer_id') && $request->get('customer_id') != 0) {
            $job->where('customer_id',$request->customer_id);  
            
        }
       
        if ($request->has('filter') && $request->get('filter') != '') {

            if($request->get('filter') != 'All'){
                $job->where('jobs.status', $request->get('filter'));
            }
            
        }
 
        $job->get();

        return Datatables::of($job)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        
        $stc = '';
        $mode = '';
        $offer_id = 0 ;
        $total_wt = 0 ;
        $customer_id = $request->customer_id;
        $customer = Party::find($request->customer_id);
        $customers = Party::all()->pluck('first_name','id');

        return view('admin.job.create',compact('customers','customer_id','customer','stc','mode','offer_id','total_wt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {   
        
        $requestData = $request->all();

        if($request->export_limit == 'No'){
            $request->export_limit_value = '' ;
        }

        $requestData['customer_id'] = $request->customer;
        $requestData['deposit_or_finance'] = $request->deposit_or_finance;
        $requestData['offer_id'] = $request->offer_id;
        $requestData['meter_phase'] = json_encode($request->meter_phase);
        
        
        $job =  Job::create($requestData);
        
        if($job){

            $customer = Party::find($job->customer_id);

            $stc = $job->no_of_stc ; 

            $companyDetails =  array();

            $setting  = Setting::get();
            foreach($setting as $key => $value) {
                $companyDetails[$value['name']] = $value['value'];   
            }
            $companyDetails = (object) $companyDetails;
             
            $view = view('admin.customer.pdf-STC', compact('customer','companyDetails','stc'))->render();
            $pdf = \PDF::loadHTML($view);
            $file_stc = 'STC-'.$job->id.'.pdf';
            $path = public_path()."/documents/".$customer->id;
            \File::makeDirectory($path, $mode = 0777, true, true);
            $file= public_path()."/documents/".$customer->id."/".$file_stc;
            $pdf->save($file);
            
            $folder = new Folders;
            $folder->customer_id = $customer->id;
            $folder->name = $file_stc;
            $folder->path = $file_stc;
            $folder->save();

           

        }
        


        Session::flash('flash_message', 'Job added!');

        return redirect('admin/job');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show(Request $request,$id)
    {
        $job = Job::find($id);
         if(!$job){
            return redirect()->back();
        }
        $customer = Party::with('retailer')->find($job->customer_id);
        
        $installer = User::find($job->installer_id);

       
        $status =  $request->status;
    
        if($status != null){
            
            if($status == "Completed"){
                
                 if($job->sign == 0){
                    
                    $message = "Job completion prevented, not approved by the customer!!";

                    return response()->json(['messages' => $message],404);

                    return back();
                    
                }

                if (empty($job->pvd)) {
                    $message = "Job completion prevented, PVD number not completed.";

                    return response()->json(['messages' => $message],404);

                    return back();
                }
                
            }
            
            $job->status = $status;
            $job->save();
            
            if($job->status == "Await customer signature"){
                $this->job_sign_form_email($installer, $customer, $job);

            }
            
            if($job->status == "Completed"){

                //$job->completed_on = date('d-m-Y');
                $job->save();

              //  Warranty Files and every thign move on the customer folder. 
                $folders = Folders::where('package_id',$job->package_id)->get();
                
                foreach($folders as $folder){
                    $old_file = public_path()."/package/".$folder->package_id."/".$folder->path ;
                    if(file_exists($old_file)){
                        $new_file = public_path()."/documents/".$job->customer_id."/".$folder->path ;
                        copy($old_file,$new_file);
                        
                        $cust_folder = new Folders;
                        $cust_folder->customer_id = $job->customer_id;
                        $cust_folder->name = $folder->name;
                        $cust_folder->path = $folder->path;
                       // $cust_folder->save();
                    }
                    
                }

                //get company details
                $company_details =  array();
                
                $setting  = Setting::get();
                foreach($setting as $key => $value) {
                    $company_details[$value['name']] = $value['value'];   
                }
                $company_details = (object) $company_details;

                $date = date('d-m-Y');

                $ue_questionnaire = json_decode($job->Questionnaire);

                

                //Create EWR Form & store in Customer Folder
                // make ewr on complete
                $this->makeEWR($customer, $job, $installer, $company_details, $date, $ue_questionnaire);

                $companyDetails = $company_details;

                $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                    ->where('offer_id',$job->offer_id)
                                    ->where('package_id',$job->package_id)->first();

                $package = json_decode($customer_offer->package_obj);


                if (isset($ue_questionnaire)) {
                    if($customer->electrical_distributer == 'Jemena') {
                        $this->makeJemena($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
                    }
                    else if($customer->electrical_distributer == 'SP Ausnet') {
                        $this->makeAusnet($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
                    }
                    else if($customer->electrical_distributer == 'United Energy'){
                        $this->makeUnitedEnergy($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
                    }
                    else if($customer->electrical_distributer == 'Citipower' || $customer->electrical_distributer == 'Powercor') {
                        $this->makePowerCorCitiPower($customer, $job, $installer, $company_details, $date);
                    }
                }

                $products = json_decode($customer_offer->product_obj);

                $com_date = $job->completed_on ;  
                if($com_date){
                    $date =  date('d-m-Y', strtotime($com_date.' +1 day') ) ;
                }else{
                    $date = '';
                }
                $stc = $job->no_of_stc ; 
                $view_stc = view('admin.customer.pdf-STC', compact('customer','company_details','stc','job','installer','package','products','date'))->render();

                $pdf = new \mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
                $pdf->SetDisplayMode('fullpage');
                $pdf->list_indent_first_level = 0;
                $pdf->WriteHTML($view_stc);
                            
                $file_stc = 'STC-'.$job->id.'.pdf';
                $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$file_stc)->first();
        
                if($find_job_pdf){
                    if($find_job_pdf->path != null){
                        $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                        if (File::exists($old_file)) {
                            unlink($old_file);
                        }
                    }
                    $find_job_pdf->delete();
                }
                $path = public_path()."/documents/".$customer->id;
                \File::makeDirectory($path, $mode = 0777, true, true);
                $file= public_path()."/documents/".$customer->id."/".$file_stc;
                $pdf->output($file);

                $cust_folder_ewr = new Folders;
                $cust_folder_ewr->customer_id = $customer->id;
                $cust_folder_ewr->name = $ewr_file_name;
                $cust_folder_ewr->path = $ewr_file_name;
                $cust_folder_ewr->save();

                //get all the documents from Customer Folder
                $customer_folder_doc = Folders::where('customer_id',$job->customer_id)->get();
                $docs = array(); 
                foreach($customer_folder_doc as $document){
                    $file_path = public_path()."/documents/".$job->customer_id."/".$document->path;
                    if(File::exists($file_path)){
                        $docs[] = Folders::find($document->id);
                    }
                } 
               
               
               
               
                //Send mail when job is completed
                $sender_name = Setting::give_value_of('Sender_Name');
                $sender_email = Setting::give_value_of('Sender_Email');
                $sender_cc = Setting::give_value_of('Sender_CC');
                $cc_array = [];
                $cc_array = explode(',', $sender_cc);
                $sender_bcc = Setting::give_value_of('Sender_BCC');
                $bcc_array = [];
                $bcc_array = explode(',', $sender_bcc);

                $sales = User::find($customer->sales_id);
                if( $sales && $sales->email != ""){
                   // $cc_array[] =  $sales->email ; 
                     $cc_array = explode(',',  $sales->email);
                    $sender_email = $sales->email;
                    $sender_name = $sales->name;
                }
               
                if($customer && $installer){
                    if($customer->email && $sender_email && $sender_name){

                        \Mail::send('email.order.jobCompleted', compact('customer','installer','docs'), function ($message) use ($customer,$sender_email,$sender_name, $cc_array,$bcc_array, $docs) {
                            
                            $message->from($sender_email, $sender_name)
                                ->to($customer->email)
                                ->cc($cc_array)
                                ->bcc($bcc_array)
                                ->subject('Job Completed');
                            foreach($docs as $doc){
                                $message->attach(public_path()."/documents/".$customer->id."/".$doc->path, ['as'=>$doc->name]);
                            }
                           
                          
                        });
                    }
                }
                
                // Send mail to Provider & Retailer for Grid Form - EWR Form PDF
                

                /*$pdf = \PDF::loadHTML($view_ewr); 

                $provider = Providers::find($customer->provider_id);
                if($provider ){
                    if($provider->email && $sender_email && $sender_name){

                        \Mail::send('email.order.gridform', compact('customer','provider'), function ($message) use ($customer,$provider,$sender_email,$sender_name, $cc_array,$bcc_array,$pdf, $ewr_file_name) {

                            $subject = 'Grid Forms -'.$customer->street_number.' '.$customer->street_name.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code.' ,   '.$customer->nmi; 

                             $message->from($sender_email, $sender_name)
                                ->to($provider->email)
                                ->cc($cc_array)
                                ->bcc($bcc_array)
                                ->subject($subject)
                                ->attachData($pdf->stream($ewr_file_name), $ewr_file_name);  
                          
                        });
                    }
                }
                
                if($customer ){
                    if($customer->email && $sender_email && $sender_name){

                        \Mail::send('email.order.gridform', compact('customer','provider'), function ($message) use ($customer,$provider,$sender_email,$sender_name, $cc_array,$bcc_array,$pdf,$ewr_file_name ) {

                            $subject = 'Grid Forms -'.$customer->street_number.' '.$customer->street_name.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code.' ,   '.$customer->nmi; 

                             $message->from($sender_email, $sender_name)
                                ->to($customer->email)
                                ->cc($cc_array)
                                ->bcc($bcc_array)
                                ->subject($subject)
                                ->attachData($pdf->stream($ewr_file_name), $ewr_file_name); 
                          
                        });
                        
                    }
                }
                */
                
                
                
    
            }

            $message = "Job's Status Changed Successfully !!";

            return response()->json(['messages' => $message],200);

        }else{

            return view('admin.job.show', compact('job'));
        }

       
    }


 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $job = Job::find($id);
        
        if(!$job){
            return redirect()->back();
        }

        $customers = Party::all()->pluck('first_name','id');

        $users = User::with('roles')->get();
        $id = [];
        foreach($users as $user){
            foreach($user->roles as $role){
                if($role->name == "Installers"){
                    $id[] = $user->id;
                }
            }
        }
        $designer = ED::find($job->designer_id);
        $electrical = ED::find($job->electrical_id);
        $installers = User::whereIn('id',$id)->pluck('name','id')->prepend('Select Installer','');

        $customer = Party::find($job->customer_id);

            $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                        ->where('offer_id',$job->offer_id)
                                        ->where('package_id',$job->package_id)->first();

           
            $products = json_decode($customer_offer->product_obj);

            $package = json_decode($customer_offer->package_obj);

            $package_product = PackageProduct::where('package_id',$package->id)->get();
                        
            $product_obj = [];
            
            $inverter_total_watt = 0;
            
            foreach($products as $product){
                if($product->category->name == "Solar Inverter"){
                    $quantity = 0;
                    foreach ($package_product as $pk_pr){
                        if($product->id ==  $pk_pr->product_id) {
                            $quantity = $pk_pr->quantity;
                        }
                    } 

                     $inverter_total_watt = $product->rated_power_output *   $quantity;
                } 
            }

        return view('admin.job.edit', compact('job','customers','installers','electrical','designer','inverter_total_watt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {   
        $requestData = $request->all();
        if($request->has('meter_phase') && $request->meter_phase != null ){ 
            $requestData['meter_phase'] = json_encode($request->meter_phase);
        }
        if($request->export_limit == 'No'){
            $request->export_limit_value = '' ;
        }

        $job = Job::findOrFail($id);

        $job->update($requestData);
        
        // Add or Update Designer
        $d = $request->d ;
        if($job->designer_id == ''){
           
                $designer = ED::create($d);
                if($designer){
                    $designer->type = "Designer" ;
                    $designer->save();
                    $job->designer_id = $designer->id ;
                    $job->save();
                }
        }else{
            $des = ED::find($job->designer_id);
            if($des){
                $des->update($d);
            }
        }

        //Add or update Electrical
        $e = $request->e ;
        if($job->electrical_id == ''){

                $electrical = ED::create($e);
                if($electrical){
                    $electrical->type = "Electrical" ;
                    $electrical->save();
                    $job->electrical_id = $electrical->id ;
                    $job->save();
                }
   
        }else{
            $el = ED::find($job->electrical_id);
            if($el){
                $el->update($e);
            }
        }

        // get Package & Product details 
        $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                    ->where('offer_id',$job->offer_id)
                                    ->where('package_id',$job->package_id)->first();


        $package = json_decode($customer_offer->package_obj);

        $products = json_decode($customer_offer->product_obj);
        
        // Set no of panels in Job
                    $panel_count = 0 ; $qty = 0 ;
                    foreach($products as $product){
                        if($product->category->name == "Solar Panel"){
                            $panel_count = $panel_count + 1; 
                            $qty = $qty + $product->quantity; 
                        } 
                    }
            
                    if($panel_count == 0){
                        $job->update(['no_of_panels'=>"No"]);
                    }else{
                        $job->update(['no_of_panels'=>"Yes" , 'no_of_panels_value' =>  $qty ]);
                    }
                    


        $customer = Party::find($job->customer_id);

        $installer = User::find($job->installer_id);

        if ($job->bridgeselectID == "") {
            $result = $this->createBridgeSelectSTC($job, $customer,$installer);

            //return response()->json($result);
            //response()->json(['messages' =>  env("BRIDGEURL")],200);  
                            
            if(isset($result->Success))
            {
                if ($result->Success->Code == "1000") {
                    $job->bridgeselectID = $result->Success->Details->bselectid;
                    $job->save();
                    Session::flash('flash_message', 'Success Bridge Select Job Created');
                    //return response()->json(['messages' => "Success Bridge Select ".$job->bridgeselectID ],200);  
                }
                else {
                    Session::flash('flash_message', "Failure Bridge Select ".$result->Success->Code."-".$result->Success->Description);
                    return redirect()->back();
                    //return response()->json(['messages' => "Failure Bridge Select ".$result->Code."-".$result->Description ],200);  
                }
                                    

            }
            else {
                Session::flash('flash_message', "Failure Bridge Select ".$result->Error->Code."-".$result->Error->Description);
                return redirect()->back();
            }
        }
        
        
        $stc = $job->no_of_stc ; 
        
        $companyDetails =  array();
        
        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $companyDetails[$value['name']] = $value['value'];   
        }
        $companyDetails = (object) $companyDetails;

        $job_pdf = 'STC-'.$job->id.'.pdf' ;

        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$job_pdf)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                }
            }
            $find_job_pdf->delete();
        }
    // Electrical distributor pdf        
        $ed_pdf = $customer->electrical_distributer.'-'.$job->id.'.pdf' ;

        $find_ed_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$ed_pdf)->first();
        
        if($find_ed_pdf){
            if($find_ed_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_ed_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                }
            }
            $find_ed_pdf->delete();
        }

        $com_date = $job->completed_on ;  
        if($com_date){
            $date =  date('d-m-Y', strtotime($com_date.' +1 day') ) ;
        }else{
            $date = '';
        }

        $job = Job::with('electrical','designer')->find($id);
        $view = view('admin.customer.pdf-STC', compact('customer','companyDetails','stc','job','installer','package','products','date'))->render();
        
        
          
        $pdf = new \mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $pdf->SetDisplayMode('fullpage');
        $pdf->list_indent_first_level = 0;
        $pdf->WriteHTML($view);
                    
        $file_stc = 'STC-'.$job->id.'.pdf';
        $path = public_path()."/documents/".$customer->id;
        \File::makeDirectory($path, $mode = 0777, true, true);
        $file= public_path()."/documents/".$customer->id."/".$file_stc;
        $pdf->output($file);

        $folder = new Folders;
        $folder->customer_id = $job->customer_id;
        $folder->name = $file_stc;
        $folder->path = $file_stc;
        $folder->save();      
        
         // Generate PDF of Customer's Electrical Distributor 
         $pdfType = $customer->electrical_distributer;
         

         if($pdfType != ''  && $pdfType != null) {

             $form_no = $pdfType.'-'.$job->id ;
             $date = date('d-m-Y');
             $company_details = (object) $companyDetails;
             
             $ue_questionnaire = json_decode($job->Questionnaire);

             if($pdfType == 'Jemena') {
               //  
                 $ed_view = view('admin.customer.pdf-jemna', compact('customer','job','installer','company_details','date','ue_questionnaire'))->render();
             }
             else if($pdfType == 'SP Ausnet') {
                 $ed_view =  view('admin.customer.pdf-sp-ausnet', compact('customer','job','installer','company_details','date','ue_questionnaire'))->render();
             }
             else if($pdfType == 'Citipower') {
                 $ed_view =  view('admin.customer.pdf-citipower-powercor', compact('customer','company_details','stc','job','installer','form_no','date' ,'package','products'))->render();

             }else if($pdfType == 'Powercor') {
                $ed_view =  view('admin.customer.pdf-citipower-powercor', compact('customer','company_details','stc','job','installer','form_no','date' ,'package','products'))->render();
            }
             else if($pdfType == 'United Energy'){
                 //United Energy
                 $ed_view =  view('admin.customer.pdf-UE-PR', compact('customer','job','installer','company_details','date','ue_questionnaire'))->render();
                 
                 
             }else{
                 $ed_view = "" ;
             }
             
             
            if($ed_view != "" ){
             
             $ed_pdf = new \mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
             $ed_pdf->SetDisplayMode('fullpage');
             $ed_pdf->list_indent_first_level = 0;
             $ed_pdf->WriteHTML($ed_view);

             $file_name = $form_no.'.pdf';
             $path = public_path()."/documents/".$job->customer_id;
             \File::makeDirectory($path, $mode = 0777, true, true);
             $ed_file = public_path()."/documents/".$job->customer_id."/".$file_name;
             $ed_pdf->output($ed_file);
             

             $folder = new Folders;
             $folder->customer_id = $job->customer_id;
             $folder->name = $file_name;
             $folder->path = $file_name;
             $folder->save();
            }
             
         }
        
        Session::flash('flash_message', 'Job updated!');

        return redirect('admin/job');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Job::destroy($id);

        $message = "Job Deleted Successfully !!";

        return response()->json(['messages' => $message],200);
    }


            /**
    * Create STC record in BridgeSelect
    */
    public function createBridgeSelectSTC($job , $customer, $installer) {
        $data = $this->getData($job, $customer, $installer);
        //return $data;
        $checksum = $this->getChecksum($data);
        //return $checksum ;
        $request = $this->generateRequest($data, $checksum);
        //return $request;
        $Url = env("BRIDGEURL");
        //return $Url;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $Url ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($request),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));
        $output = curl_exec($curl);
        //return $output;
        curl_close($curl); 
        $result = json_decode($output);

        return $result;
    }

    /*
    This method:
    1. Retrieves requisite information for the job from your database / systems
    2. Removes all line breaks (\n), carriage returns (\r), tab spaces (\t) and leading blank spaces ( ) from the data
    3. Respond with a base64 encoded version of the JSON
    "fs": "partial",
    */
    function getData($job, $customer,$installer) {
        $party_data = json_decode($customer->party_data);
        $street_concat = $customer->street_number.' '.$customer->street_name;
        $street_type = $this->streetType($street_concat);

        $longitude = '';
        $latitude = '';

        if( isset($customer->party_data['longitude'] ) ){
           $longitude = $party_data->longitude;
        }

        if( isset( $customer->party_data['longitude'] ) ){
           $latitude = $party_data->latitude;
        }

        $data = '{
            "crmid": "'.$job->id.'",
            "at": "Physical",
            "e": "'.$customer->email.'",
            "fn": "'.$customer->first_name.'",
            "id": "'.$job->installation_date.'",
            "ie": "'.$customer->email.'",
            "ifn": "'.$customer->first_name.'",
            "ilat": "'.$latitude.'",
            "iln": "'.$customer->last_name.'",
            "ilng": "'.$longitude.'",
            "im": "'.$customer->phone_mobile.'",
            "ipa": "'.$street_concat.'" ,
            "ipc": "'.$customer->post_code.'",
            "iph": "'.$customer->phone_home.'",
            "ipra": "'.$customer->street_name.'",
            "unt" : "'.$customer->unit.'",
            "iuntn" : "'.$customer->unit.'",
            "isb": "'.$customer->suburb.'",
            "ist": "'.$customer->state.'",
            "istn": "'.$customer->street_number.'",
            "istp": "'.$street_type.'" ,
            "lat": "'.$latitude.'",
            "ln": "'.$customer->last_name.'",
            "lng": "'.$longitude.'",
            "m": "'.$customer->phone_mobile.'",
            "p": "'.$customer->phone_home.'",
            "pa": "'.$street_concat.'",
            "pc": "'.$customer->post_code.'",
            "pra": "'.$customer->street_name.'",
            "pt": "'.$customer->property_type.'",
            "sb": "'.$customer->suburb.'",
            "st": "'.$customer->state.'",
            "stn": "'.$customer->street_number.'",
            "stp": "'.$street_type.'",
            "installer": "'.$installer->accredition_number.'"
          }';

          
        $data = trim($data);
        //return $data;
        $data = preg_replace('@[\s]{2,}@', '', $data);
        return base64_encode($data);
    }

    function streetType($street_name) {
        $a = 'How are you?';
        $search = 'St';
        if(preg_match("/{$search}/i", $street_name)) { return "Street";  }
        $search = 'Street';
        if(preg_match("/{$search}/i", $street_name)) { return "Street"; }
        $search = 'Ave';
        if(preg_match("/{$search}/i", $street_name)) { return "Avenue"; }
        $search = 'Avenue';
        if(preg_match("/{$search}/i", $street_name)) { return "Avenue"; }
        $search = 'Alley'; 
        if(preg_match("/{$search}/i", $street_name)) { return "Alley"; }
        $search = 'Ally';
        if(preg_match("/{$search}/i", $street_name)) { return "Alley"; }
        $search = 'Arc';
        if(preg_match("/{$search}/i", $street_name)) { return "Arcade"; }
        $search = 'Arcade';
        if(preg_match("/{$search}/i", $street_name)) { return "Arcade"; }
        $search = 'Bvd';
        if(preg_match("/{$search}/i", $street_name)) { return "Boulevard"; }
        $search = 'Boulevard';
        if(preg_match("/{$search}/i", $street_name)) { return "Boulevard"; }
        $search = 'Bypa';
        if(preg_match("/{$search}/i", $street_name)) { return "Bypass"; }
        $search = 'Bypass';
        if(preg_match("/{$search}/i", $street_name)) { return "Bypass"; }
        $search = 'Cct';
        if(preg_match("/{$search}/i", $street_name)) { return "Circuit"; }
        $search = 'Circuit';
        if(preg_match("/{$search}/i", $street_name)) { return "Circuit"; }
        $search = 'Cl';
        if(preg_match("/{$search}/i", $street_name)) { return "Close"; }
        $search = 'Close';
        if(preg_match("/{$search}/i", $street_name)) { return "Close"; }
        $search = 'Crn';
        if(preg_match("/{$search}/i", $street_name)) { return "Corner"; }
        $search = 'Corner';
        if(preg_match("/{$search}/i", $street_name)) { return "Corner"; }
        $search = 'Ct';
        if(preg_match("/{$search}/i", $street_name)) { return "Court"; }
        $search = 'Court';
        if(preg_match("/{$search}/i", $street_name)) { return "Court"; }
        $search = 'Cres';
        if(preg_match("/{$search}/i", $street_name)) { return "Crescent"; }
        $search = 'Crescent';
        if(preg_match("/{$search}/i", $street_name)) { return "Crescent"; }
        $search = 'Cds';
        if(preg_match("/{$search}/i", $street_name)) { return "Cul-de-sac"; }
        $search = 'Cul-de-sac';
        if(preg_match("/{$search}/i", $street_name)) { return "Cul-de-sac"; }
        $search = 'Dr';
        if(preg_match("/{$search}/i", $street_name)) { return "Drive"; }
        $search = 'Drive';
        if(preg_match("/{$search}/i", $street_name)) { return "Drive"; }
        $search = 'Esp';
        if(preg_match("/{$search}/i", $street_name)) { return "Esplanade"; }
        $search = 'Esplanade';
        if(preg_match("/{$search}/i", $street_name)) { return "Esplanade"; }
        $search = 'Grn';
        if(preg_match("/{$search}/i", $street_name)) { return "Green"; }
        $search = 'Green';
        if(preg_match("/{$search}/i", $street_name)) { return "Green"; }
        $search = 'Gr';
        if(preg_match("/{$search}/i", $street_name)) { return "Grove"; }
        $search = 'Grove';
        if(preg_match("/{$search}/i", $street_name)) { return "Grove"; }
        $search = 'Hwy';
        if(preg_match("/{$search}/i", $street_name)) { return "Highway"; }
        $search = 'Highway';
        if(preg_match("/{$search}/i", $street_name)) { return "Highway"; }
        $search = 'Jnc';
        if(preg_match("/{$search}/i", $street_name)) { return "Junction"; }
        $search = 'Junction';
        if(preg_match("/{$search}/i", $street_name)) { return "Junction"; }
        $search = 'Lane';
        if(preg_match("/{$search}/i", $street_name)) { return "Lane"; }
        $search = 'Link';
        if(preg_match("/{$search}/i", $street_name)) { return "Link"; }
        $search = 'Mews';
        if(preg_match("/{$search}/i", $street_name)) { return "Mews"; }
        $search = 'Pde';
        if(preg_match("/{$search}/i", $street_name)) { return "Parade"; }
        $search = 'Parade';
        if(preg_match("/{$search}/i", $street_name)) { return "Parade"; }
        $search = 'Pl';
        if(preg_match("/{$search}/i", $street_name)) { return "Place"; }
        $search = 'Place';
        if(preg_match("/{$search}/i", $street_name)) { return "Place"; }
        $search = 'Rdge';
        if(preg_match("/{$search}/i", $street_name)) { return "Ridge"; }
        $search = 'Ridge';
        if(preg_match("/{$search}/i", $street_name)) { return "Ridge"; }
        $search = 'Rd';
        if(preg_match("/{$search}/i", $street_name)) { return "Road"; }
        $search = 'Road';
        if(preg_match("/{$search}/i", $street_name)) { return "Road"; }
        $search = 'Sq';
        if(preg_match("/{$search}/i", $street_name)) { return "Square"; }
        $search = 'Square';
        if(preg_match("/{$search}/i", $street_name)) { return "Square"; }
        $search = 'Tce';
        if(preg_match("/{$search}/i", $street_name)) { return "Terrace"; }
        $search = 'Terrace';
        if(preg_match("/{$search}/i", $street_name)) { return "Terrace"; }

        return "Unknown";
    }

    function getSalt() {
        $salt = env('BRIDGESLT');//replace with SALT provided
        return $salt;
    }

    /*
    This method:
    1. Appends the SALT to the data 
    2. Generates SHA256 hash for the appended data
    */
    function getChecksum($data) {
        $salt = $this->getSalt();
        //return '793f8ca9d4ce359145bb3c507302d5b8e64920d3a1b095dd157b421a3f645b9f';
        return hash('sha256', $data.$salt);
    }

    /*
    This method generates the request to be sent to Bridge Select Connector API
    */
    function generateRequest($data, $checksum) {
        $data1 = [
            'data' => $data,
            'csum' => $checksum,
        ];

        return  $data1;
    }

    /*
        This is just a helper method to check the generated checksum. 
        You will not need this for sending requests, but you will need it 
        when Bridge Select responses are to handled by your system
    */
    function checkChecksum($request) {
        $json = json_decode($request);
        $data = $json ->{'data'};
        $csum = $json->{'csum'};
        $checksum = getChecksum($data);
        return $csum == $checksum ? "Yippee :)": "Damn :(";
    }   

    public function regenerate_forms($id) {
        $this->regen_forms($id);
        Session::flash('flash_message', 'Forms have been regenerated');
        return redirect('/admin/customer/addfolder/'.$customer->id);
    }

    public function regen_forms($id) {
        $job = Job::with('electrical','designer')->find($id);
        $customer = Party::find($job->customer_id);

        $company_details =  array();
                
        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;

        $date = date('d-m-Y');

        $installer = User::find($job->installer_id);

        $ue_questionnaire = json_decode($job->Questionnaire);

        $this->makeEWR($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
        if($customer->electrical_distributer == 'Jemena') {
            $this->makeJemena($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
        }
        else if($customer->electrical_distributer == 'SP Ausnet') {
            $this->makeAusnet($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
        }
        else if($customer->electrical_distributer == 'United Energy'){
            $this->makeUnitedEnergy($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
        }
        else if($customer->electrical_distributer == 'Citipower' || $customer->electrical_distributer == 'Powercor') {
            $this->makePowerCorCitiPower($customer, $job, $installer, $company_details, $date);

        }

    }

    public function email_to_retailer($id){

        $job = Job::find($id);
        $job->email_to_retailer = 'Yes';
        $job->save();

        $customer = Party::find($job->customer_id);

        if($job->status == "Grid forms to be sent" &&  $job->email_to_retailer = "Yes" ){
            if (empty($job->pvd)) {
                $job->status = "STC registration TBC";
            }
            else {
                $job->status = "Completed";
            }
            $job->save();
            //get company details
            $company_details =  array();
            
            $setting  = Setting::get();
            foreach($setting as $key => $value) {
                $company_details[$value['name']] = $value['value'];   
            }
            $company_details = (object) $company_details;
           
            //Send mail when job is completed
            $sender_name = Setting::give_value_of('Sender_Name');
            $sender_email = Setting::give_value_of('Sender_Email');
            $sender_cc = Setting::give_value_of('Sender_CC');
            $cc_array = [];
            $cc_array = explode(',', $sender_cc);
            $sender_bcc = Setting::give_value_of('Sender_BCC');
            
            $bcc_array = [];
            $bcc_array = explode(',', $sender_bcc);

            //get ewr form of the job to attach in mail 
            $ewr_file_name = "EWR-".$job->id.".pdf" ;
            $ewr = Folders::where('path','=', $ewr_file_name )->first();

             //get electrical distributors  form of the customer to attach in mail 
            $ed_file_name = $customer->electrical_distributer.'-'.$job->id.".pdf" ;
            $ed = "" ;
            $ed = Folders::where('path','=', $ed_file_name )->first();   

            $ld_file_name = 'linediagram-'.$job->id.'.jpg';
            $ld = "";
            $ld = Folders::where('path','=', $ld_file_name )->first();   

            $CES_file_name = 'CES-'.$job->id.'.pdf';
            $CES = "";
            $CES = Folders::where('name','=', $CES_file_name )->first();  

            $SOP_file_name = 'SOP-'.$job->id.'.pdf';
            $SOP = "";
            $SOP = Folders::where('name','=', $SOP_file_name )->first();  

            $Preapproval_file_name = 'Preapproval-'.$job->id.'.pdf';
            $Preapproval = "";
            $Preapproval = Folders::where('name','=', $Preapproval_file_name )->first();  

            //get single line diagram from the Package which is selected for this job
            $pk_image = Package::find($job->package_id);
            
            //&& $pk_image
            if($ewr ){
            // Send mail to Provider or Retailer for Grid Form - EWR Form PDF
                $provider = Providers::find($customer->provider_id);
                if($provider ){
                    if($provider->email && $sender_email && $sender_name){

                        \Mail::send('email.order.gridform', compact('customer','provider'), function ($message) use ($customer,$provider,$sender_email,$sender_name, $cc_array,$bcc_array,$ewr, $ed, $pk_image,$ld,$CES,$SOP,$Preapproval) {

                            $subject = 'Grid Forms -'.$customer->street_number.' '.$customer->street_name.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code.' ,   '.$customer->nmi; 

                            $message->from($sender_email, $sender_name)
                                ->to($provider->email)
                                ->cc($cc_array)
                                ->bcc($bcc_array)
                                ->subject($subject);

                            $message->attach(public_path()."/documents/".$customer->id."/".$ewr->path);  
                            if($ed != ""){
                                $message->attach(public_path()."/documents/".$customer->id."/".$ed->path);
                            }
                            if($ld->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$ld->path);
                            }

                            if (isset($Preapproval)) {
                              if($Preapproval->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$Preapproval->path);
                              }   
                            }

                            if (isset($CES)) {
                              if($CES->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$CES->path);
                              }   
                            }

                            if (isset($SOP)) {
                              if($SOP->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$SOP->path);
                              }   
                            }
                            
                            if ($pk_image) {
                                if($pk_image->package_image){
                                    $message->attachData(public_path()."/package/".$pk_image->id."/".$pk_image->package_image,"Single Line Diagram.pdf"); 
                                }
                            }
                        });
                    }
                }

                if($customer ){
                    if($customer->email && $sender_email && $sender_name){

                        $sales = User::find($customer->sales_id);
                        if( $sales && $sales->email != ""){
                           // $cc_array[] =  $sales->email ; 
                             $cc_array = explode(',',  $sales->email);
                            $sender_email = $sales->email;
                            $sender_name = $sales->name;
                        }

                        \Mail::send('email.order.gridform', compact('customer','provider'), function ($message) use ($customer,$provider,$sender_email,$sender_name, $cc_array,$bcc_array,$ewr, $ed, $pk_image,$ld,$CES,$SOP,$Preapproval ) {

                            $subject = 'Grid Forms -'.$customer->street_number.' '.$customer->street_name.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code.' ,   '.$customer->nmi; 

                            $message->from($sender_email, $sender_name)
                                ->to($customer->email)
                                ->cc($cc_array)
                                ->bcc($bcc_array)
                                ->subject($subject);

                            $message->attach(public_path()."/documents/".$customer->id."/".$ewr->path);  
                            if($ed != ""){
                                $message->attach(public_path()."/documents/".$customer->id."/".$ed->path);
                            }
                            if($ld->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$ld->path);
                            }

                            if (isset($Preapproval)) {
                              if($Preapproval->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$Preapproval->path);
                              }   
                            }

                            if (isset($CES)) {
                              if($CES->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$CES->path);
                              }   
                            }

                            if (isset($SOP)) {
                              if($SOP->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$SOP->path);
                              }   
                            }
                            
                            if ($pk_image) {
                                if($pk_image->package_image){
                                    $message->attachData(public_path()."/package/".$pk_image->id."/".$pk_image->package_image,"Single Line Diagram.pdf"); 
                                }
                            }
                          
                        });
                        
                    }
                }
            }
        }
        Session::flash('flash_message', 'Email Send To Retailer');
        return redirect()->back();
    }

    public function preview_email_to_retailer($id){

        $this->regen_forms($id);
        $job = Job::find($id);
        //$job->email_to_retailer = 'Yes';
        $customer = Party::find($job->customer_id);

        if($job->status == "Grid forms to be sent" ){

            //get company details
            $company_details =  array();
            
            $setting  = Setting::get();
            foreach($setting as $key => $value) {
                $company_details[$value['name']] = $value['value'];   
            }
            $company_details = (object) $company_details;
           
            //Send mail when job is completed
            $sender_name = Setting::give_value_of('Sender_Name');
            $sender_email = Setting::give_value_of('Sender_Email');
            $sender_cc = Setting::give_value_of('Sender_CC');
            $cc_array = [];
            $cc_array = explode(',', $sender_cc);
            $sender_bcc = Setting::give_value_of('Sender_BCC');
            $bcc_array = [];
            $bcc_array = explode(',', $sender_bcc);

            //get ewr form of the job to attach in mail 
            $ewr_file_name = "EWR-".$job->id.".pdf" ;
            $ewr = Folders::where('path','=', $ewr_file_name )->first();

             //get electrical distributors  form of the customer to attach in mail 
            $ed_file_name = $customer->electrical_distributer.'-'.$job->id.".pdf" ;
            $ed = "" ;
            $ed = Folders::where('path','=', $ed_file_name )->first();   

            $ld_file_name = 'linediagram-'.$job->id.'.jpg';
            $ld = "";
            $ld = Folders::where('path','=', $ld_file_name )->first();   

            $CES_file_name = 'CES-'.$job->id.'.pdf';
            $CES = "";
            $CES = Folders::where('name','=', $CES_file_name )->first();  

            $SOP_file_name = 'SOP-'.$job->id.'.pdf';
            $SOP = "";
            $SOP = Folders::where('name','=', $SOP_file_name )->first();  

            $Preapproval_file_name = 'Preapproval-'.$job->id.'.pdf';
            $Preapproval = "";
            $Preapproval = Folders::where('name','=', $Preapproval_file_name )->first();  


            //get single line diagram from the Package which is selected for this job
            $pk_image = Package::find($job->package_id);
            
            $authEmail = \Auth::user()->email;
            //if($ewr && $pk_image){
            if($ewr){
            // Send mail to Provider or Retailer for Grid Form - EWR Form PDF
                $provider = Providers::find($customer->provider_id);
                if($provider ){
                    if($provider->email && $sender_email && $sender_name){

                        \Mail::send('email.order.gridform', compact('customer','provider'), function ($message) use ($customer,$provider,$sender_email,$sender_name, $cc_array,$bcc_array,$ewr, $ed, $pk_image,$ld,$CES,$SOP,$Preapproval) {

                            $subject = 'Grid Forms -'.$customer->street_number.' '.$customer->street_name.' '.$customer->suburb.' '.$customer->state.' '.$customer->post_code.' ,   '.$customer->nmi; 

                            $message->from($sender_email, $sender_name)
                                ->to(\Auth::user()->email)
                                //->cc($cc_array)
                                //->bcc($bcc_array)
                                ->subject($subject);
                            $message->attach(public_path()."/documents/".$customer->id."/".$ewr->path);  
                            if($ed != ""){
                                $message->attach(public_path()."/documents/".$customer->id."/".$ed->path);
                            }
                            if($ld->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$ld->path);
                            }

                            if (isset($Preapproval)) {
                              if($Preapproval->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$Preapproval->path);
                              }   
                            }

                            if (isset($CES)) {
                              if($CES->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$CES->path);
                              }   
                            }

                            if (isset($SOP)) {
                              if($SOP->path != "") {
                                $message->attach(public_path()."/documents/".$customer->id."/".$SOP->path);
                              }   
                            }
                            
                            if ($pk_image) {
                                if($pk_image->package_image){
                                $message->attachData(public_path()."/package/".$pk_image->id."/".$pk_image->package_image,"Single Line Diagram.pdf"); 
                            }
                            }
                            
                        });
                    }
                }
            }
        }
        //echo($authEmail);
        Session::flash('flash_message', 'Email sent to '.$authEmail);
        return back();
    }

    public function change_date(Request $request, $id){
        
        $date = $request->date ;  
        $job = Job::find($id);
        $requires_pre_approval = false;
        $customer = Party::find($job->customer_id);

         $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                     ->where('offer_id',$job->offer_id)
                                     ->where('package_id',$job->package_id)->first();

           
        $products = json_decode($customer_offer->product_obj);

        $package = json_decode($customer_offer->package_obj);

        $package_product = PackageProduct::where('package_id',$package->id)->get();
                        
        $product_obj = [];
            
        $inverter_total_watt = 0;
            
        foreach($products as $product){
            if($product->category->name == "Solar Inverter"){
                $quantity = 0;
                foreach ($package_product as $pk_pr){
                    if($product->id ==  $pk_pr->product_id) {
                        $quantity = $pk_pr->quantity;
                    }
                } 
                $inverter_total_watt = $product->rated_power_output *   $quantity;
            } 
        }

        if($customer->electrical_distributer == 'Jemena' || $customer->electrical_distributer == 'United Energy') {
            if($customer->electrical_distributer == 'Jemena') {
                $maxcap = 10000;
            }
            if($customer->electrical_distributer == 'United Energy') {
                $maxcap = 30000;
            }
            if ($inverter_total_watt > $maxcap) {
               $requires_pre_approval = true;
            }
        }
        else {
            $requires_pre_approval = true;    
        }
        

        if ($job->pre_approval_number != "" || $requires_pre_approval == false) {
            $job->installation_date = $date;
            $job->status = "Await installation";
            $job->save();

            
            if($customer->email){

                $sender_name = Setting::give_value_of('Sender_Name');
                $sender_email = Setting::give_value_of('Sender_Email');
                $sender_cc = Setting::give_value_of('Sender_CC');
                $cc_array = [];
                $cc_array = explode(',', $sender_cc);
                $sender_bcc = Setting::give_value_of('Sender_BCC');
                $bcc_array = [];
                $bcc_array = explode(',', $sender_bcc);

                $sales = User::find($customer->sales_id);
                if( $sales && $sales->email != ""){
                   // $cc_array[] =  $sales->email ; 
                     $cc_array = explode(',',  $sales->email);
                    $sender_email = $sales->email;
                    $sender_name = $sales->name;
                }

                
                if($customer->email && $sender_email && $sender_name){
                    \Mail::send('email.order.installationDateMail', compact('customer','date'), function ($message) use ($customer,$date,$sender_email,$sender_name, $cc_array,$bcc_array) {
                        $message->from($sender_email, $sender_name)
                                ->to($customer->email)
                                ->cc($cc_array)
                                ->bcc($bcc_array)
                                ->subject('Your Installation Date!');                   
                    });
                }
                

            }
            $message = "Installation date Set Successfully";
            return response()->json(['messages' => $message],200);
        }
        else {
            $message = "Cannot set installation date without pre-approval number!!";

            return response()->json(['messages' => $message],404);
        }

        

    }

    public function pre_approval_number(Request $request, $id){

        $job = Job::find($id);
        if($job){
            $job->pre_approval_number = $request->pre_approval_number;
            $job->save();
            $message = "Pre Approval Number Set Successfully";
        }else{
            $message = "Job Not Found";
        }
    
        return response()->json(['message' => $message],200);
    }
    
    public function questions($id){
        
        $job = Job::find($id);
        
        if(!$job){
            return redirect()->back();
        }

        $customer = Party::find($job->customer_id);

        $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                    ->where('offer_id',$job->offer_id)
                                    ->where('package_id',$job->package_id)->first();

        $installer = User::find($job->installer_id);

        $package = json_decode($customer_offer->package_obj);

        $products = json_decode($customer_offer->product_obj);

        $questionnaire_data = json_decode($job->Questionnaire);
        
        $package_product = PackageProduct::where('package_id',$package->id)->get();
                    
        $product_obj = [];
       
        
        foreach($products as $product){
            if($product->category->name == "Solar Inverter"){
                $quantity = 0;
                foreach ($package_product as $pk_pr){
                    if($product->id ==  $pk_pr->product_id) {
                        $quantity = $pk_pr->quantity;
                    }
                } 
                $inverter[] = (object) [
                    'manufacturer' => $product->brand->name,
                    'model' => $product->model,
                    'rated_power_output' => $product->rated_power_output,
                    'as4777_compliant' => $product->as4777_compliant,
                    'cac_number' => $product->certifying_authority_certificate_number,
                    'quantity' =>   $quantity,
                    'rating_inverter' =>  ($product->rated_power_output *   $quantity) / 1000              
                ];
                
            } 
        }

        if (empty($inverter)) {
            $inverter[] = (object) [
                    'manufacturer' => '',
                    'model' => '',
                    'rated_power_output' => 0,
                    'as4777_compliant' => '',
                    'cac_number' => '',
                    'quantity' =>   0,
                    'rating_inverter' =>  0              
                ];
        }

        $inverters = json_encode($inverter);
        $providers = Providers::all()->pluck('name','id')->prepend('Select Provider','');
        if (isset($customer->electrical_distributer)) {
            if($customer->electrical_distributer == 'Jemena') {
             return view('admin.job.questions', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
        }
        else if($customer->electrical_distributer == 'SP Ausnet') {
             return view('admin.job.questions', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
        }
        else if($customer->electrical_distributer == 'Citipower') {
             return view('admin.job.questions', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
        }
        else if($customer->electrical_distributer == 'Powercor') {
            return view('admin.job.questions', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
        }
        else if($customer->electrical_distributer == 'United Energy'){
             //United Energy
            return view('admin.job.questions', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
         }else{
             return view('admin.job.questions', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
         }
        }else{
             return view('admin.job.questions', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
         }
        
//return view('admin.job.questions', compact('job','customer','providers'));
        
         
    }

     public function questionspartial($id,$distributor){
        
       // return 'hello';
        

        $job = Job::find($id);
        
        if(!$job){
            return redirect()->back();
        }

        $customer = Party::find($job->customer_id);

        $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                    ->where('offer_id',$job->offer_id)
                                    ->where('package_id',$job->package_id)->first();

        $installer = User::find($job->installer_id);

        $package = json_decode($customer_offer->package_obj);

        $products = json_decode($customer_offer->product_obj);
        
        $package_product = PackageProduct::where('package_id',$package->id)->get();

        $questionnaire_data = json_decode($job->Questionnaire);

        //todo
                    
        $product_obj = [];
       
        
        foreach($products as $product){
            if($product->category->name == "Solar Inverter"){
                $quantity = 0;
                foreach ($package_product as $pk_pr){
                    if($product->id ==  $pk_pr->product_id) {
                        $quantity = $pk_pr->quantity;
                    }
                } 
                $inverter[] = (object) [
                    'manufacturer' => $product->brand->name,
                    'model' => $product->model,
                    'rated_power_output' => $product->rated_power_output,
                    'as4777_compliant' => $product->as4777_compliant,
                    'cac_number' => $product->certifying_authority_certificate_number,
                    'quantity' =>   $quantity,
                    'rating_inverter' =>  ($product->rated_power_output *   $quantity) / 1000              
                ];
                
            } 
        }
        $inverters = json_encode($inverter);
        $providers = Providers::all()->pluck('name','id')->prepend('Select Provider','');

        if($distributor == 'Jemena') {
             return view('admin.job.questions-jemena', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
        }
        else if($distributor == 'SP Ausnet') {
            $view=view('admin.job.questions-ausnet', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
                $view=$view->render();
             return $view; 
        }
        else if($distributor == 'United Energy'){
             //United Energy
            return view('admin.job.questions-unitedenergy', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
         }else{
             return view('admin.job.questions-default', compact('job','customer','providers','package','products','inverter','installer','questionnaire_data'));
         }
        
         
    }


    public function questionsUpdateUE($id, Request $request){

            
 
        $job = Job::with('electrical','designer')->find($id);

        $customer = Party::find($job->customer_id);

        $requestData = $request->all();

        if($request->has('meter_phase') && $request->meter_phase != null ){ 
            $requestData['meter_phase'] = json_encode($request->meter_phase);
        }
        

        $ue_questionnaire = (object) [
            'ces_number' => $request->ces_number,
            'project_number' => $request->project_number,
            'connection_type' => $request->connection_type,
            'connection_type_1' => $request->connection_type_1,
            'manual_to_customer' => $request->manual_to_customer,
            'instructed_customer' => $request->instructed_customer,
            'solar_pv_net' => $request->solar_pv_net,
            'solar_total_cap' => $request->solar_total_cap,
            'wind_pv_net' => $request->wind_pv_net,
            'wind_total_cap' => $request->wind_total_cap,
            'Other_pv_net' => $request->Other_pv_net,
            'Other_total_cap' => $request->Other_total_cap,
            'energy_storage_pv_net' => $request->energy_storage_pv_net,
            'energy_storage_total_cap' => $request->energy_storage_total_cap,
            'red_power_rating' => $request->red_power_rating,
            'red_energy_storage_capacity' => $request->red_energy_storage_capacity,
            'white_power_rating' => $request->white_power_rating,
            'white_energy_storage_capacity' => $request->white_energy_storage_capacity,
            'blue_power_rating' => $request->blue_power_rating,
            'blue_energy_storage_capacity' => $request->blue_energy_storage_capacity,
            'inverter_manufacturer_1' => $request->inverter_manufacturer_1,
            'inverter_model_1' => $request->inverter_model_1,
            'inverter_number_1' => $request->inverter_number_1,
            'inverter_rating_1' => $request->inverter_rating_1,
            'AS4777_complaint_1' => $request->AS4777_complaint_1,
            'inverter_status_1' => $request->inverter_status_1,
            'inverter_energy_source_1' => $request->inverter_energy_source_1,
            'CAC_Number_1' => $request->CAC_Number_1,
            'additional_inverter_1' => $request->additional_inverter_1,
            'inverter_manufacturer_2' => $request->inverter_manufacturer_2,
            'inverter_model_2' => $request->inverter_model_2,
            'inverter_rating_2' => $request->inverter_rating_2,
            'inverter_number_2' => $request->inverter_number_2,
            'AS4777_complaint_2' => $request->AS4777_complaint_2,
            'inverter_status_2' => $request->inverter_status_2,
            'inverter_energy_source_2' => $request->inverter_energy_source_2,
            'CAC_Number_2' => $request->CAC_Number_2,
            'additional_inverter_2' => $request->additional_inverter_2,
            'inverter_manufacturer_3' => $request->inverter_manufacturer_3,
            'inverter_model_3' => $request->inverter_model_3,
            'inverter_rating_3' => $request->inverter_rating_3,
            'inverter_number_3' => $request->inverter_number_3,
            'AS4777_complaint_3' => $request->AS4777_complaint_3,
            'inverter_status_3' => $request->inverter_status_3,
            'inverter_energy_source_3' => $request->inverter_energy_source_3,
            'CAC_Number_3' => $request->CAC_Number_3,
            'storage_installed_1' => $request->storage_installed_1,
            'storage_manufacture_1' => $request->storage_manufacture_1,
            'storage_model_1' => $request->storage_model_1,
            'storage_size_1' => $request->storage_size_1,
            'storage_status_1' => $request->storage_status_1,
            'CAC_Storage_Number_1' => $request->CAC_Storage_Number_1,
            'additional_storage_1' => $request->additional_storage_1,
            'storage_installed_2' => $request->storage_installed_2,
            'storage_manufacture_2' => $request->storage_manufacture_2,
            'storage_model_2' => $request->storage_model_2,
            'storage_size_2' => $request->storage_size_2,
            'storage_status_2' => $request->storage_status_2,
            'CAC_Storage_Number_2' => $request->CAC_Storage_Number_2,
            'additional_storage_2' => $request->additional_storage_2,
            'storage_installed_3' => $request->storage_installed_3,
            'storage_manufacture_3' => $request->storage_manufacture_3,
            'storage_model_3' => $request->storage_model_3,
            'storage_size_3' => $request->storage_size_3,
            'storage_status_3' => $request->storage_status_3,
            'CAC_Storage_Number_3' => $request->CAC_Storage_Number_3,
            'other_info' => $request->other_info,
            'company_name' => $request->company_name,
            'address' => $request->address,
            'Telephone' => $request->Telephone,
            'Email' => $request->Email,
            'REC_Name' => $request->REC_Name,
            'REC_Telephone' => $request->REC_Telephone,
            'REC_ContractorName' => $request->REC_ContractorName,
            'REC_Number' => $request->REC_Number,
            'REC_ResponsiblePerson' => $request->REC_ResponsiblePerson,
            'supply_address' => $request->supply_address,
            'owner_name' => $request->owner_name,
            'customer_rmi' => $request->customer_rmi,
            'tel_business' => $request->tel_business,
            'tel_home' => $request->tel_home,
            'mail_address' => $request->mail_address
        ];

        //var_dump($ue_questionnaire);

        $customer->update($requestData);

        $job->update($requestData);
        $job->Questionnaire = json_encode($ue_questionnaire);
        $job->completed_on = $request->completed_on;
        if ($job->sign == 0) {
            $job->status = "Await customer signature";
        }
       
        $job->save();
        //get company details
        $company_details =  array();
                
        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;

        $date = date('d-m-Y');

        $installer = User::find($job->installer_id);

        // Delete EWR Form if already exist
        $job_pdf = 'UE-'.$job->id.'.pdf' ;

        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$job_pdf)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                }
            }
            $find_job_pdf->delete();
        }

         //Create EWR Form & store in Customer Folder

         $this->makeEWR($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
         $this->makeUnitedEnergy($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
         

         Session::flash('flash_message', 'EWR Form Created Successfully!');

         if($job->status == "Await customer signature"){
            $this->job_sign_form_email($installer, $customer, $job);
         }

        return redirect('/admin/customer/addfolder/'.$customer->id);

    }

    public function job_sign_form_email($installer, $customer, $job) {
        //Send mail for Customer Approval
                $sender_name = Setting::give_value_of('Sender_Name');
                $sender_email = Setting::give_value_of('Sender_Email');
                $sender_cc = Setting::give_value_of('Sender_CC');
                if (isset($installer->email)) {
                   $sender_cc = $installer->email.','.$sender_cc; 
                }
                
                $cc_array = [];
                $cc_array = explode(',', $sender_cc);
                $sender_bcc = Setting::give_value_of('Sender_BCC');
                $bcc_array = [];
                $bcc_array = explode(',', $sender_bcc);

                $sales = User::find($customer->sales_id);
                if( $sales && $sales->email != ""){
                   // $cc_array[] =  $sales->email ; 
                    if (isset($installer->email)) {
                        $sender_cc = $sales->email.','.$sender_cc; 
                    }
                    $cc_array = explode(',',  $sender_cc);
                    $sender_email = $sales->email;
                    $sender_name = $sales->name;
                }
                
                if($customer ){
                    if($customer->email && $sender_email && $sender_name){

                        \Mail::send('email.order.job-sign-form', compact('customer','job'), function ($message) use ($customer,$sender_email,$sender_name, $cc_array,$bcc_array ) {

                             $message->from($sender_email, $sender_name)
                                ->to($customer->email)
                                ->cc($cc_array)
                                ->bcc($bcc_array)
                                ->subject("Customer Approval for Job Completion");
                        });
                        
                    }
                }
    }

    public function questionsUpdateJemena($id, Request $request){

            
 
        $job = Job::with('electrical','designer')->find($id);

        $customer = Party::find($job->customer_id);

        $requestData = $request->all();

        if($request->has('meter_phase') && $request->meter_phase != null ){ 
            $requestData['meter_phase'] = json_encode($request->meter_phase);
        }
        

        $ue_questionnaire = (object) [
            'ces_number' => $request->ces_number,
            'project_number' => $request->project_number,
            'company_name' => $request->company_name, 
            'address' => $request->address,
            'Telephone' => $request->Telephone,
            'Email' => $request->Email,
            'REC_Name' => $request->REC_Name,
            'applicant_supply_address' => $request->applicant_supply_address,
            'customer_name' => $request->customer_name,
            'customer_email' => $request->customer_email,
            'customer_homephone' => $request->customer_homephone,
            'customer_cell' => $request->customer_cell,
            'customer_address' => $request->input('customer_address'),
            'supply_address' => $request->supply_address,
            'customer_nmi' => $request->customer_nmi,
            'server_required_inverter' => $request->server_required_inverter,
            'service_required_electicity' => $request->service_required_electicity,
            'seperate_inverter_installed' => $request->seperate_inverter_installed,
            'solar_new' => $request->solar_new,
            'solar_retained' => $request->solar_retained,
            'solar_removed' => $request->solar_removed,
            'solar_total' => $request->solar_total,
            'wind_new' => $request->wind_new,
            'wind_retained' => $request->wind_retained,
            'wind_removed' => $request->wind_removed,
            'wind_total' => $request->wind_total,
            'other_new' => $request->other_new,
            'other_retained' => $request->other_retained,
            'other_removed' => $request->other_removed,
            'other_total' => $request->other_total,
            'energy_storage_new' => $request->energy_storage_new,
            'energy_storage_retained' => $request->energy_storage_retained,
            'energy_storage_removed' => $request->energy_storage_removed,
            'energy_storage_total' => $request->energy_storage_total,
            'red_power_rating' => $request->red_power_rating,
            'red_energy_storage_capacity' => $request->red_energy_storage_capacity,
            'white_power_rating' => $request->white_power_rating,
            'white_energy_storage_capacity' => $request->white_energy_storage_capacity,
            'blue_power_rating' => $request->blue_power_rating,
            'blue_energy_storage_capacity' => $request->blue_energy_storage_capacity,
            'inverter_manufacturer_1' => $request->inverter_manufacturer_1,
            'inverter_model_1' => $request->inverter_model_1,
            'inverter_rating_1' => $request->inverter_rating_1,
            'inverter_number_1' => $request->inverter_number_1,
            'inverter_status_1' => $request->inverter_status_1,
            'inverter_energy_source_1' => $request->inverter_energy_source_1,
            'CAC_Number_1' => $request->CAC_Number_1,
            'additional_inverter_1' => $request->additional_inverter_1,
            'inverter_manufacturer_2' => $request->inverter_manufacturer_2,
            'inverter_model_2' => $request->inverter_model_2,
            'inverter_rating_2' => $request->inverter_rating_2,
            'inverter_number_2' => $request->inverter_number_2,
            'inverter_status_2' => $request->inverter_status_2,
            'CAC_Number_2' => $request->CAC_Number_2,
            'additional_inverter_2' => $request->additional_inverter_2,
            'inverter_manufacturer_3' => $request->inverter_manufacturer_3,
            'inverter_model_3' => $request->inverter_model_3,
            'inverter_rating_3' => $request->inverter_rating_3,
            'inverter_number_3' => $request->inverter_number_3,
            'inverter_status_3' => $request->inverter_status_3,
            'CAC_Number_3' => $request->CAC_Number_3,
            'storage_installed_1' => $request->storage_installed_1,
            'storage_manufacture_1' => $request->storage_manufacture_1,
            'storage_model_1' => $request->storage_model_1,
            'storage_rating_1' => $request->storage_rating_1,
            'storage_status_1' => $request->storage_status_1,
            'storage_number_1' => $request->storage_number_1,
            'CAC_Storage_Number_1' => $request->CAC_Storage_Number_1,
            'additional_storage_1' => $request->additional_storage_1,
            'description_inverter_alteration' => $request->description_inverter_alteration,
            'site_access_special_instruction' => $request->site_access_special_instruction,
            'expedited_offer' => $request->expedited_offer,
            'acknowledge' => $request->acknowledge          
        ];

        //var_dump($ue_questionnaire);

        $customer->update($requestData);

        $job->update($requestData);
        $job->completed_on = $request->completed_on;
        $job->Questionnaire = json_encode($ue_questionnaire);

        if ($job->sign == 0) {
            $job->status = "Await customer signature";
        }
        $job->save();
        //get company details
        $company_details =  array();
                
        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;

        $date = date('d-m-Y');

        $installer = User::find($job->installer_id);

        // Delete EWR Form if already exist
        $job_pdf = 'jemena-'.$job->id.'.pdf' ;

        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$job_pdf)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                }
            }
            $find_job_pdf->delete();
        }

         //Create EWR Form & store in Customer Folder
//      
         $this->makeEWR($customer, $job, $installer, $company_details, $date, $ue_questionnaire);

         $this->makeJemena($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
         

         Session::flash('flash_message', 'EWR Form Created Successfully!');

        if($job->status == "Await customer signature"){
            $this->job_sign_form_email($installer, $customer, $job);
        }

        return redirect('/admin/customer/addfolder/'.$customer->id);

    }
    

    public function questionsUpdateAusnet($id, Request $request){

            
 
        $job = Job::with('electrical','designer')->find($id);

        $customer = Party::find($job->customer_id);

        $requestData = $request->all();

        if($request->has('meter_phase') && $request->meter_phase != null ){ 
            $requestData['meter_phase'] = json_encode($request->meter_phase);
        }
        
        

        $ue_questionnaire = (object) [
            'ces_number' => $request->ces_number,
            'project_number' => $request->project_number,
            'embedded_generation_type' => $request->embedded_generation_type,
            'no_of_phases_1' => $request->no_of_phases_1,
            'inverter_phase' => $request->inverter_phase,
            'include_battery_storage' => $request->include_battery_storage,
            'embedded_generator_installed' => $request->embedded_generator_installed,
            'existing_rating' => $request->existing_rating, 
            'existing_export_limit' => $request->existing_export_limit,
            'pre_approval_number' => $request->pre_approval_number,
            'total_capacity_installed' => $request->total_capacity_installed,
            'export_limit' => $request->export_limit,
            'phase_a' => $request->phase_a,
            'phase_b' => $request->phase_b,
            'phase_c' => $request->phase_c,
            'company_name' => $request->company_name,
            'address' => $request->address,
            'Telephone' => $request->Telephone,
            'Email' => $request->Email,
            'REC_Name' => $request->REC_Name,
            'compliance_sop_33_06' => $request->compliance_sop_33_06,
            'inverter_manufacturer' => $request->inverter_manufacturer,
            'inverter_model' => $request->inverter_model,
            'inverter_rating' => $request->inverter_rating,
            'inverter_total_watt' => $request->inverter_total_watt,
            'storage_installed' => $request->storage_installed,
            'storage_manufacture' => $request->storage_manufacture,
            'storage_model' => $request->storage_model,
            'storage_rating' => $request->storage_rating,
            'storage_capacity' => $request->storage_capacity,
            'storage_type' => $request->storage_type,
            'other_type' => $request->other_type,
            'manual_to_customer' => $request->manual_to_customer,
            'instructed_customer' => $request->instructed_customer,
            'clean_energy_approved' => $request->clean_energy_approved,
            'test_1_time' => $request->test_1_time,
            'test_1_result' => $request->test_1_result,
            'test_2_time' => $request->test_2_time,
            'test_2_result' => $request->test_2_result,
            'limited_export_commissioning' => $request->limited_export_commissioning,
            'SOP_Time' => $request->SOP_Time,
            'REC_Telephone' => $request->REC_Telephone,
            'REC_ContractorName' => $request->REC_ContractorName,
            'REC_Number' => $request->REC_Number,
            'REC_ResponsiblePerson' => $request->REC_ResponsiblePerson
            
        ];

      
        $customer->update($requestData);

        $job->update($requestData);
        $job->Questionnaire = json_encode($ue_questionnaire);
        $job->completed_on = $request->completed_on;
        if ($job->sign == 0) {
            $job->status = "Await customer signature";
        }
        $job->save();
        //get company details
        $company_details =  array();
                
        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;

        $date = date('d-m-Y');

        $installer = User::find($job->installer_id);

        // Delete EWR Form if already exist
        $job_pdf = 'ausnet-'.$job->id.'.pdf' ;

        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$job_pdf)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                }
            }
            $find_job_pdf->delete();
        }

         //Create EWR Form & store in Customer Folder
//      
         $this->makeEWR($customer, $job, $installer, $company_details, $date, $ue_questionnaire);

         $this->makeAusnet($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
        
         Session::flash('flash_message', 'EWR Form Created Successfully!');

        if($job->status == "Await customer signature"){
            $this->job_sign_form_email($installer, $customer, $job);
        }

        return redirect('/admin/customer/addfolder/'.$customer->id);

    }

    function create_line_diagrams($customer, $job, $company_details) {

         

         $equipment_schedule = "Equipment Schedule: ";
         $pv_inverter_details = "PV Inverter Details : ";
         $panel_details = "Panels Details: ";
         $installed_capacity = "Total installed capacity: ";
         $number_of_panels = "Total number of panels: ";
         $panel_rating = "Individual Panel Rating: ";
         $inverter_manufacturer = "Manufacturer: ";
         $inverter_model = "Model Number: ";
         $invert_count = "Number of Inverters: ";
         $inverter_total_watt = "Total inverter rating: ";

         $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                    ->where('offer_id',$job->offer_id)
                                    ->where('package_id',$job->package_id)->first();

         $package = json_decode($customer_offer->package_obj);
                
         $products = json_decode($customer_offer->product_obj);

         $package_product = PackageProduct::where('package_id',$package->id)->get();

         $inverter_count = 0;

         foreach($products as $product){
            if($product->category->name == "Solar Panel"){
                $quantity = 0;
                $rated_power_output = 0;
                foreach ($package_product as $pk_pr){
                    if($product->id ==  $pk_pr->product_id) {
                        $quantity = $pk_pr->quantity;   
                        $rated_power_output = $product->rated_power_output;
                    }
                } 

                $panel_details = $panel_details.$product->brand->name.' '.$product->model;
                $installed_capacity = $installed_capacity.' '.($job->total_wt/1000);
                $number_of_panels = $number_of_panels.' '.$job->no_of_panels_value;
                $panel_rating = $panel_rating.' '.$rated_power_output;
            } 
            elseif($product->category->name != "Solar Panel"){
                $quantity = 0;
                foreach ($package_product as $pk_pr){
                    if($product->id ==  $pk_pr->product_id) {
                        $quantity = $pk_pr->quantity;
                        $inverter_count = $inverter_count + $pk_pr->quantity;
                    }
                } 
                $inverter_manufacturer = $inverter_manufacturer.' '.$product->brand->name;
                $inverter_model = $inverter_model.' '.$product->model;
                $invert_count = $invert_count.' '.$quantity;
                $inverter_total_watt = $inverter_total_watt.' '.(($product->rated_power_output *   $quantity) / 1000);
            }
        }

        //$jpg_image = imagecreatefromjpeg(public_path().'/assets/images/1/GreenSky_SalesBrochure_1.jpg');
         if ($job->no_of_phases == "3") {
            if ($inverter_count > 1) {
                $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/LineDiagrams/Phase3Micro.jpg');
            }
            else {
                $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/LineDiagrams/Phase3String.jpg');
            }
            
         }
         elseif ($job->no_of_phases == "2") {
            if ($inverter_count > 1) {
                $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/LineDiagrams/Phase2Micro.jpg');
            }
            else {
                $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/LineDiagrams/Phase2String.jpg');
            }
            
         }
         else {
            if ($inverter_count > 1) {
                $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/LineDiagrams/Phase1Micro.jpg');   
            }
            else {
                $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/LineDiagrams/Phase1String.jpg');   
            }
            
         }


         

         $setting  = Setting::get();
         $company_address = "";
         $company_name = "";
         $company_contactnumber = "";
         $company_rec_number = "";
         foreach($setting as $key => $value) {
            if($value['name'] == 'Address')
            {
               $company_address = str_replace("<br/>", " ", $value['value']);
            }               
            elseif ($value['name'] == 'Company_Name') {
                $company_name =$value['value'];
            }   
            elseif ($value['name'] == 'REC_Number') {
                $company_rec_number =$value['value'];
            }  
            elseif ($value['name'] == 'Phone') {
                $company_contactnumber =$value['value'];
            }  
         }

         $installation_company = "Installation company: ".$company_name;
         $comapny_address = "Address: ".$company_address;
         $contact_number = "Contact Number: ".$company_contactnumber;
         $REC_Number = "Registered Electrical contractor: ".$company_rec_number;


         $black = imagecolorallocate($jpg_image, 0, 0, 0);
         $font_path = public_path().'/assets/fonts/Calibri.ttf';
         $text = $customer->first_name.' '.$customer->last_name;
         $textaddress1 = trim("".trim($customer->unit).' '.trim($customer->street_number).' '.trim($customer->street_name));
         $textaddress2 = "".$customer->suburb.' '.$customer->post_code.' '.$customer->state;

         
         $setting  = Setting::get();
                
         foreach($setting as $key => $value) {
            if($value['name'] == 'Address')
            {
               $company_address = explode("<br/>", $value['value']);
            }                  
         }

         $addressline1 = trim($company_address[0]);
         $addressline2 = trim($company_address[1]);
         $date = date('d F Y');
         $dateline = $date;
         imagettftext($jpg_image, 12, 0, 47, 72, $black, $font_path, $equipment_schedule);
         imagettftext($jpg_image, 12, 0, 47, 95, $black, $font_path, $panel_details);
         imagettftext($jpg_image, 12, 0, 47, 120, $black, $font_path, $installed_capacity);
         imagettftext($jpg_image, 12, 0, 47, 145, $black, $font_path, $number_of_panels);
         imagettftext($jpg_image, 12, 0, 47, 170, $black, $font_path, $panel_rating);
         imagettftext($jpg_image, 12, 0, 47, 195, $black, $font_path, $pv_inverter_details);
         imagettftext($jpg_image, 12, 0, 47, 220, $black, $font_path, $inverter_manufacturer);
         imagettftext($jpg_image, 12, 0, 47, 245, $black, $font_path, $inverter_model);
         imagettftext($jpg_image, 12, 0, 47, 270, $black, $font_path, $invert_count);
         imagettftext($jpg_image, 12, 0, 47, 295, $black, $font_path, $inverter_total_watt);
         

         imagettftext($jpg_image, 10, 0, 47, 725, $black, $font_path, $installation_company);
         imagettftext($jpg_image, 10, 0, 47, 745, $black, $font_path, $comapny_address);
         imagettftext($jpg_image, 10, 0, 47, 765, $black, $font_path, $contact_number);
         imagettftext($jpg_image, 10, 0, 47, 785, $black, $font_path, $REC_Number);
                 
         $linediagramPath = public_path()."/documents/".$customer->id;
         $imagename = 'linediagram-'.$job->id.'.jpg';
         if(!File::exists($linediagramPath)) {
            \File::makeDirectory($linediagramPath, $mode = 0777, true, true);
         }

         $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$imagename)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                    clearstatcache(); 
                }
            }
            $find_job_pdf->delete();
        }

         imagejpeg($jpg_image,$linediagramPath.'/'.$imagename);

         $cust_folder_ewr = new Folders;
         $cust_folder_ewr->customer_id = $customer->id;
         $cust_folder_ewr->name = $imagename;
         $cust_folder_ewr->path = $imagename;
         $cust_folder_ewr->save();

         imagedestroy($jpg_image);

    }

    public function makePowerCorCitiPower($customer, $job, $installer, $company_details, $date) {

        $form_no = $customer->electrical_distributer.'-'.$job->id ;

        $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                    ->where('offer_id',$job->offer_id)
                                    ->where('package_id',$job->package_id)->first();


        $package = json_decode($customer_offer->package_obj);

        $products = json_decode($customer_offer->product_obj);

        $view_ewr =  view('admin.customer.pdf-citipower-powercor', compact('customer','company_details','stc','job','installer','form_no','date' ,'package','products'))->render();

        $ewr_file_name = $customer->electrical_distributer.'-'.$job->id.".pdf" ;
         
        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$ewr_file_name)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                    clearstatcache(); 
                }
            }
            $find_job_pdf->delete();
        }

         $ewr_path = public_path()."/documents/".$customer->id;
         \File::makeDirectory($ewr_path, $mode = 0777, true, true);
         $ewr_file= public_path()."/documents/".$customer->id."/".$ewr_file_name;

         $this->save_pdf($view_ewr , $ewr_file);

         $cust_folder_ewr = new Folders;
         $cust_folder_ewr->customer_id = $customer->id;
         $cust_folder_ewr->name = $ewr_file_name;
         $cust_folder_ewr->path = $ewr_file_name;
         $cust_folder_ewr->save();
    }

    public function makeJemena($customer, $job, $installer, $company_details, $date,$ue_questionnaire) {
        $view_ewr = view('admin.customer.pdf-jemna', compact('customer','job','installer','company_details','date','ue_questionnaire'))->render();

        $ewr_file_name = $customer->electrical_distributer.'-'.$job->id.".pdf" ;
         
        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$ewr_file_name)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                    clearstatcache(); 
                }
            }
            $find_job_pdf->delete();
        }

         $ewr_path = public_path()."/documents/".$customer->id;
         \File::makeDirectory($ewr_path, $mode = 0777, true, true);
         $ewr_file= public_path()."/documents/".$customer->id."/".$ewr_file_name;

         $this->save_pdf($view_ewr , $ewr_file);

         $cust_folder_ewr = new Folders;
         $cust_folder_ewr->customer_id = $customer->id;
         $cust_folder_ewr->name = $ewr_file_name;
         $cust_folder_ewr->path = $ewr_file_name;
         $cust_folder_ewr->save();
    }

    public function makeUnitedEnergy($customer, $job, $installer, $company_details, $date,$ue_questionnaire) {
        return;
        $view_ewr = view('admin.customer.pdf-UE-PR', compact('customer','job','installer','company_details','date','ue_questionnaire'))->render();
        $ewr_file_name = $customer->electrical_distributer.'-'.$job->id.".pdf" ;
         
        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$ewr_file_name)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                    clearstatcache(); 
                }
            }
            $find_job_pdf->delete();
        }
         $ewr_path = public_path()."/documents/".$customer->id;
         \File::makeDirectory($ewr_path, $mode = 0777, true, true);
         $ewr_file= public_path()."/documents/".$customer->id."/".$ewr_file_name;

         $this->save_pdf($view_ewr , $ewr_file);

         $cust_folder_ewr = new Folders;
         $cust_folder_ewr->customer_id = $customer->id;
         $cust_folder_ewr->name = $ewr_file_name;
         $cust_folder_ewr->path = $ewr_file_name;
         $cust_folder_ewr->save();
    }

    public function makeAusnet($customer, $job, $installer, $company_details, $date,$ue_questionnaire) {
        $view_ewr = view('admin.customer.pdf-sp-ausnet', compact('customer','job','installer','company_details','date','ue_questionnaire'))->render();
         //return  $view_ewr;

        $ewr_file_name = $customer->electrical_distributer.'-'.$job->id.".pdf" ;
        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$ewr_file_name)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                    clearstatcache(); 
                }
            }
            $find_job_pdf->delete();
        }
         $ewr_path = public_path()."/documents/".$customer->id;
         \File::makeDirectory($ewr_path, $mode = 0777, true, true);
         $ewr_file= public_path()."/documents/".$customer->id."/".$ewr_file_name;

         $this->save_pdf($view_ewr , $ewr_file);

         $cust_folder_ewr = new Folders;
         $cust_folder_ewr->customer_id = $customer->id;
         $cust_folder_ewr->name = $ewr_file_name;
         $cust_folder_ewr->path = $ewr_file_name;
         $cust_folder_ewr->save();

    }


    public function makeEWR ($customer, $job, $installer, $company_details, $date , $ue_questionnaire) {

        
        
        $view_ewr = view('admin.customer.pdf-EWR', compact('customer','job','installer','company_details','date','ue_questionnaire'))->render();

        $ewr_file_name = 'EWR-'.$job->id.'.pdf';
        $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$ewr_file_name)->first();
        
        if($find_job_pdf){
            if($find_job_pdf->path != null){
                $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                if (File::exists($old_file)) {
                    unlink($old_file);
                    clearstatcache(); 
                }
            }
            $find_job_pdf->delete();
        }

        $ewr_path = public_path()."/documents/".$customer->id;
        \File::makeDirectory($ewr_path, $mode = 0777, true, true);
        $ewr_file= public_path()."/documents/".$customer->id."/".$ewr_file_name;

        $this->save_pdf($view_ewr , $ewr_file);
        $cust_folder_ewr = new Folders;
        $cust_folder_ewr->customer_id = $customer->id;
        $cust_folder_ewr->name = $ewr_file_name;
        $cust_folder_ewr->path = $ewr_file_name;
        $cust_folder_ewr->save();

        $this->create_line_diagrams($customer, $job,$company_details);
    }


    public function questionsUpdate($id, Request $request){


        $job = Job::with('electrical','designer')->find($id);

        $customer = Party::find($job->customer_id);

        $requestData = $request->all();

        if($request->has('meter_phase') && $request->meter_phase != null ){ 
            $requestData['meter_phase'] = json_encode($request->meter_phase);
        }

        if ($requestData['electrical_distributer'] == "Jemena") {
            $this->questionsUpdateJemena($id,$request);
        }
        else if ($requestData['electrical_distributer'] == "SP Ausnet") {
             $this->questionsUpdateAusnet($id,$request);
        }
        else if ($requestData['electrical_distributer'] == "United Energy") {
             $this->questionsUpdateUE($id,$request);
        }
        else {

            $ue_questionnaire = (object) [
                'ces_number' => $request->ces_number,
                'project_number' => $request->project_number,
            ];

            $customer->update($requestData);

            $job->update($requestData);
            $job->Questionnaire = json_encode($ue_questionnaire);
            $job->completed_on = $request->completed_on;
            if ($job->sign == 0) {
                $job->status = "Await customer signature";
            }
            $job->save();
            //get company details
            $company_details =  array();
                    
            $setting  = Setting::get();
            foreach($setting as $key => $value) {
                $company_details[$value['name']] = $value['value'];   
            }
            $company_details = (object) $company_details;

            $date = date('d-m-Y');

            $installer = User::find($job->installer_id);

            // Delete EWR Form if already exist
            $job_pdf = 'EWR-'.$job->id.'.pdf' ;

            $find_job_pdf = Folders::where('customer_id',$job->customer_id)->where('path',$job_pdf)->first();
            
            if($find_job_pdf){
                if($find_job_pdf->path != null){
                    $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                    if (File::exists($old_file)) {
                        unlink($old_file);
                    }
                }
                $find_job_pdf->delete();
            }

             //Create EWR Form & store in Customer Folder
    //
             $this->makeEWR($customer, $job, $installer, $company_details, $date, $ue_questionnaire);
             if($customer->electrical_distributer == 'Citipower' || $customer->electrical_distributer == 'Powercor') {
                $this->makePowerCorCitiPower($customer, $job, $installer, $company_details, $date);
             }

             Session::flash('flash_message', 'EWR Form Created Successfully!');

            if($job->status == "Await customer signature"){
               $this->job_sign_form_email($installer, $customer, $job);
            }
        }


        
        return redirect('/admin/customer/addfolder/'.$customer->id);
        
        

    }

}
