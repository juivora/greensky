<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\ProductImage;
use Maatwebsite\Excel\Facades\Excel;
use App\Setting;
use App\Brand;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.products');
        $this->middleware('permission:access.product.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.product.create')->only(['create', 'store']);
        $this->middleware('permission:access.product.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)    {
        // $keyword = $request->get('search');
        // $perPage = 25;

        // if (!empty($keyword)) {
        //     $products = Product::select('products.*','category.name as cat_name')
        //     ->leftJoin('category','products.category_id','=','category.id')
        //     ->where('products.name', 'LIKE', "%$keyword%")
        //     ->orWhere('category.name', 'LIKE', "%$keyword%")
        //     ->orWhere('price', 'LIKE', "%$keyword%")
        //     ->orWhere('sale_price', 'LIKE', "%$keyword%")
		// 	->orderBy('id', 'DESC')
        //     ->paginate($perPage);
        // } else {
        //     $products =  Product::select('products.*','category.name as cat_name')
        //     ->leftJoin('category','products.category_id','=','category.id')->orderBy('id', 'DESC')->paginate($perPage);
        // }
                     
        return view('admin.products.index');
    }

    public function datatable(Request $request ){

        $products = Product::with('category');

        if ($request->has('filter') && $request->get('filter') != '') {
            if($request->get('filter') == 'all'){
                $products;    
            }else{
                $products->where('feature', $request->get('filter'));
            }
        }

        $products->get();
        return Datatables::of($products)->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
        $category = Category::pluck('name','id')->prepend('Select Category',''); 
        $brand = Brand::pluck('name','id')->prepend('Select Brand',''); 
		return view('admin.products.create',compact('category','brand'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
     /*   echo '<pre>';
        $dataArray = array (
        );
          
       
        foreach($dataArray as $key => $val)
        { 
            $newArray = array();
            $newArray = $val;
            $newArray['product_type'] = 'solar_inverter';
            $newArray['description'] = $val['name'];
            $newArray['price'] = 0;
            $newArray['sale_price'] = 0;
            $newArray['category_id'] = 0;
            $newArray['rated_power_output'] = 0;
            $newArray['ac_power_rating'] = 0;
            $newArray['solar_dc_rating'] = 0;
            $newArray['battery_rated_storage_capacity'] = 0;
            $newArray['image'] = 'product.jpg';
            $product = Product::create($newArray);
        }
        exit;*/
		$this->validate($request, [
            'name' => 'required',
            'shortdesc' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
         //   'image' => 'required|mimes:jpeg,jpg,png|max:1000',
          //  'price' => 'integer|min:0',
          //  'sale_price' => 'integer|min:0'
        ]);
		$data = $request->all();
       
        if ($request->file('image')) {
            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Products'), $filename);
            $data['image'] = $filename;
        }
        else{
            $default_image = 'product.jpg';
            $data['image'] = $default_image ;
        }

        if($request->has('brand_id') && $request->brand_id != null){
            $brand = Brand::find($request->brand_id);
            $data['brand'] = $brand->name;
        }

        $product = Product::create($data);

        $files = $request->file('mimage');
        if ($request->file('mimage')) {
            foreach ($files as $image) {                
                $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('Products'), $filename);
                $dataimage['image']=$filename;
                $dataimage['product_id'] = $product->id; 
                $image = ProductImage::create($dataimage);
               
            }
        
        }

        
        Session::flash('flash_message', 'Product added!');
        return redirect('admin/products');
    }


 /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {  
        $product = Product::with('category')->find($id);   
        $images = array();
        if($product){
            $images = ProductImage::where('product_id',$id)->get();
            return view('admin.products.show', compact('product','images'));
        }else{
            return redirect('admin/products');
        }
    }
        

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $product = Product::where('id',$id)->first();
        if($product){
            $images = ProductImage::where('product_id',$id)->get();
            $category = Category::pluck('name','id')->prepend('Select Category','');   
            $brand = Brand::pluck('name','id')->prepend('Select Brand','');      
            return view('admin.products.edit', compact('product','category','images','brand'));
        }else{
            return redirect('admin/products');
        }
    }
    
     
 /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[            
            'name' => 'required',
            'shortdesc' => 'required',
          //  'category_id' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:1000',
          //  'price' => 'integer|min:0',
          //  'sale_price' => 'integer|min:0'
        ]);
        $requestData = $request->all(); 
       // echo '<pre>';print_r($requestData);exit; 
        $product = Product::findOrFail($id);

        if($request->has('brand_id') && $request->brand_id != null){
            $brand = Brand::find($request->brand_id);
            $requestData['brand'] = $brand->name;
        }

        if ($request->file('image')) {
            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Products'), $filename);
            $requestData['image'] = $filename;
        }    


        $files = $request->file('mimage');
        if ($request->file('mimage')) {        
            if(count($files)){
                $images = ProductImage::where('product_id',$id);
                $images->delete();
                foreach ($files as $image) {                    
                    $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('Products'), $filename);
                
                    $imageData['image']=$filename;
                    $imageData['product_id'] = $id;
                    $image = ProductImage::create($imageData);
                }
            }
        }
        
	    $product->update($requestData);

        Session::flash('flash_message','Product Updated Successfully!');
		
        return redirect('admin/products');
    }

        
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
       
        $product = Product::find($id);
        if($product->image != "product.jpg"){
            unlink(public_path('Products') . '/'.$product->image);
        }
            
        $product->delete();

        $images = ProductImage::where('product_id',$id);
        $images->delete();

        if($request->ajax()){
            $message='Product Deleted Successfully!';
             return response()->json(['messages'=>$message],200);
        }else{
            Session::flash('flash_message','Product Deleted Successfully!');            
            return redirect('admin/products');
        }
       
    }  


    public function deleteimage(Request $request)
    {
        $id =  $request->id;
        $image = ProductImage::where('id',$id)->first();
        $image->delete();
        $JSONARRAY = Array(
            'msg'=>'Success',
        );
        // echo json_encode($JSONARRAY);
        exit;
    }   
    
    public function make_feature(Request $request, $id){

        $product = Product::find($id);
        $product->feature = $request->feature;
        $product->save();
        return redirect()->back();

    }

       

    }


