<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Complaint;
use App\Role;
use App\User;
use Session;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class ComplaintController extends Controller
{
    public function __construct()
    {
       /* $this->middleware('permission:access.complaints');
        $this->middleware('permission:access.complaint.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.complaint.create')->only(['create', 'store']);
        $this->middleware('permission:access.complaint.delete')->only('destroy');*/
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->checkComplaintStatus();

        $complaints = Complaint::latest()->paginate(5);
        $users = User::with('roles')->get();
        $id = [];
        foreach($users as $user){
            if ($user->name != 'wesley' && $user->name != 'wesleyfraser') {
               foreach($user->roles as $role){
                    $id[] = $user->id;
                } 
            }
            
        }
        $sales = User::whereIn('id',$id)->get();
  
        return view('admin.complaint.index',compact('complaints','sales'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function checkComplaintStatus() {
        $complaints = Complaint::latest()->where('status','!=','Completed')->get();
        foreach($complaints as $complaint){
            $start = Carbon::parse($complaint->created_at);
            $end = Carbon::now();
            $hours = $end->diffInHours($start);
            if ($complaint->status == "New" && ($hours > 24 && $hours < 48) ) {
               $complaint->status = "Action required";
               $complaint->save();
            }
            elseif ($complaint->status == "Action required" && isset($complaint->actiontaken)  && $hours > 120) {
                $complaint->status = "To be resolved";
                $complaint->save();
            }
            elseif ($complaint->status == "Action required" && !isset($complaint->actiontaken)  && ($hours > 48 && $hours < 120)) {
                $complaint->status = "Action URGENT";
                $complaint->save();
            }
            elseif ($hours > 168 && $hours < 504  && !isset($complaint->dateresolved)) {
                $complaint->status = "Resolution required";
                $complaint->save();
            }
            elseif ($hours > 504 && !isset($complaint->dateresolved)) {
                $complaint->status = "Resolution urgent";
                $complaint->save();
            }
          //return $end.' '.$start.' '.$hours.' '.$complaint->created_at.' '.$complaint->complaintmessage;
        }
        return $complaints;
    }

    public function datatable(Request $request){

       $complaints = Complaint::with('assigneduserdetails');
       if ($request->has('filter') && $request->get('filter') != '') {

            if($request->get('filter') != 'All'){
                $complaints->where('status', $request->get('filter'));
            }
            
        }
 
        $complaints->get();

        return Datatables::of($complaints)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $users = User::with('roles')->get();
        $id = [];
        foreach($users as $user){
            if ($user->name != 'wesley' && $user->name != 'wesleyfraser') {
               foreach($user->roles as $role){
                    $id[] = $user->id;
                } 
            }
            
        }
        $sales = User::whereIn('id',$id)->get();
        return view('admin.complaint.create',compact('sales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request['dateresolved'] != "") {
           $date = Carbon::createFromFormat('d-m-Y', $request['dateresolved'], 'Australia/Melbourne'); 
           $request['dateresolved'] = $date;
        }

        if ($request['firstActionDate'] != "") {
           $date = Carbon::createFromFormat('d-m-Y', $request['firstActionDate'], 'Australia/Melbourne'); 
           $request['firstActionDate'] = $date;
        }
        if ($request['created_at'] != "") {
           $date = Carbon::createFromFormat('d-m-Y', $request['created_at'], 'Australia/Melbourne'); 
           $request['created_at'] = $date;
        }
        
        Complaint::create($request->all());
        
        return redirect('admin/complaints');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function show(Complaint $complaint)
    {
        //
        return view('admin.complaint.show',compact('complaint'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function edit(Complaint $complaint)
    {
        //
        $users = User::with('roles')->get();
        $id = [];
        foreach($users as $user){
            if ($user->name != 'wesley' && $user->name != 'wesleyfraser') {
               foreach($user->roles as $role){
                    $id[] = $user->id;
                } 
            }
        }
        $sales = User::whereIn('id',$id)->get();
        return view('admin.complaint.edit',compact('complaint','sales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //
        //$complaint->update($request->all());
        

        $complaint = Complaint::findOrFail($id);

        $actiontakenMsg = $complaint->actiontaken;
        $outcomeMsg = $complaint->outcome;
        $companyChangesMsg = $complaint->companychanges;
        $furtherActionMsg = $complaint->furtheractions;
        
        
        $user_timestamp = Carbon::now()->format('d-m-Y h:m:s')." - ".\Auth::user()->name;

        if ($request['dateresolved'] != "") {
           $date = Carbon::createFromFormat('d-m-Y', $request['dateresolved'], 'Australia/Melbourne'); 
           $request['dateresolved'] = $date;
        }

        if ($request['firstActionDate'] != "") {
           $date = Carbon::createFromFormat('d-m-Y', $request['firstActionDate'], 'Australia/Melbourne'); 
           $request['firstActionDate'] = $date;
        }

        if ($request['actiontaken'] != "") {
           $actiontakenMsg.= "<br>".$user_timestamp.' - '.$request['actiontaken'];
           $request['actiontaken'] = $actiontakenMsg;
        }
        else {
            $request['actiontaken'] = $actiontakenMsg;
        }

        if ($request['outcome'] != "") {
           $outcomeMsg.= "<br>".$user_timestamp.' - '.$request['outcome'];
           $request['outcome'] = $outcomeMsg;
        }
        else {
           $request['outcome'] = $outcomeMsg;
        }

        if ($request['companychanges'] != "") {
           $companyChangesMsg.= "<br>".$user_timestamp.' - '.$request['companychanges'];
           $request['companychanges'] = $companyChangesMsg;
        }
        else {
           $request['companychanges'] = $companyChangesMsg;
        }

        if ($request['furtheractions'] != "") {
           $furtherActionMsg.= "<br>".$user_timestamp.' - '.$request['furtheractions'];
           $request['furtheractions'] = $furtherActionMsg;
        }
        else {
            $request['furtheractions'] = $furtherActionMsg;
        }
        $requestData = $request->all();
        $complaint->update($requestData);

        return redirect('admin/complaints');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Complaint $complaint)
    {
        //
    }
}
