<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.category');
        $this->middleware('permission:access.category.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.category.create')->only(['create', 'store']);
        $this->middleware('permission:access.category.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $categories = Category::where('name', 'LIKE', "%$keyword%")
                    ->orWhere('status', 'LIKE', "%$keyword%")
                    ->orderBy('id', 'DESC')
                    ->paginate($perPage);
        } else {
            $categories = Category::orderBy('id', 'DESC')->paginate($perPage);
        }
       //$categories = Category::All();
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
        
        return view('admin.category.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required|unique:category',         
        ]);
		$data = $request->all();

        $category = Category::create($data);

        Session::flash('flash_message', 'Category added!');

        return redirect('admin/category');
    }

     public function datatable(request $request)
    {
        $category = Category::All();        

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(name LIKE  '%$value%')";

                $category=Category::whereRaw($where_filter);
            }
        }     
        return Datatables::of($category)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   
        $category = Category::where('id',$id)->first();
       if($category) {
        //change client status
            $status = $request->get('status');
            if(!empty($status)){
                if($status == 'active' ){
                    $category->status= 'inactive';
                    $category->update();            

                    return redirect()->back();
                }else{
                    $category->status= 'active';
                    $category->update();               
                    return redirect()->back();
                }

            }
            return view('admin.category.show', compact('category'));
        }else{
            return redirect('admin/category');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$category = Category::where('id',$id)->first();
        if($category){
            return view('admin.category.edit', compact('category'));
        }else{
            return redirect('admin/category');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:category,name,' . $id,      
        ]);
        $requestData = $request->all();  

        $category = Category::findOrFail($id);

	    $category->update($requestData);

        flash('Category Updated Successfully!');
		
        return redirect('admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request ,$id)
    {
       
       
        $res = Category::where("id",$id)->first();
        if ($res) {
            Category::where("id",$id)->delete();
            $result['message'] = "Record Deleted Successfully.";
            $result['code'] = 200;
        } else {
            $result['message'] = "Something went wrong , Please try again later.";
            $result['code'] = 400;
        }
        if($request->ajax()){
            $message='Deleted';
             return response()->json(['message'=>$message],200);
        }else{

            Session::flash('flash_message','Category Deleted Successfully!');
            
            return redirect('admin/category');
        }
       
    }  

}
