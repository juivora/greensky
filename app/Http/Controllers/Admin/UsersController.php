<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\ActivationMail;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\CapsuleOwner;
use App\Opportunity;
use App\UserStates;
use DB;
use App\States;

class UsersController extends Controller

{

    public function __construct()
    {
        $this->middleware('permission:access.users');
        $this->middleware('permission:access.user.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.user.create')->only(['create', 'store']);
        $this->middleware('permission:access.user.delete')->only('destroy');
    }


    
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $users = User::with('roles')->where('name', 'LIKE', "%$keyword%")
                    ->orWhere('email', 'LIKE', "%$keyword%")
                    ->orderBy('id', 'DESC')
                    ->paginate($perPage);
        } else {
            $users = User::with('roles')->orderBy('id', 'DESC')->paginate($perPage);
        }

        //$users = User::with('roles')->get();
        
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');
        //$category = Category::pluck('name','id')->prepend('Select Category',''); 
        $capsule = CapsuleOwner::pluck('name','capsule_id')->prepend('--Select--','');

        $states = States::select('id', 'state', 'code')->get();
        $states = $states->pluck('state', 'code');

       
        return view('admin.users.create', compact('roles','capsule','states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:users', //unique:users,email,NULL,id,deleted_at,NULL
                'password' => 'required',
                'roles' => 'required',
                //'capsule_id'=>'unique:users,capsule_id'
                ]);


        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);


        $data['type'] = "admin";

        $user = User::create($data);

        // foreach ($request->roles as $role) {
        //     $user->assignRole($role);
        // }
        $user->assignRole($request->roles);

        // if($request->has('states')){
        //     foreach ($request->states as $states) {
        //         $user_states= UserStates::where('user_id',$user->id)->where('state_id',$states)->first();
        //         if(count($user_states) == 0 ){
        //             $u_state = new UserStates ;
        //             $u_state->user_id = $user->id;
        //             $u_state->state_id = $states;
        //             $u_state->save();
                    
        //         }
        //     } 
        // }

        if ($request->file('image')) {

            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/user/'), $filename);
            $user->image = $filename;
            $user->save();

        }

        Session::flash('flash_message', __('User added!'));

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::with('state')->where('id',$id)->first();

     
        if($user){           
            $opportunities = Opportunity::where('owner_id',$user->capsule_id)->get();
            return view('admin.users.show', compact('user','opportunities'));
        }else{
            return redirect('admin/users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {   
        if($id == 1 && Auth::user()->id != 1){
           
            return redirect()->back();
        }

        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');
        

        $states = States::select('id', 'state', 'code')->get();
        $states = $states->pluck('state','code');
        
        
        $user = User::with('roles','state')->where('id',$id)->first();
        
        if($user){

            $user_roles = [];
            foreach ($user->roles as $role) {
                $user_roles[] = $role->name;
            }
            

            $user_states = [];
            foreach($user->state as $state){
                $user_states[] = $state->state_id;
            }
            
            $capsule = CapsuleOwner::pluck('name','capsule_id')->prepend('--Select--','');
            return view('admin.users.edit', compact('user', 'roles', 'user_roles','capsule','states','user_states'));
        }else{
            return redirect('admin/users');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {   
        
        
        $this->validate($request,[
            'name' => 'required',
            'email' => 'unique:users,email,' . $id,
            //'roles' => 'required',
        ]);

        $data = $request->except('password','email');
        if ($request->has('password')) {
            // no need for bycript
            $data['password'] = bcrypt($request->password); 
        }
        if ($request->has('email')) {
            // no need for bycript
            $data['email'] = $request->email;
        }
        if ($request->file('image')) {

            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/user/'), $filename);
            $data['image'] = $filename;

        }

        $user = User::findOrFail($id);
        $user->update($data);

        if($request->has('roles') && ( $request->roles != null || $request->roles != '' )){
            
            $user->roles()->detach();

            $user->assignRole($request->roles);
        } 

        

        Session::flash('flash_message', __('User updated!'));

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request ,$id)
    {
        $result = array();
        if(Auth::user()->id==$id  || $id==1 ){
            $result['message'] = "You don't have permission to delete this user";
            $result['code'] = 400;
        }else{
           try {
                $res = User::where("id",$id)->first();
                if ($res) {
                    User::where("id",$id)->delete();
                    $result['message'] = "Record Deleted Successfully.";
                    $result['code'] = 200;
                } else {
                    $result['message'] = "Something went wrong , Please try again later.";
                    $result['code'] = 400;
                }
            } catch (\Exception $e) {
                $result['message'] = $e->getMessage();
                $result['code'] = 400;
            }
        }

      

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{

            Session::flash('flash_message','User Deleted Successfully!');
            
            return redirect('admin/users');
        }
       

    }  
 
}
