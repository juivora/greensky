<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.brand');
        $this->middleware('permission:access.brand.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.brand.create')->only(['create', 'store']);
        $this->middleware('permission:access.brand.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $brand = Brand::where('name', 'LIKE', "%$keyword%")
                    ->orWhere('status', 'LIKE', "%$keyword%")
                    ->orderBy('id', 'DESC')
                    ->paginate($perPage);
        } else {
            $brand = Brand::orderBy('id', 'DESC')->paginate($perPage);
        }
       //$brand = Brand::All();
        return view('admin.brand.index',compact('brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
        
        return view('admin.brand.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required|unique:brand',  
            'image' => 'required|mimes:jpeg,jpg,png,gif',     
        ]);
        $data = $request->all();

        if ($request->file('image')) {

            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('brand/'), $filename);
            $data['image'] = $filename;

        }

        $brand = Brand::create($data);

        Session::flash('flash_message', 'Brand added!');

        return redirect('admin/brand');
    }

     public function datatable(request $request)
    {
        $brand = Brand::All();        

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(name LIKE  '%$value%')";

                $brand=Brand::whereRaw($where_filter);
            }
        }     
        return Datatables::of($brand)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   
        $brand = Brand::where('id',$id)->first();
       if($brand) {
        //change client status
            $status = $request->get('status');
            if(!empty($status)){
                if($status == 'active' ){
                    $brand->status= 'inactive';
                    $brand->update();            

                    return redirect()->back();
                }else{
                    $brand->status= 'active';
                    $brand->update();               
                    return redirect()->back();
                }

            }
            return view('admin.brand.show', compact('brand'));
        }else{
            return redirect('admin/brand');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$brand = Brand::where('id',$id)->first();
        if($brand){
            return view('admin.brand.edit', compact('brand'));
        }else{
            return redirect('admin/brand');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:brand,name,' . $id, 
            'image' => 'sometimes|mimes:jpeg,jpg,png,gif',      
        ]);
        $requestData = $request->all();
        
        if ($request->file('image')) {

            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('brand/'), $filename);
            $requestData['image'] = $filename;

        }

        $brand = Brand::findOrFail($id);

	    $brand->update($requestData);

        flash('Brand Updated Successfully!');
		
        return redirect('admin/brand');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request ,$id)
    {
       
       
        $res = Brand::where("id",$id)->first();
        if ($res) {
            Brand::where("id",$id)->delete();
            $result['message'] = "Record Deleted Successfully.";
            $result['code'] = 200;
        } else {
            $result['message'] = "Something went wrong , Please try again later.";
            $result['code'] = 400;
        }
        if($request->ajax()){
            $message='Deleted';
             return response()->json(['message'=>$message],200);
        }else{

            Session::flash('flash_message','Brand Deleted Successfully!');
            
            return redirect('admin/brand');
        }
       
    }  

}
