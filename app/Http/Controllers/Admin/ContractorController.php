<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contractor;
use Illuminate\Http\Request;
use Session;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $contractor = Contractor::where('full_name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $contractor = Contractor::paginate($perPage);
        }

        return view('admin.contractor.index', compact('contractor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.contractor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'full_name' => 'required'
		]);
        $requestData = $request->all();
        
        Contractor::create($requestData);

        Session::flash('flash_message', 'Contractor added!');

        return redirect('admin/contractor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $contractor = Contractor::findOrFail($id);

        return view('admin.contractor.show', compact('contractor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $contractor = Contractor::findOrFail($id);

        return view('admin.contractor.edit', compact('contractor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'full_name' => 'required'
		]);
        $requestData = $request->all();
        
        $contractor = Contractor::findOrFail($id);
        $contractor->update($requestData);

        Session::flash('flash_message', 'Contractor updated!');

        return redirect('admin/contractor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Contractor::destroy($id);

        Session::flash('flash_message', 'Contractor deleted!');

        return redirect('admin/contractor');
    }
}
