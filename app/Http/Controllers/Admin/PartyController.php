<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PDF;
use Carbon\Carbon;
use App\Party;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;
use App\UserStates;
use App\States;
use App\Package;
use App\Setting;
use App\Folders;
use App\CustomerOffer;
use App\Offer;
use App\PackageProduct;
use App\Product;
use App\Job;
use App\User;
use App\Providers;
use App\OfferImage;
use App\Note;
use Storage;
use Mail;
use File;
use mPDF;
use DB;
use XeroPrivate;

class PartyController extends Controller
{   
    public $mPdf;

    public function __construct()
    {
       // $this->middleware('permission:access.customers');
        $this->middleware('permission:access.customers.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.customers.create')->only(['create', 'store']);
        $this->middleware('permission:access.customers.delete')->only('destroy');
        
        $this->mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $this->mPdf->SetDisplayMode('fullpage');
        $this->mPdf->list_indent_first_level = 0;
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   

        $users = User::with('roles')->get();
        $id = [];
        foreach($users as $user){
            foreach($user->roles as $role){
                if($role->name == "Sales" or  $role->name == "Admin" ){
                    $id[] = $user->id;
                }
            }
        }
        $sales = User::whereIn('id',$id)->get();
        
        return view('admin.customer.index',compact('sales'));
    }

    public function datatable(Request $request){

        
        
        $party = Party::with('offer','folders','jobs')
        ->select( DB::raw("CONCAT(parties.first_name,' ',parties.last_name) AS customer_name"),'parties.*', 'parties.first_name','parties.last_name as last_name','users.name as sales_name')
        ->leftjoin('users','users.id','=','parties.sales_id')
        ->where('party_id','!=',0);
        
        if(\Auth::user()->roles[0]->name == "Sales"    ){
            $party->where('sales_id',\Auth::user()->id);
        }

        if ($request->has('filter') && $request->get('filter') != '') {
            if($request->get('filter') == 'all'){
                $party = $party->where('customer_status','!=',3);    
            }else{
                $party->where('customer_status', $request->get('filter'));
            }
        }
        $party->get();

        return Datatables::of($party)->make(true);
    }

    public function datatableOffers(Request $request){

        
        
        $party = Party::with('offer','folders','jobs')
        ->select( DB::raw("CONCAT(parties.first_name,' ',parties.last_name) AS customer_name"),'parties.*', 'parties.first_name','parties.last_name as last_name','users.name as sales_name')
        ->leftjoin('users','users.id','=','parties.sales_id')
        ->where('party_id','!=',0);

        $offer = DB::table('offer')
            ->join('parties', 'offer.customer_id', '=', 'parties.party_id')
            ->join('users', 'users.id', '=', 'parties.sales_id')
            ->select('offer.*', DB::raw("CONCAT(parties.first_name,' ',parties.last_name) AS customer_name"), 'parties.first_name','parties.last_name as last_name','parties.email as email', 'users.name as sales_name')
            ->get();
        
        if(\Auth::user()->roles[0]->name == "Sales"    ){
            $party->where('sales_id',\Auth::user()->id);
        }

        if ($request->has('filter') && $request->get('filter') != '') {
            if($request->get('filter') == 'all'){
                $party = $party->where('customer_status','!=',3);    
            }else{
                $party->where('customer_status', $request->get('filter'));
            }
        }
        $party->get();

        return Datatables::of($offer)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   

        $state = States::select('id', 'state', 'code')->get();
        $state = $state->pluck('state', 'code');

        $providers = Providers::all()->pluck('name','id')->prepend('Select Provider','');
       
        return view('admin.customer.create',compact('state','providers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {   
        
        $this->validate($request, [
           // 'meter_phase' => 'required',       
        ]);

        $requestData = $request->all();

        $requestData['status'] = 0;
        if($request->has('meter_phase') && $request->meter_phase != null ){
            $requestData['meter_phase'] = json_encode($request->meter_phase);
        }
        
        $party = Party::create($requestData);

        $contact_name = \Auth::user()->name;

        //$date1 = "2019-07-11"; 
        //$date2 = date("Y-m-d"); 
        //|| $date2 < $date1
        $post_code_match = false;
        if ($requestData['post_code'] != '') {
            $tas_sales = User::where('post_codes', 'like','%'.$requestData['post_code'].'%')->first();
            if (isset($tas_sales)) {
                $contact_name = $tas_sales->name;
                $party->sales_id = $tas_sales->id;

                if ($tas_sales->id > 0) {
                    $post_code_match = true;
                }
            }
            
        }

        $party->party_id = $party->id;

        $party->save();

        $sender_name = Setting::give_value_of('Sender_Name');
        $sender_email = Setting::give_value_of('Sender_Email');
        $sender_cc = Setting::give_value_of('Sender_CC');
        $cc_array = [];
        $cc_array = explode(',', $sender_cc);
        $sender_bcc = Setting::give_value_of('Sender_BCC');
        $bcc_array = [];
        $bcc_array = explode(',', $sender_bcc);

        $parties = $party;

        if ($post_code_match == true) {
            $customer = $party;
            $sales = $tas_sales;
            $this->assignSalesRepMail($party,$tas_sales); 
        }
        else {
            if($parties->email && $sender_email && $sender_name){
            \Mail::send('email.order.contactus', compact('parties','contact_name'), function ($message) use ($parties,$sender_email,$sender_name, $cc_array, $bcc_array) {
                $message->from($sender_email, $sender_name)
                        ->to($parties->email)
                        ->cc($cc_array)
                        ->bcc($bcc_array)
                        ->subject('Contact Request!');
                                        
            });
          }
        }
        
        if($parties->email){
            
            //Subscribe email to MailChimp
            header('content-type: application/json; charset=utf-8');
            header("access-control-allow-origin: *");
            $apiKey = env('MAILCHIMP_API_KEY');
            $list_id = env('LIST_ID');
            
            $json = '{"email_address": "'.$parties->email.'","status": "subscribed"}';
        
            $url = "https://us18.api.mailchimp.com/3.0/lists/".$list_id."/members/";

            
            $ch = curl_init($url);
            
            curl_setopt($ch, CURLOPT_USERPWD, 'ishan:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 
            $result = curl_exec($ch);
           

            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
 
        }

        // Make entry in Capsule CRM
       
    
            $partyArray['party'] = array(
                'type' => "person",
                'name' => $party->first_name.' '.$party->last_name,
                'firstName' => $party->first_name,
                'lastName' => $party->last_name,
                'emailAddresses' => array (
                    0 => array (                 
                        'address' => $party->email,
                    ),
                ),
                'phoneNumbers' =>  array (
                     0 => array (  
                         'type' => 'Mobile',               
                         'number' => number_format($party->phone_mobile),
                     ),
                     1 => array (  
                         'type' => 'Home',               
                         'number' => number_format($party->phone_home),
                     ),
                ),
                'addresses' => array (
                    0 => array (                 
                        "country" => "Australia",
                        "street" => $party->unit.' '.$party->street_number.' '.$party->street_name.' '.$party->suburb,
                        "state" => $party->state,
                        "zip" => $party->post_code,
                    ),
                ),
            );
            
            $Url = "https://api.capsulecrm.com/api/v2/parties/".$party->capsule_id;
            
            $ch = curl_init();
            $headers = array(
                'Authorization: Bearer 0e7qbw0RS7kLwZb7uLjutkZZ5HpZn+M1tKuGu5sQCfRrRC2GEA3GC/Cm6Pvb3IVC',
                'Accept: application/json',
                'Content-Type: application/json',
            );
            curl_setopt($ch, CURLOPT_URL, $Url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // Timeout in seconds
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            $result = curl_exec($ch);
            curl_close($ch);
            $finalArray = json_decode($result); 
            
            if($finalArray){
                if(isset($finalArray->party)){
                    // if($finalArray->party->id == $party->capsule_id){
                    //     $party_data = $this->curl_call("https://api.capsulecrm.com/api/v2/parties/".$party->capsule_id,$partyArray); 
                       
                    // }
                }else{
                    $party_data = $this->curl_call("https://api.capsulecrm.com/api/v2/parties",$partyArray);
                    if(isset($party_data->message) && $party_data->message != "" ){
                                   
                    }else{
                        $party->capsule_id =  $party_data->party->id;
                        $party->save();
                    }
                    
                }
            }
        
        Session::flash('flash_message', 'Customer Added!');

        return redirect('admin/customer');
    }

    public function curl_call($url,$postArray){
        $ch = curl_init();
        $url = $url;
        // Set URL to download
        curl_setopt($ch, CURLOPT_URL, $url);      
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postArray)); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer 0e7qbw0RS7kLwZb7uLjutkZZ5HpZn+M1tKuGu5sQCfRrRC2GEA3GC/Cm6Pvb3IVC',
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen(json_encode($postArray)))                                                                       
        );
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // Download the given URL, and return output
        $output = curl_exec($ch);
        curl_close($ch); 
        $result = json_decode($output);
        return $result;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show(Request $request,$id)
    {   
        $party = Party::find($id);

        if(!$party){
            return redirect()->back();
        }

        $status =  $request->status;
    
        if($status != null){

            $party->customer_status = $status;
            $party->save();

            $message = "Customer's Status Changed Successfully !!";

            return response()->json(['messages' => $message],200);

        }else{
            return view('admin.customer.show', compact('party'));
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $party = Party::find($id);
         if(!$party){
            return redirect()->back();
        }
        

        $state = States::select('id', 'state', 'code')->get();
        $state = $state->pluck('state', 'code');

        $providers = Providers::all()->pluck('name','id')->prepend('Select Provider','');
        

        return view('admin.customer.edit', compact('party','state','providers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {      
        $this->validate($request, [
         //   'meter_phase' => 'required',       
        ]);
        
        $requestData = $request->all();
        if($request->has('meter_phase') && $request->meter_phase != null ){
            $requestData['meter_phase'] = json_encode($request->meter_phase);
        }
        
        $party = Party::findOrFail($id);

        if($party->status == 0){
            $requestData['party_data'] = json_encode($requestData) ;
        }


        $party->update($requestData);

        Session::flash('flash_message', 'Customer Updated !');

        return redirect('admin/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
         $party = Party::find($id);
        $party->offer()->delete();
        $party->folders()->delete();
        $party->jobs()->delete();
        $party->delete();

        $message = "Customer Deleted Successfully !!";

        return response()->json(['messages' => $message],200);
    }


    public function createoffer(Request $request,$id){
        
        $discount = Setting::give_value_of('Max_Rebate');

        $customer = Party::find($id);
        
        if($customer != ''){
            
            if(!empty($request->submit) && $request->submit == 'submit'){
              
              
                $offer = new Offer;
                $offer->customer_id = $id ;
                $offer->follow_up_date = date('Y-m-d');
                $offer->mail_counter = 0;
                $offer->created_by = \Auth::user()->id;
                $offer->save();

                // Once Offer is created, lead -> prospect
                $customer->customer_status = 2;
                $customer->save();


                //Make Entry for Customer Offer For multiple package 
                $packages = $request->get('package');
                
                
                //Get all the files which are there in All the Packages Folders 
                $pk_folder = Folders::whereIn('package_id',$packages)->pluck('path','id')->toArray();
                $pk_folder = array_unique($pk_folder);
                $pk_files = array() ;
                foreach($pk_folder as $key => $value){
                    $pk_files[] = Folders::find($key);
                }
                $pk_files = array_unique($pk_files) ;

                foreach ($packages as $package){
    
                    $package_obj = Package::find($package);
                    
                    $package_product = PackageProduct::where('package_id',$package_obj->id)->get();
                    
                    $product_obj = [];
                    foreach ($package_product as $pk_pr){
                        $product_obj_a = Product::with('category','brand')->find($pk_pr->product_id);
                        $product_obj_a->quantity = $pk_pr->quantity ;
                        $product_obj[] = $product_obj_a;
                    } 
                   
                    $cust_offer = new CustomerOffer ;
                    $cust_offer->customer_id = $id ;
                    $cust_offer->offer_id =  $offer->id;
                    $cust_offer->package_id = $package_obj->id ;
                    $cust_offer->rebates = $request->rebates;
                    $cust_offer->loan_amount = $request->loan_amount ;
                    $cust_offer->package_kw = $package_obj->kw ;
                    $cust_offer->package_price = $request->price[$package_obj->id];
                    $cust_offer->extra_price = $request->extra_price ;
                    $cust_offer->package_discount = $request->discount[$package_obj->id];
                    $cust_offer->package_discount_value = $request->discount_value[$package_obj->id];
                    $cust_offer->package_net_value = $request->net_value[$package_obj->id];
                    $cust_offer->package_obj = $package_obj;
                    $cust_offer->customer_obj = $customer;
                    $cust_offer->product_obj = json_encode($product_obj);
                    $cust_offer->description = $request->get('description');
                    $cust_offer->save();
          
                }
                
                //Make entry for images when offer is created
                if($request->hasFile('solar_panel_placement') && $request->solar_panel_placement != '' ){
                   
                    $image = $request->file('solar_panel_placement');         
                    $filename =  'Solar Panel Placement-'.$offer->id.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('offer/'.$offer->id), $filename);
                    
                    $offer_image = new OfferImage;
                    $offer_image->offer_id = $offer->id;
                    $offer_image->name = "solar_panel_placement";
                    $offer_image->path = $filename;
                    $offer_image->save();
                }
                if($request->hasFile('system_params') && $request->system_params != '' ){

                    $image = $request->file('system_params');           
                    $filename =  'System Parameters-'.$offer->id.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('offer/'.$offer->id), $filename);
                    
                    $offer_image = new OfferImage;
                    $offer_image->offer_id = $offer->id;
                    $offer_image->name = "system_params";
                    $offer_image->path = $filename;
                    $offer_image->save();
                }
                if($request->hasFile('performance') && $request->performance != '' ){

                    $image = $request->file('performance');           
                    $filename =  'Estiamte Performance-'.$offer->id.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('offer/'.$offer->id), $filename);
                    
                    $offer_image = new OfferImage;
                    $offer_image->offer_id = $offer->id;
                    $offer_image->name = "performance";
                    $offer_image->path = $filename;
                    $offer_image->save();
                }
                if($request->hasFile('daily_average') && $request->daily_average != '' ){

                    $image = $request->file('daily_average');           
                    $filename =  'Average Daily Energy Output-'.$offer->id.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('offer/'.$offer->id), $filename);
                    
                    $offer_image = new OfferImage;
                    $offer_image->offer_id = $offer->id;
                    $offer_image->name = "daily_average";
                    $offer_image->path = $filename;
                    $offer_image->save();
                }
                if($request->has('panel_orientation') && $request->panel_orientation != '' ){

                    $image = $request->file('panel_orientation');           
                    $filename =  'Panel orientation-'.$offer->id.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('offer/'.$offer->id), $filename);
                    
                    $offer_image = new OfferImage;
                    $offer_image->offer_id = $offer->id;
                    $offer_image->name = "panel_orientation";
                    $offer_image->path = $filename;
                    $offer_image->save();
                } 
                
                $date = date('d-m-Y');
                $offer = Offer::with('offer_detail','job')->find($offer->id);
                $stc = 0;
                if($offer->job){
                    $stc = (int)$offer->job->no_of_stc ;
                }
        
                $customer = Party::with('sales','retailer')->find($offer->customer_id); 
        
                $filename = $customer->first_name."-offer-".$offer->id.".pdf";
        
                $company_details =  array();
                
                $setting  = Setting::get();
                foreach($setting as $key => $value) {
                    $company_details[$value['name']] = $value['value'];   
                }
                $company_details = (object) $company_details;
                if($company_details){
                    $company_details->STC_Price = (int)$company_details->STC_Price ;
                }


                //get offerImages
                $offer_img = OfferImage::where('offer_id',$offer->id)->get();
                $solar_panel_placement = '';
                $system_params = '';
                $daily_average = '';
                if(count($offer_img) > 0){
                    foreach ($offer_img as $images ){
                        if($images->name == "solar_panel_placement" && $images->path != ''){
                            $solar_panel_placement = $images->path ;
                        }
                        if($images->name == "system_params" && $images->path != ''){
                            $system_params = $images->path ;
                        }
                        if($images->name == "daily_average" && $images->path != ''){
                            $daily_average = $images->path ;
                        }
                    }
                } 

                $firstpage = $this->create_proposal_page_1($customer,$offer);
                $this->create_proposal_page_2($customer,$offer);
                $this->create_proposal_page_9($customer,$offer);

                $view = view('admin.customer.proposal-1',compact('offer','customer','company_details','date','stc','solar_panel_placement' , 'system_params','daily_average','offer_img','firstpage' ))->render();
                //return $view;
                
                $pdf = \PDF::loadHTML($view); 
            
                // return $pdf->download($filename);
                //return;
                
                //find salesrep
                $sales = User::find($customer->sales_id);

                // Send mail with Attachment of QAF Form
                $sender_name = Setting::give_value_of('Sender_Name');
                $sender_email = Setting::give_value_of('Sender_Email');
                $sender_cc = Setting::give_value_of('Sender_CC');
                $cc_array = [];
                $cc_array = explode(',', $sender_cc);
                $sender_bcc = Setting::give_value_of('Sender_BCC');
                $bcc_array = [];
                $bcc_array = explode(',', $sender_bcc);
                
                if( $sales && $sales->email != ""){
                   // $cc_array[] =  $sales->email ; 
                     $cc_array = explode(',',  $sales->email);
                    $sender_email = $sales->email;
                    $sender_name = $sales->name;
                }
                
                if($customer->email && $sender_email && $sender_name){
                    
                    \Mail::send('email.order.pdfMail', compact('customer','offer'), function ($message) use ($customer , $pdf , $sender_email,$sender_name, $cc_array , $bcc_array , $pk_files) {
                        $message->from($sender_email, $sender_name)
                            //->to("wesleyfraser@gmail.com")
                            ->to($customer->email)
                            ->cc($cc_array)
                            ->bcc($bcc_array)
                            ->subject('Solar Quotation Form')
                            ->attachData($pdf->stream('Proposal.pdf'), "Proposal.pdf")
                            ->attach(public_path()."/documents/CEC_COC_REBRAND_A4_v2 - Solar Retailer Code of Conduct.pdf",['as' => 'Solar Retailer Code of Conduct.pdf' ]);
                            foreach($pk_files as $doc){
                                $message->attach(public_path()."/package/".$doc->package_id."/".$doc->path,['as' => $doc->name ]);
                            }       
                                    
                    });
                }
                
                Session::flash('flash_message','Customer Offer Added Successfully!');   
                return redirect('/admin/offers/'.$customer->id);
                
            }
            
        }else{

            return redirect()->back();

        }

        return view('admin/customer/createoffer',compact('customer','discount'));
    } 

    function create_proposal_images($customer, $offer) {
        $this->create_proposal_page_1($customer, $offer); 

    }

    function create_proposal_page_1($customer, $offer) {
        //$jpg_image = imagecreatefromjpeg(public_path().'/assets/images/1/GreenSky_SalesBrochure_1.jpg');
         $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/4/GreenSky_SalesBrochure_01.jpg');
         $white = imagecolorallocate($jpg_image, 109, 110, 112);
         $addresscolor = imagecolorallocate($jpg_image, 100, 100, 100);
         $datecolor = imagecolorallocate($jpg_image, 103, 103, 103);
         $grey = imagecolorallocate($jpg_image, 169, 169, 169);
         $font_path = public_path().'/assets/fonts/Zurich Condensed BT.ttf';
         $text = $customer->first_name.' '.$customer->last_name;
         $textaddress1 = trim("".trim($customer->unit).' '.trim($customer->street_number).' '.trim($customer->street_name));
         $textaddress2 = "".$customer->suburb.' '.$customer->post_code.' '.$customer->state;
         //$textaddress3 = "".$customer->post_code;
         //$textaddress4 = "".$customer->state;


         $setting  = Setting::get();
                
         foreach($setting as $key => $value) {
            if($value['name'] == 'Address')
            {
               $company_address = explode("<br/>", $value['value']);
            }                  
         }

         $addressline1 = trim($company_address[0]);
         $addressline2 = trim($company_address[1]);
         $date = date('d F Y');
         $dateline = $date;
         imagettftext($jpg_image, 40, 0, 150, 1800, $white, $font_path, $text);
         imagettftext($jpg_image, 36, 0, 150, 1865, $addresscolor, $font_path, $textaddress1);
         imagettftext($jpg_image, 36, 0, 150, 1930, $addresscolor, $font_path, $textaddress2);
         //imagettftext($jpg_image, 36, 0, 360, 1995, $addresscolor, $font_path, $textaddress3);
         //imagettftext($jpg_image, 36, 0, 360, 2060, $addresscolor, $font_path, $textaddress4);
         imagettftext($jpg_image, 36, 0, 1080, 1875, $datecolor, $font_path, $dateline);
         imagettftext($jpg_image, 26, 0, 1080, 2198, $grey, $font_path, $addressline1);
         imagettftext($jpg_image, 26, 0, 1080, 2242, $grey, $font_path, $addressline2);

        // imagettftext($jpg_image, 75, 0, 212, 2700, $white, $font_path, $text);
        // imagettftext($jpg_image, 53, 0, 212, 2800, $addresscolor, $font_path, $textaddress1);
         //imagettftext($jpg_image, 53, 0, 533, 2870, $addresscolor, $font_path, $textaddress2);
        // imagettftext($jpg_image, 53, 0, 533, 2940, $addresscolor, $font_path, $textaddress3);
        // imagettftext($jpg_image, 53, 0, 533, 3010, $addresscolor, $font_path, $textaddress4);
        // imagettftext($jpg_image, 53, 0, 1621, 2815, $datecolor, $font_path, $dateline);

         $offerpath = public_path().'/offer/'.$offer->id;
         if(!File::exists($offerpath)) {
            \File::makeDirectory($offerpath, $mode = 0777, true, true);
         }

         imagejpeg($jpg_image,$offerpath.'/GreenSky_SalesBrochure_1_'.$offer->id.'.jpg');
         imagedestroy($jpg_image);

    }

    function create_proposal_page_2($customer, $offer) {
        //$jpg_image = imagecreatefromjpeg(public_path().'/assets/images/1/GreenSky_SalesBrochure_1.jpg');
         $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/4/GreenSky_SalesBrochure_02.jpg');
         $white = imagecolorallocate($jpg_image, 255, 255, 255);
         $addresscolor = imagecolorallocate($jpg_image, 100, 100, 100);
         $datecolor = imagecolorallocate($jpg_image, 103, 103, 103);
         $grey = imagecolorallocate($jpg_image, 169, 169, 169);
         $font_path = public_path().'/assets/fonts/Zurich Condensed BT.ttf';

          $sales = User::find($customer->sales_id);
         //
         //
         //
         if ($sales) {
            $text = "Your Sales Representative:";

            $name = trim($sales->name);
            $email = trim($sales->email);
            $phone = trim($sales->phone);
         
            imagettftext($jpg_image, 26, 0, 1122, 1963, $white, $font_path, $text);
            imagettftext($jpg_image, 24, 0, 1122, 2000, $white, $font_path, $name);
            imagettftext($jpg_image, 24, 0, 1122, 2037, $white, $font_path, $phone);
            imagettftext($jpg_image, 24, 0, 1122, 2074, $white, $font_path, $email);
         }

         $offerpath = public_path().'/offer/'.$offer->id;
         if(!File::exists($offerpath)) {
            \File::makeDirectory($offerpath, $mode = 0777, true, true);
         }

         imagejpeg($jpg_image,$offerpath.'/GreenSky_SalesBrochure_2_'.$offer->id.'.jpg');
         imagedestroy($jpg_image);

    }

    function create_proposal_page_9($customer, $offer) {
        //$jpg_image = imagecreatefromjpeg(public_path().'/assets/images/1/GreenSky_SalesBrochure_1.jpg');
        $jpg_image = imagecreatefromjpeg(public_path().'/assets/images/4/GreenSky_SalesBrochure_09.jpg');
        $black = imagecolorallocate($jpg_image, 0, 0, 0);
        $grey = imagecolorallocate($jpg_image, 97, 97, 97);
        $customer_name = $customer->first_name.' '.$customer->last_name;
        $font_path = public_path().'/assets/fonts/Zurich Condensed BT.ttf';
        $text = "X";
        $selected_system = "System selected: ________________________________________";
         
        $date = date('d F Y');
        $day = date('d');
        $month = date('m');
        $year = date('Y');
        $dateline = $date;
        imagettftext($jpg_image, 25, 0, 1468, 365, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1468, 478, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1468, 575, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1468, 707, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1468, 804, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1468, 936, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1468, 1019, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1468, 1115, $black, $font_path, $text);
        imagettftext($jpg_image, 25, 0, 1144, 1369, $black, $font_path, $customer_name);
        imagettftext($jpg_image, 25, 0, 222, 1458, $black, $font_path, $day);
        imagettftext($jpg_image, 25, 0, 270, 1458, $black, $font_path, $month);
        imagettftext($jpg_image, 25, 0, 325, 1458, $black, $font_path, $year);
        imagettftext($jpg_image, 20, 0, 815, 1460, $grey, $font_path, $selected_system);

         //($jpg_image, 36, 0, 1080, 1875, $datecolor, $font_path, $dateline);

        $offerpath = public_path().'/offer/'.$offer->id;
        if(!File::exists($offerpath)) {
          \File::makeDirectory($offerpath, $mode = 0777, true, true);
        }
        if($offer->sign == 1) {
            if (isset($offer->customer_sign)) {
                $signaturepath = $offerpath.'/signature'.$offer->id.'.jpg';
                $signature = base64_decode(str_replace("data:image/jpeg;base64,","",$offer->customer_sign));
                file_put_contents($signaturepath, $signature);

                list($width, $height) = getimagesize($signaturepath);
                //obtain ratio
                $imageratio = $width/$height;

                if ($imageratio >= 1){
                    $newwidth = 345;
                    $newheight = 345 / $imageratio; 
                }
                else{
                     $newwidth = 345;
                     $newheight = 345 / $imageratio;
                };
                
                $thumb = ImageCreateTrueColor($newwidth, $newheight);
                $source = imagecreatefromjpeg($signaturepath);

                // Resize
                imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagecopymerge($jpg_image, $thumb,386, 1248, 0, 0, $newwidth, $newheight, 75);
                imagedestroy($thumb);

                $package = Package::where('id',$offer->package_id)->first();
                imagettftext($jpg_image, 25, 0, 1000, 1458, $black, $font_path, $package->title);
            }
            
        }

        imagejpeg($jpg_image,$offerpath.'/GreenSky_SalesBrochure_09_'.$offer->id.'.jpg');
        imagedestroy($jpg_image);

    }
   
    public function resend_offer(Request $request, $id){
        
                $date = date('d-m-Y');
                $offer = Offer::with('offer_detail','job')->find($id);
                $stc = 0;
                if($offer->job){
                    $stc = (int)$offer->job->no_of_stc ;
                }
        
                $customer = Party::with('sales','retailer')->find($offer->customer_id); 
                
                $cust_offers = CustomerOffer::where('offer_id',$offer->id)->where('customer_id',$customer->id)->get();
                
                $pk_folder = [];
                foreach($cust_offers as $cust_offer ){ 
                    //Get all the files which are there in All the Packages Folders 
                    $pk_folder = Folders::where('package_id',$cust_offer->package_id)->pluck('path','id')->toArray();
        
                }
                $pk_folder = array_unique($pk_folder);
                $pk_files = array() ;
                foreach($pk_folder as $key => $value){
                    $pk_files[] = Folders::find($key);
                }
                $pk_files = array_unique($pk_files) ;
        
                $filename = $customer->first_name."-offer-".$offer->id.".pdf";
        
                $company_details =  array();
                
                $setting  = Setting::get();
                foreach($setting as $key => $value) {
                    $company_details[$value['name']] = $value['value'];   
                }
                $company_details = (object) $company_details;
                if($company_details){
                    $company_details->STC_Price = (int)$company_details->STC_Price ;
                }

                //get offerImages
                $offer_img = OfferImage::where('offer_id',$offer->id)->get();
                $solar_panel_placement = '';
                $system_params = '';
                $daily_average = '';
                if(count($offer_img) > 0){
                    foreach ($offer_img as $images ){
                        if($images->name == "solar_panel_placement" && $images->path != ''){
                            $solar_panel_placement = $images->path ;
                        }
                        if($images->name == "system_params" && $images->path != ''){
                            $system_params = $images->path ;
                        }
                        if($images->name == "daily_average" && $images->path != ''){
                            $daily_average = $images->path ;
                        }
                    }
                } 

                $view = view('admin.customer.proposal-1',compact('offer','customer','company_details','date','stc','solar_panel_placement' , 'system_params','daily_average','offer_img' ))->render();

                $pdf = \PDF::loadHTML($view); 
                
                //find salesrep
                $sales = User::find($customer->sales_id);

                // Send mail with Attachment of QAF Form
                $sender_name = Setting::give_value_of('Sender_Name');
                $sender_email = Setting::give_value_of('Sender_Email');
                $sender_cc = Setting::give_value_of('Sender_CC');
                $cc_array = [];
                $cc_array = explode(',', $sender_cc);
                $sender_bcc = Setting::give_value_of('Sender_BCC');
                $bcc_array = [];
                $bcc_array = explode(',', $sender_bcc);
                
                if( $sales && $sales->email != ""){
                   // $cc_array[] =  $sales->email ; 
                     $cc_array = explode(',',  $sales->email);
                    $sender_email = $sales->email;
                    $sender_name = $sales->name;
                }
             
                
                if($customer->email && $sender_email && $sender_name){
                    \Mail::send('email.order.pdfMail', compact('customer','offer'), function ($message) use ($customer , $pdf , $sender_email,$sender_name, $cc_array , $bcc_array,  $pk_files) {
                        $message->from($sender_email, $sender_name)
                            ->to($customer->email)
                            ->cc($cc_array)
                            ->bcc($bcc_array)
                            ->subject('Solar Quotation Form')
                            ->attachData($pdf->stream('Proposal.pdf'), "Proposal.pdf")
                            ->attach(public_path()."/documents/CEC_COC_REBRAND_A4_v2 - Solar Retailer Code of Conduct.pdf",['as' => 'Solar Retailer Code of Conduct.pdf' ]);
                            foreach($pk_files as $doc){
                                $message->attach(public_path()."/package/".$doc->package_id."/".$doc->path,['as' => $doc->name ]);
                            }  
                            
                                                      
                    });
                }
                
               Session::flash('flash_message','Proposal Offer Sent Successfully!');    
               return redirect()->back();
        
    }
    
    

    public function showpackage(Request $request){

        $customer =  Party::find($request->customer_id);
        $message = '';
        $min = $request->min ;
        $max = $request->max ;
        $packages = Package::whereBetween('kw',[$min,$max])->where('number_of_phase',$customer->number_of_phase)->get();
        if(count($packages) == 0){
            $message = "No Packages found in this Range";
        }
        return redirect()->back()->with(compact('packages','message'));
       
    }

    public function deleteoffer($id){

        $offer = Offer::find($id)->delete();

        Session::flash('flash_message','Offer Deleted Successfully!');            
        return redirect()->back();

    }

    public function save_pdf( $htmlview, $filename ){
        
        $mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $mPdf->SetDisplayMode('fullpage');
        $mPdf->list_indent_first_level = 0;
        $mPdf->WriteHTML($htmlview);
        return $mPdf->Output($filename);

    }

    public function download_pdf( $htmlview, $filename ) {

        $pdf = PDF::loadHTML($htmlview);
        return $pdf->download($filename);
    }

    public function view_pdf( $htmlview, $filename ){

        $mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $mPdf->SetDisplayMode('fullpage');
        $mPdf->list_indent_first_level = 0;
        $mPdf->WriteHTML($htmlview);
        return $mPdf->Output($filename, 'I');
    }


    public function changestatusoffer(Request $request,$id){
       
        $requestData = $request->all();
        $offer = Offer::find($id);
        
        
        $customer = Party::find($offer['customer_id']);   

        if($requestData['package_id'] != 0){
            $package = Package::where('id',$requestData['package_id'])->withTrashed()->first();
        }
        
        //find salesrep
        $sales = User::find($customer->sales_id);

        //Email details 
        $sender_name = Setting::give_value_of('Sender_Name');
        $sender_email = Setting::give_value_of('Sender_Email');
        $sender_cc = Setting::give_value_of('Sender_CC');
        $cc_array = [];
        $cc_array = explode(',', $sender_cc);
        $sender_bcc = Setting::give_value_of('Sender_BCC');
        $bcc_array = [];
        $bcc_array = explode(',', $sender_bcc);
        
        if( $sales && $sales->email != ""){
           // $cc_array[] =  $sales->email ; 
             $cc_array = explode(',',  $sales->email);
            $sender_email = $sales->email;
            $sender_name = $sales->name;
        }

        if($requestData['status'] == 'Accepted')
        {   
            
             
            if(!$package){
                Session::flash('flash_message', 'Selected Package might be Deleted or Expired. You Cannot Accept the Offer!! ');
                return redirect()->back();
            }
                   
            //check if the customer is having its NMI
            if($customer->nmi == "" || $customer->nmi == null){
                
                Session::flash('flash_message', 'Customer Does not have its NMI number. You Cannot Accept the Offer!! ');
                return redirect()->back();
            }
            //check if the customer is having its  Meter number
            if($customer->meter_number == "" || $customer->meter_number == null){
                
                Session::flash('flash_message', 'Customer Does not have its Meter number. You Cannot Accept the Offer!! ');
                return redirect()->back();
            }
            //check if the customer is having its NMI,  Meter number, number of phases 
            if($customer->number_of_phase == "" || $customer->number_of_phase == null){
                
                Session::flash('flash_message', 'Customer Does not have its Number of Phase . You Cannot Accept the Offer!! ');
                return redirect()->back();
            }

            //check whether all the three images are uploaded or not
            $count = 0;
            $offer_image = OfferImage::where('offer_id',$id)->get();
            foreach($offer_image as $images){
                if($images->name == 'solar_panel_placement'){
                    $filename = public_path('/offer/'.$offer->id.'/'.$images->path); 
                    if( file_exists($filename) ){
                        $count = $count + 1;
                    }
                     
                }
                if($images->name == 'system_params'){
                    $filename = public_path('/offer/'.$offer->id.'/'.$images->path); 
                    if( file_exists($filename) ){
                        $count = $count + 1;
                    }
                }
                if($images->name == 'daily_average'){
                    $filename = public_path('/offer/'.$offer->id.'/'.$images->path); 
                    if( file_exists($filename) ){
                        $count = $count + 1;
                    }
                }
            } 
            
            
            
            
            if($count == 3){
                

            

                //Once offer is signed Prospect to customer
                $customer->customer_status = 1 ;
                $customer->save();
                
                // Send Mail to Customer when offer is accepted

                if($customer->email && $sender_email && $sender_name){
                    \Mail::send('email.order.quoteAccept', compact('customer','package'), function ($message) use ($customer , $sender_email,$sender_name, $cc_array, $bcc_array) {
                        $message->from($sender_email, $sender_name)
                            ->to($customer->email)
                            ->cc($cc_array)
                            ->bcc($bcc_array)
                            ->subject('Quote Accepted!');                               
                    });
                }

                $companyDetails =  array();
                $setting  = Setting::get();
                
                foreach($setting as $key => $value) {
                    if($value['name'] == 'ABN_(if_applicable)')
                    {
                        $companyDetails['ABN'] = $value['value'];
                    }
                    else
                    {
                        $companyDetails[$value['name']] = $value['value'];
                    }
                    
                }
            
                $companyDetails = (object) $companyDetails;
            
                //stc Calculation  Formula :- STC = ROUNDDOWN(1.1*14.01*[Wt]/1000,0)
                $stc_value = Setting::give_value_of('STC_Index');
                $kw = $package->kw ;
                $stc = (int)floor( (1.1 * $stc_value * $kw)/1000  );
                
                $total_wt = $package->kw ;
    
                $jobData['customer_id'] = $customer->id;
                $jobData['offer_id'] = $offer->id;
                $jobData['package_id'] = $package->id;
                $jobData['meter_phase'] = $customer->meter_phase;
                $jobData['no_of_phases'] = $customer->number_of_phase;
                $jobData['inverter_phase'] = $customer->number_of_phase;
                $jobData['no_of_stc'] = $stc;
                $jobData['total_wt'] = $total_wt;
                $jobData['solar_capacity_installed'] = $total_wt;
                if($customer->number_of_phase == 1){
                    $jobData['solar_capacity_installed_phase_a'] = $total_wt;
                }
                if($customer->number_of_phase == 2){
                    $jobData['solar_capacity_installed_phase_a'] = $total_wt / $customer->number_of_phase;
                    $jobData['solar_capacity_installed_phase_b'] = $total_wt / $customer->number_of_phase;
                }
                if($customer->number_of_phase == 3){
                    $jobData['solar_capacity_installed_phase_a'] = $total_wt / $customer->number_of_phase;
                    $jobData['solar_capacity_installed_phase_b'] = $total_wt / $customer->number_of_phase;
                    $jobData['solar_capacity_installed_phase_c'] = $total_wt / $customer->number_of_phase;
                }
            
    
                $job =  Job::create($jobData);
                

                if($job){
                    
                   // Offer is Accepted & Job is created Add the customer invoice in Xero
                    
                   // $invoice_data =  $this->generateInvoice($offer,$customer,$package);
            
                   // if($invoice_data){
                   //     $customer->xero_account_number = $invoice_data->contact->AccountNumber ;
                   //     $customer->save();
                   //     $offer->xero_invoice_number = $invoice_data->InvoiceNumber ;
                   //     $offer->save();
                   // }
                   
                 
                    // get Package & Product details 
                    $customer_offer = CustomerOffer::where('customer_id',$job->customer_id)
                                                    ->where('offer_id',$job->offer_id)
                                                    ->where('package_id',$job->package_id)->first();


                    $package = json_decode($customer_offer->package_obj);

                    $products = json_decode($customer_offer->product_obj);
                    
                     // Set no of panels in Job
                    $panel_count = 0 ; $qty = 0 ;  $inverter_count = 0 ; $inverter_quantity = 0;  $rated_power_output = 0;
                    foreach($products as $product){
                        if($product->category->name == "Solar Panel"){
                            $panel_count = $panel_count + 1; 
                            $qty = $qty + $product->quantity; 
                        }
                        if($product->category->name == "Solar Inverter"){
                            $inverter_quantity =  $inverter_quantity + $product->quantity;
                            $rated_power_output =  $rated_power_output + $product->rated_power_output;
                        } 
                    }
            
                    if($panel_count == 0){
                        $job->update(['no_of_panels'=>"No"]);
                    }else{
                        $job->update(['no_of_panels'=>"Yes" , 'no_of_panels_value' =>  $qty ]);
                    }
                     $job->update(['solar_panel_value'=> $total_wt, 'inverter_value' => $rated_power_output ]);
                    
                    $installer = User::find($job->installer_id);

                    // Date field: Job completion date + 1 day  when- job is completed
                    $date = '';

                    // Add Job's stc form in Customer folder
                    $view_stc = view('admin.customer.pdf-STC', compact('customer','companyDetails','stc','job','installer','package','products','date','offer'))->render();


                    $stc_file_name = 'STC-'.$job->id.'.pdf';
                    $stc_path = public_path()."/documents/".$customer->id;
                    \File::makeDirectory($stc_path, $mode = 0777, true, true);
                    $stc_file= public_path()."/documents/".$customer->id."/".$stc_file_name;

                    $this->save_pdf($view_stc , $stc_file);
                    
                    $folder_stc = new Folders;
                    $folder_stc->customer_id = $customer->id;
                    $folder_stc->name = $stc_file_name;
                    $folder_stc->path = $stc_file_name;
                    $folder_stc->save();

                    // Generate PDF of Customer's Electrical Distributor & send in Mail
                    $pdfType = $customer['electrical_distributer'];

                    if($pdfType != ''  && $pdfType != null) {

                        $form_no = $pdfType.'-'.$job->id ;
                        $date = date('d-m-Y');

                        if($pdfType == 'Jemena') {
                            $view = view('admin.customer.pdf-jemna', compact('customer','companyDetails','stc','job','installer','form_no','date','package','products','offer'))->render();
                        }
                        else if($pdfType == 'SP Ausnet') {
                            $view =  view('admin.customer.pdf-sp-ausnet', compact('customer','companyDetails','stc','job','installer','form_no','date','package','products','offer'))->render();
                        }
                        else if($pdfType == 'Citipower') {
                            $view =  view('admin.customer.pdf-citipower-powercor', compact('customer','companyDetails','stc','job','installer','form_no','date' ,'package','products','offer'))->render();

                        } else if($pdfType == 'Powercor') {
                            $view =  view('admin.customer.pdf-citipower-powercor', compact('customer','companyDetails','stc','job','installer','form_no','date' ,'package','products','offer'))->render();
                        }
                        else if($pdfType == 'United Energy'){
                            //United Energy
                            $view =  view('admin.customer.pdf-UE-PR', compact('customer','companyDetails','stc','job','installer','form_no','date' ,'package','products','offer'))->render();                        
                        }else{
                            $view = "";
                        }
                        
                        if($view != ""){
                            
                        
                            $file_name = $form_no.'.pdf';
                            $path = public_path()."/documents/".$offer['customer_id'];
                            \File::makeDirectory($path, $mode = 0777, true, true);
                            $file = public_path()."/documents/".$offer['customer_id']."/".$file_name;
                        
                            $this->save_pdf($view , $file);
    
                            $folder = new Folders;
                            $folder->customer_id = $offer['customer_id'];
                            $folder->name = $file_name;
                            $folder->path = $file_name;
                            $folder->save();
                        }

                        
                        
                    }

                    $offer->update($requestData); 

                    
                    // Redirect to job's Edit Page 
                    Session::flash('flash_message', 'Offer is Accepted and a New Job is Added!');
                    return redirect('/admin/job/'.$job->id.'/edit');

                }

            }else{

                    Session::flash('flash_message', 'All the three Nearmap Images are Not Uploaded Yet. You Cannot Accept the Offer!! ');
                    return redirect()->back();
            }
                         
        }else{
            if($requestData['status'] == 'Declined'){

                 // Send Mail to Customer when offer is Declined
                if($customer->email && $sender_email && $sender_name){
                    \Mail::send('email.order.quoteDeclined', compact('customer'), function ($message) use ($customer , $sender_email,$sender_name, $cc_array, $bcc_array) {
                        $message->from($sender_email, $sender_name)
                            ->to($customer->email)
                            ->cc($cc_array)
                            ->bcc($bcc_array)
                            ->subject('Quote Declined!');                               
                    });
                }

            }
            $offer->update($requestData); 

            Session::flash('flash_message','Offer Status update Successfully!');            
            return redirect()->back();

        }  
         

    }


    public function addfolder(Request $request,$id){
        
        $customer = Party::where('id',$id)->with('folders')->first();

        if(!$customer){
            return redirect()->back();
        }

        if ($request->has('newnotes')) {
            $new_note = $request->newnotes;
            $capture_note = new Note;
            $capture_note->note = $new_note;
            $capture_note->user_id = \Auth::user()->id;
            $capture_note->customer_id = $customer->id;
            $capture_note->save();
            return redirect()->back();
        }

        $hasFileUploaded = false;
        
        if ($request->hasFile('House')) {
            $document = $request->file('House');
            
            $saveName = 'HouseUpload';
            $this->saveFolderDoc($document,$saveName,$id,$customer);
            $hasFileUploaded = true;
        }

        if ($request->hasFile('MeterBox')) {
            $document = $request->file('MeterBox');
            
            $saveName = 'MeterBoxUpload';
            $this->saveFolderDoc($document,$saveName,$id,$customer);
            $hasFileUploaded = true;
        }

        if ($request->hasFile('SwitchBox')) {
            $document = $request->file('SwitchBox');
            
            $saveName = 'SwitchBoxUpload';
            $this->saveFolderDoc($document,$saveName,$id,$customer);
            $hasFileUploaded = true;
        }

        if ($request->hasFile('NearMaps')) {
            $document = $request->file('NearMaps');
            
            $saveName = 'ElectricityBillUpload';
            $this->saveFolderDoc($document,$saveName,$id,$customer);
            $hasFileUploaded = true;
        }

        if ($request->hasFile('document')) {
            $hasFileUploaded = true;
            $documents = $request->file('document');
            foreach ($documents as $document){

                if (startsWith(strtolower($document->getClientOriginalName()), 'ces-') || startsWith(strtolower($document->getClientOriginalName()), 'sop-')  || startsWith(strtolower($document->getClientOriginalName()), 'preapproval-')) {
                    $job_pdf = $document->getClientOriginalName();

                    $find_job_pdf = Folders::where('customer_id',$customer->id)->where('path',$job_pdf)->first();
                    
                    if($find_job_pdf){
                        if($find_job_pdf->path != null){
                            $old_file = public_path()."/documents/".$job->customer_id."/".$find_job_pdf->path;
                            if (File::exists($old_file)) {
                                unlink($old_file);
                            }
                        }
                        $find_job_pdf->delete();
                    }

                    $filename = $document->getClientOriginalName();
                    $path = public_path()."/documents/".$id;
                    \File::makeDirectory($path, $mode = 0777, true, true);
                    $document->move(public_path('documents/'.$id), $filename);

                    $folder = new Folders;
                    $folder->customer_id = $id;
                    if($request->has('name')){
                        $folder->name = $request->get('name').'.'.$document->getClientOriginalExtension();
                    }
                    else{
                        $folder->name = $filename;
                    }
                    $folder->path = $filename;
                    $folder->save();
                }
                else {
                    $filename = uniqid(time()) . '.' . $document->getClientOriginalExtension();
                    $path = public_path()."/documents/".$id;
                    \File::makeDirectory($path, $mode = 0777, true, true);
                    $document->move(public_path('documents/'.$id), $filename);

                    $folder = new Folders;
                    $folder->customer_id = $id;
                    if($request->has('name')){
                        $folder->name = $request->get('name').'.'.$document->getClientOriginalExtension();
                    }
                    else{
                        $folder->name = $filename;
                    }
                    $folder->path = $filename;
                    $folder->save();
                }
                
            }    
            
        }

        if ($hasFileUploaded ) {
            return redirect()->back();
        }

        $customer_notes = Note::where('customer_id',$id)->with('user')->orderBy('created_at', 'desc')->get();

        $notes_data = "";
        $notes_last_date = "";
        foreach ($customer_notes as $note){
            if ($notes_last_date == "") {
                $notes_last_date = ' (Last Updated: '.$note->created_at.')';
            }
            $notes_data .= $note->created_at.' - '.$note->user->name.' - '.$note->note."\n";
        }
        $UploadFiles = $this->getUploadFiles($customer);
        //return $UploadFiles;
        return view('admin/customer/addfolder',compact('customer','notes_data','notes_last_date','UploadFiles'));
    }

    function getUploadFiles($customer) {
        
        $HouseUpload_docs = Folders::where('customer_id',$customer->id)->where('name','like', '%'.'HouseUpload'.'%')->first();
        $MeterBoxUpload_docs = Folders::where('customer_id',$customer->id)->where('name','like', '%'.'MeterBoxUpload'.'%')->first();
        $SwitchBoxUpload_docs = Folders::where('customer_id',$customer->id)->where('name','like', '%'.'SwitchBoxUpload'.'%')->first();
        $NearMapsUpload_docs = Folders::where('customer_id',$customer->id)->where('name','like', '%'.'ElectricityBillUpload'.'%')->first();

        $UploadFiles = array(
        'HouseUpload' => $this->returnPath($HouseUpload_docs),
        'MeterBoxUpload' => $this->returnPath($MeterBoxUpload_docs),
        'SwitchBoxUpload' => $this->returnPath($SwitchBoxUpload_docs),
        'NearMapsUpload' => $this->returnPath($NearMapsUpload_docs),
        );
        return $UploadFiles;
    }

    function returnPath($find_job_pdf) {
        if($find_job_pdf){
            if($find_job_pdf->path != null){ 
                return $find_job_pdf->path;
            }
        }
    }

    function saveFolderDoc($document,$saveName,$id,$customer) {
            $filename = uniqid(time()) . '.' . $document->getClientOriginalExtension();
            $save_Name = $saveName.'.'.$document->getClientOriginalExtension();
            $path = public_path()."/documents/".$id;
            \File::makeDirectory($path, $mode = 0777, true, true);
            $document->move(public_path('documents/'.$id), $filename);

           

            $find_job_pdf = Folders::where('customer_id',$customer->id)->where( 'name','like', '%'.$saveName.'%')->get();
            
            
            foreach ($find_job_pdf as $uploadfile){ 
                if($uploadfile){
                    if($uploadfile->path != null){
                        $old_file = public_path()."/documents/".$customer->id."/".$uploadfile->path;
                        if (File::exists($old_file)) {
                            unlink($old_file);
                        }
                    }
                    $uploadfile->delete();
                }
            }        
            

            $folder = new Folders;
            $folder->customer_id = $id;
            if($saveName){
                $folder->name = $saveName.'.'.$document->getClientOriginalExtension();
            }
            else{
                $folder->name = $filename;
            }
            $folder->path = $filename;
            $folder->save();
    }

    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 


    public function deletefolder($id){

        $folder = Folders::find($id);
        
        if(!$folder){
            Session::flash('flash_message','We Cannot found this file in our database. It may be deleted!');            
            return redirect()->back();
        }
        if($folder->path != ""){
            $file = public_path('documents/'.$folder->customer_id.'/'.$folder->path);
            if (file_exists($file)) {
                unlink($file);
            }
        }

        $folder->delete();

        Session::flash('flash_message','File Deleted Successfully!');            
        return redirect()->back();

    }
    public function rename(Request $request,$id){

        $folder = Folders::find($id);

        if($folder->path != ""){
            $file = public_path('documents/'.$folder->customer_id.'/' .$folder->path, PATHINFO_EXTENSION);
            if (file_exists($file)) {
                $ext = \File::extension($file);
            }
        }

        dd($ext);

        Session::flash('flash_message','Folder Renamed Successfully!');            
        return redirect()->back();

    }
 
    public function offerpdf($id)
    {   
        $date = date('d-m-Y');
        $offer = Offer::with('offer_detail','job')->find($id);

        //return $offer->job->package_id;

        if(!$offer){
            return redirect('/');
        }
        $stc = 0;
        if($offer->job){
            $stc = (int)$offer->job->no_of_stc ;
        }

        $customer = Party::with('sales','retailer')->find($offer->customer_id); 

        $filename = $customer->first_name."-offer-".$offer->id.".pdf";

        $company_details =  array();
        
        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;
        if($company_details){
            $company_details->STC_Price = (int)$company_details->STC_Price ;
        }

         //get offerImages
         $offer_img = OfferImage::where('offer_id',$offer->id)->get();
         $solar_panel_placement = '';
         $system_params = '';
         $daily_average = '';
         if(count($offer_img) > 0){
             foreach ($offer_img as $images ){
                 if($images->name == "solar_panel_placement" && $images->path != ''){
                     $solar_panel_placement = $images->path ;
                 }
                 if($images->name == "system_params" && $images->path != ''){
                     $system_params = $images->path ;
                 }
                 if($images->name == "daily_average" && $images->path != ''){
                     $daily_average = $images->path ;
                 }
             }
         } 

        $firstpage = $this->create_proposal_page_1($customer,$offer);
        $this->create_proposal_page_2($customer,$offer);
        $this->create_proposal_page_9($customer,$offer);
       

        $view = view('admin.customer.proposal-1',compact('offer','customer','company_details','date','stc','solar_panel_placement','system_params','daily_average','offer_img' ))->render();

       //return $view;
        $this->download_pdf($view , $filename);
       //$this->view_pdf($view , $filename);
    
    }

    public function  viewoffer($id)
    {   
        
        $offer = Offer::with('offer_detail','image','job')->find($id);
        
        $customer = Party::with('sales')->find($offer->customer_id); 

        return view('admin.customer.viewoffer',compact('offer','customer'));

    }

    public function ajaxviewoffer($id) {
        
        $offer = Offer::with('offer_detail')->find($id);
        return response()->json(['offer' => $offer],200);
    }

    public function  offers($id)
    {
        $customer = Party::with('offer')->find($id);
    
         return view('admin.customer.offerlisting',compact('customer'));

    }

    public function  offersList()
    {
        $party = Party::with('offer','folders','jobs')
        ->select( DB::raw("CONCAT(parties.first_name,' ',parties.last_name) AS customer_name"),'parties.*', 'parties.first_name','parties.last_name as last_name','users.name as sales_name')
        ->leftjoin('users','users.id','=','parties.sales_id')
        ->where('party_id','!=',0);
        
        if(\Auth::user()->roles[0]->name == "Sales"    ){
            $party->where('sales_id',\Auth::user()->id);
        }

        //if ($request->has('filter') && $request->get('filter') != '') {
       //     if($request->get('filter') == 'all'){
       //         $party = $party->where('customer_status','!=',3);    
       //     }else{
      //          $party->where('customer_status', $request->get('filter'));
      //      }
      //  }
        $party->get();

       // return Datatables::of($party)->make(true);
    
         return view('admin.customer.offerslisting',compact('party'));

    }

    public function mode(Request $request,$id){

        $job = Job::find($id);

        if($request->mode != '' || $request->mode != null){

            $job->deposit_or_finance = $request->mode ;
            $job->save();
            return response()->json(['job' => $job],200);
        }
        return redirect()->back();
    }

    public function changeSales(Request $request){

        if(!empty($request->customer_id)){

            $customer = Party::find($request->customer_id);
            $customer->sales_id = $request->sales;
            $customer->save();
            
            //find salesrep
            $sales = User::find($request->sales);

            //Email details 
            $this->assignSalesRepMail($customer,$sales); 

            return json_encode(array('msg' => 'Success'));
        }

        
        return redirect()->back();
    }


    public function assignSalesRepMail($customer,$sales) {
        $sender_name = Setting::give_value_of('Sender_Name');
        $sender_email = Setting::give_value_of('Sender_Email');
        $sender_cc = Setting::give_value_of('Sender_CC');
        $cc_array = [];
        $cc_array = explode(',', $sender_cc);
        $sender_bcc = Setting::give_value_of('Sender_BCC');
        $bcc_array = [];
        $bcc_array = explode(',', $sender_bcc);
            
        if( $sales && $sales->email != ""){
           // $cc_array[] =  $sales->email ; 
             $cc_array = explode(',',  $sales->email);
            $sender_email = $sales->email;
            $sender_name = $sales->name;
        }

         //Send Mail to Customer When new sales rep is Assigned top him
        if($customer->email && $sender_email && $sender_name){
            \Mail::send('email.order.assignedSalesRep', compact('customer'), function ($message) use ($customer , $sender_email,$sender_name, $cc_array, $bcc_array) {
                $message->from($sender_email, $sender_name)
                    ->to($customer->email)
                    ->cc($cc_array)
                    ->bcc($bcc_array)
                    ->subject('Sales Representative Assigned!');                          
            });
        }

    }


    public function ewr_pdf(Request $request){
        $filename  = "ewr.pdf";
        
        $view = view('admin.ewr-test')->render(); 
        
        if($request->pdf == 1){
            $this->view_pdf($view , $filename);
        }else{
            return $view ;
        }

       
    }
    
    public function citipower_pdf(Request $request){
        $filename  = "citipower.pdf";
        
        $view = view('admin.citipower-test')->render(); 
        
        if($request->pdf == 1){
            $this->view_pdf($view , $filename);
        }else{
            return $view ;
        }

       
    }

    public function proposal_pdf(Request $request){
        $filename  = "proposal.pdf";
        
        $view = view('admin.proposal-test')->render(); 

        if($request->pdf == 1){
            $this->view_pdf($view , $filename);
        }else{
            return $view ;
        }

        
    }
    
     public function spausnet_pdf(Request $request){
        $filename  = "spausnet.pdf";
        
        $view = view('admin.spausnet-test')->render(); 

        if($request->pdf == 1){
            $this->view_pdf($view , $filename);
        }else{
            return $view ;
        }
        
    }
    
    public function ue_pdf(Request $request){
        $filename  = "ue.pdf";
        
        $view = view('admin.ue-test')->render(); 

        if($request->pdf == 1){
            $this->view_pdf($view , $filename);
        }else{
            return $view ;
        }
        
    }
    
    public function jemna_pdf(Request $request){
        $filename  = "jemna.pdf";
        
        $view = view('admin.jemna-test')->render(); 

        if($request->pdf == 1){
            $this->view_pdf($view , $filename);
        }else{
            return $view ;
        }
        
    }

    public function nearmap($id){

        $offer_image = OfferImage::where('offer_id',$id)->get();

        $view = view('admin.customer.nearmap',compact('offer_image'))->render();

        $filename = "Nearmap-".$id.".doc";

        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=".$filename);

        return $view;

    } 

    public function upload_nearmap(Request $request,$id){

        $offer = Offer::with('image')->find($id);

        $cust_offer = CustomerOffer::where('offer_id',$id)->update(['extra_price' => $request->extra_price,'description' => $request->description,'rebates'=> $request->rebates,'loan_amount'=> $request->loan_amount]) ;
 
        if($request->submit == 'submit'){
           
             if($request->hasFile('solar_panel_placement') && $request->solar_panel_placement != '' ){
                 
                foreach( $offer->image as $images ){
                    if($images->name == "solar_panel_placement"){
                        $file = public_path('/offer/'.$offer->id.'/'.$images->path) ;
                        if( file_exists($file) ){
                            unlink($file);
                        }
                        $images->delete();
                    }
                }
                $image = $request->file('solar_panel_placement');         
                $filename =  'Solar Panel Placement-'.$offer->id.'.'.$image->getClientOriginalExtension();
                $image->move(public_path('offer/'.$offer->id), $filename);
                
                $offer_image = new OfferImage;
                $offer_image->offer_id = $offer->id;
                $offer_image->name = "solar_panel_placement";
                $offer_image->path = $filename;
                $offer_image->save();
                $count++;
            }

            if($request->hasFile('system_params') && $request->system_params != '' ){

                foreach( $offer->image as $images ){
                    if($images->name == "system_params"){
                        $file = public_path('/offer/'.$offer->id.'/'.$images->path) ;
                        if( file_exists($file) ){
                            unlink($file);
                        }
                        $images->delete();
                    }
                }

                $image = $request->file('system_params');           
                $filename =  'System Parameters-'.$offer->id.'.'.$image->getClientOriginalExtension();
                $image->move(public_path('offer/'.$offer->id), $filename);
                
                $offer_image = new OfferImage;
                $offer_image->offer_id = $offer->id;
                $offer_image->name = "system_params";
                $offer_image->path = $filename;
                $offer_image->save();
                $count++;
            }
          
            if($request->hasFile('daily_average') && $request->daily_average != '' ){

                foreach( $offer->image as $images ){
                    if($images->name == "daily_average"){
                        $file = public_path('/offer/'.$offer->id.'/'.$images->path) ;
                        if( file_exists($file) ){
                            unlink($file);
                        }
                        $images->delete();
                    }
                }

                $image = $request->file('daily_average');           
                $filename =  'Average Daily Energy Output-'.$offer->id.'.'.$image->getClientOriginalExtension();
                $image->move(public_path('offer/'.$offer->id), $filename);
                
                $offer_image = new OfferImage;
                $offer_image->offer_id = $offer->id;
                $offer_image->name = "daily_average";
                $offer_image->path = $filename;
                $offer_image->save();
                $count++;
            }


        }

        Session::flash('flash_message','Offer Updated Successfully!'); 
                   
        return redirect('/admin/viewoffer/'.$id);
    }
   
   public function generateInvoice($offer,$customer,$package)
    {   
        // get Package & Product details & Discount details 
        $customer_offer = CustomerOffer::where('customer_id',$customer->id)
                                                    ->where('offer_id',$offer->id)
                                                    ->where('package_id',$package->id)->first();
                                                    
        $package_obj = json_decode($customer_offer->package_obj);

        $product_obj = json_decode($customer_offer->product_obj);

		/* 
		 * Resolve instances of Xero, XeroInvoice, XeroContact 
		 * and XeroInvoiceLine out of the IoC container.
		 */
	    $pname = array();
        foreach( $product_obj as $products){
            $pname[] = $products->quantity.' x '.$products->name;
        }
		
		$rand = rand (10,100);
		
		$xero = 	\App::make('XeroPrivate');
		$invoice = 	\App::make('XeroInvoice');
		$contact = 	\App::make('XeroContact');
        $address =  \App::make('XeroAddress');
		$line1 = 	\App::make('XeroInvoiceLine');
		$line2 = 	\App::make('XeroInvoiceLine');
        $line3 = 	\App::make('XeroInvoiceLine');
        $line4 =    \App::make('XeroInvoiceLine');
	
		// Set up the invoice contact
		
		if($customer->xero_account_number == ""){
		    $contact->setAccountNumber('GS-'.$customer->id);
		}else{
		   $contact->setAccountNumber( $customer->xero_account_number ); 
		}
		$contact->setClean();
		$contact->setContactStatus('ACTIVE');
		$contact->setName($customer->first_name.' '.$customer->last_name);
		$contact->setFirstName($customer->first_name);
		$contact->setLastName($customer->last_name);
		$contact->setEmailAddress($customer->email);
		$contact->setDefaultCurrency('AUD');

        $address->setAddressLine1($customer->unit.' '.$customer->street_number.' '.$customer->street_name);
        $address->setCity($customer->suburb);
        $address->setRegion($customer->state);
        $address->setPostalCode($customer->post_code);
        $address->setAddressType("POBOX");
        $contact->addAddress($address);
        		
		// Assign the contact to the invoice
		$invoice->setContact($contact);

		// Set the type of invoice being created (ACCREC / ACCPAY)
		$invoice->setType('ACCREC');
		$dateInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateInstance);
		
		$invoice->setInvoiceNumber('INV-'.$offer->id);
		$invoice->setReference('INV-'.$offer->id);

        $brandingTheme = null;
        $brandingThemes = $xero->load('Accounting\\BrandingTheme')
            ->where('Name', 'Green Sky Australia')
            ->execute();
        foreach ($brandingThemes as $brandingThemeItem) {
            $brandingTheme = $brandingThemeItem;
        }

        $invoice->setBrandingThemeID($brandingTheme->BrandingThemeID);


		// Provide an URL which can be linked to from Xero to view the full order
		//$invoice->setUrl('http://yourdomain/fullpathtotheorder');

 		$invoice->setCurrencyCode('AUD');
		$invoice->setStatus('DRAFT');
		$invoice->setLineAmountType('Inclusive');

      
        
		// Create some order lines
		$line1->setDescription($package->title.'
'.implode(",\n", $pname).'
'.$customer_offer->description);

        $stc = 0;
        $stc_price = 0;

        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;
        if($company_details){
            $stc_price = (int)$company_details->STC_Price ;
        }

       
        if($offer->job){
            $stc = (int)$offer->job->no_of_stc ;
        }

        $stc_amount = round(($stc * $stc_price),0,PHP_ROUND_HALF_DOWN);
    

        $amount = $package_obj->price + $customer_offer->extra_price + $stc_amount - $customer_offer->package_discount_value;
        
        
        $line1->setTaxType('OUTPUT');
        $line1->setAccountCode('200');
		$line1->setQuantity(1);
		$line1->setUnitAmount($amount);
	    //$line1->setTaxAmount(5);
		//$line1->setDiscountRate($customer_offer->package_discount); // Percentage

		// Add the line to the order
		$invoice->addLineItem($line1);
 

        $line2->setDescription('STC Discount');
        $line2->setTaxType('EXEMPTOUTPUT');
        $line2->setAccountCode('200');
		$line2->setQuantity(1);
		$line2->setUnitAmount(-1*$stc_amount);

        $invoice->addLineItem($line2);

        if ($customer_offer->loan_amount > 0) {
            $line3->setDescription('Solar Victoria Loan');
            $line3->setTaxType('EXEMPTOUTPUT');
            $line3->setAccountCode('200');
            $line3->setQuantity(1);
            $line3->setUnitAmount(-1*$customer_offer->loan_amount);

            $invoice->addLineItem($line3);
        }

        if ($customer_offer->rebates > 0) {
            $line4->setDescription('Solar Victoria Rebate');
            $line4->setTaxType('EXEMPTOUTPUT');
            $line4->setAccountCode('200');
            $line4->setQuantity(1);
            $line4->setUnitAmount(-1*$customer_offer->rebates);

            $invoice->addLineItem($line4);
        }
        


		// Repeat for all lines...

         $xero->save($invoice);

		return $invoice ;

		
	}
}
