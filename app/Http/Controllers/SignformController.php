<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Offer;
use App\Party;
use App\User;
use App\Job;
use App\CustomerOffer;
use DB;
use App\Setting;
use XeroPrivate;

class SignformController extends Controller
{

    public function sign_form(Request $request,$id){
        
        
        $offer = Offer::with('offer_detail','image','job')->where(DB::raw('md5(id)'), $id)->first();
      
    
        if($offer){
        $customer = Party::with('sales')->find($offer->customer_id);

        $stc = 0;
        $stc_price = 0;

        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;
        if($company_details){
            $stc_price = (int)$company_details->STC_Price ;
            $stc_value = (float)$company_details->STC_Index;
        }

        if($request->has('submit') && $request->submit != ''){

          // Offer is Accepted & Job is created Add the customer invoice in Xero
          //return $stc_price.' '.$stc_value;
          $invoice_data =  $this->generateInvoice($offer,$customer ,$request->package);

          //return $invoice_data;
          //return $invoice_data;
            
           if($invoice_data){
              $customer->xero_account_number = $invoice_data->contact->AccountNumber ;
              $customer->save();
              $offer->xero_invoice_number = $invoice_data->InvoiceNumber ;
              $offer->save();
           }
           
           $offer->package_id = $request->package ;
           $offer->customer_sign = $request->sign ;
           $offer->status = 'Customer Approved' ;
           $offer->sign = 1 ;
           $offer->sign_date = date('d-m-Y');
           $offer->ip = $request->ip() ;
           $offer->latitude = $request->lat ;
           $offer->longitude = $request->long ;
           $offer->save();
           
           if($request->has('nmi') && $request->nmi != ''){
               $customer->nmi = $request->nmi ;
               $customer->save();
           }
           if($request->has('meter_number') && $request->meter_number != ''){
               $customer->meter_number = $request->meter_number ;
               $customer->save();
           }

           $sales = User::find($customer->sales_id);

           $cc_array = [];
           

           if (isset($sales->email)) {
              $to_email = $sales->email;
              $cc_array = explode(',', Setting::give_value_of('Sender_Email'));
             
           }
           else {
              $to_email = Setting::give_value_of('Sender_Email');
           }
           
           
           \Mail::send('email.order.notify-admin', compact('customer','offer'), function ($message) use ($customer,$to_email ) {
                if (isset($cc_array)) {
                  $message
                  ->to($to_email)
                  ->cc($cc_array)
                  ->subject("Offer Signed By Customer");
                }
                else {
                  $message
                  ->to($to_email)
                  ->subject("Offer Signed By Customer");
                }
                
            });
           
          
           
        }
        
        return view('email.order.sign-form',compact('offer','customer','stc_price','stc_value'));
        
        }else{
            
        }
        
        
    }

    public function generateInvoice($offer,$customer,$package)
    {   
        // get Package & Product details & Discount details 
        $customer_offer = CustomerOffer::where('customer_id',$customer->id)
                                                    ->where('offer_id',$offer->id)
                                                    ->where('package_id',$package)->first();
                                                    
        $package_obj = json_decode($customer_offer->package_obj);

        $product_obj = json_decode($customer_offer->product_obj);

    /* 
     * Resolve instances of Xero, XeroInvoice, XeroContact 
     * and XeroInvoiceLine out of the IoC container.
     */
      $pname = array();
        foreach( $product_obj as $products){
            $pname[] = $products->quantity.' x '.$products->name;
        }
    
    $rand = rand (10,100);
    
    $xero =   \App::make('XeroPrivate');
    $invoice =  \App::make('XeroInvoice');
    $contact =  \App::make('XeroContact');
    $address =  \App::make('XeroAddress');
    $line1 =  \App::make('XeroInvoiceLine');
    $line2 =  \App::make('XeroInvoiceLine');
    $line3 =  \App::make('XeroInvoiceLine');
    $line4 =    \App::make('XeroInvoiceLine');


    $existingInvoiceFound = false;

    try {
        $existingInvoice = $xero->loadByGUID('Accounting\\Invoice', 'INV-'.$offer->id);
        $existingInvoiceFound = true;
    } catch (Exception $e) {
        $existingInvoiceFound = false;
    }
    catch (\Throwable $ex) {
        $existingInvoiceFound = false;  
    }

    
    if ($existingInvoiceFound) {
        $existingInvoice->removeLineItems();
        $invoice = $existingInvoice;
    }
  
    // Set up the invoice contact
    
    if($customer->xero_account_number == ""){
        $contact->setAccountNumber('GS-'.$customer->id);
    }else{
       $contact->setAccountNumber( $customer->xero_account_number ); 
    }
    $contact->setClean();
    $contact->setContactStatus('ACTIVE');
    $contact->setName($customer->first_name.' '.$customer->last_name);
    $contact->setFirstName($customer->first_name);
    $contact->setLastName($customer->last_name);
    $contact->setEmailAddress($customer->email);
    $contact->setDefaultCurrency('AUD');

    $address->setAddressLine1($customer->unit.' '.$customer->street_number.' '.$customer->street_name);
    $address->setCity($customer->suburb);
    $address->setRegion($customer->state);
    $address->setPostalCode($customer->post_code);
    $address->setAddressType("POBOX");
    $contact->addAddress($address);
            
    // Assign the contact to the invoice
    $invoice->setContact($contact);

    // Set the type of invoice being created (ACCREC / ACCPAY)
    $invoice->setType('ACCREC');
    $dateInstance = new \DateTime();
    $invoice->setDate($dateInstance);
    $invoice->setDueDate($dateInstance);
    
    $invoice->setInvoiceNumber('INV-'.$offer->id);
    $invoice->setReference('INV-'.$offer->id);

        $brandingTheme = null;
        $brandingThemes = $xero->load('Accounting\\BrandingTheme')
            ->where('Name', 'Green Sky Australia')
            ->execute();
        foreach ($brandingThemes as $brandingThemeItem) {
            $brandingTheme = $brandingThemeItem;
        }

        $invoice->setBrandingThemeID($brandingTheme->BrandingThemeID);


    // Provide an URL which can be linked to from Xero to view the full order
    //$invoice->setUrl('http://yourdomain/fullpathtotheorder');

    $invoice->setCurrencyCode('AUD');
    $invoice->setStatus('DRAFT');
    $invoice->setLineAmountType('Inclusive');
   
    // Create some order lines
    $line1->setDescription($package_obj->title.'
'.implode(",\n", $pname).'
'.$customer_offer->description);

        $stc = 0;
        $stc_price = 0;

        $setting  = Setting::get();
        foreach($setting as $key => $value) {
            $company_details[$value['name']] = $value['value'];   
        }
        $company_details = (object) $company_details;
        if($company_details){
            $stc_price = (int)$company_details->STC_Price ;
        }

        $stc_value = Setting::give_value_of('STC_Index');
        $kw = $package_obj->kw ;
        $stc = (int)floor( (1.1 * $stc_value * $kw)/1000  );

       
        //if($offer->job){
        //    $stc = (int)$offer->job->no_of_stc ;
        //}

        $stc_amount = round(($stc * $stc_price),0,PHP_ROUND_HALF_DOWN);
    

        $amount = $package_obj->price + $customer_offer->extra_price + $stc_amount - $customer_offer->package_discount_value;
        
        
        $line1->setTaxType('OUTPUT');
        $line1->setAccountCode('200');
        $line1->setQuantity(1);
        $line1->setUnitAmount($amount);
        //$line1->setTaxAmount(5);
        //$line1->setDiscountRate($customer_offer->package_discount); // Percentage

        // Add the line to the order
        $invoice->addLineItem($line1);
 

        $line2->setDescription('STC Discount');
        $line2->setTaxType('EXEMPTOUTPUT');
        $line2->setAccountCode('200');
        $line2->setQuantity(1);
        $line2->setUnitAmount(-1*$stc_amount);

        $invoice->addLineItem($line2);

        if ($customer_offer->loan_amount > 0) {
            $line3->setDescription('Solar Victoria Loan');
            $line3->setTaxType('EXEMPTOUTPUT');
            $line3->setAccountCode('200');
            $line3->setQuantity(1);
            $line3->setUnitAmount(-1*$customer_offer->loan_amount);

            $invoice->addLineItem($line3);
        }

        if ($customer_offer->rebates > 0) {
            $line4->setDescription('Solar Victoria Rebate');
            $line4->setTaxType('EXEMPTOUTPUT');
            $line4->setAccountCode('200');
            $line4->setQuantity(1);
            $line4->setUnitAmount(-1*$customer_offer->rebates);

            $invoice->addLineItem($line4);
        }


        // Repeat for all lines...

         $xero->save($invoice);

    return $invoice ;

    
  }
    
    public function form_submit(){
        return view('email.order.form-submit');
    }
    
    public function complete_sign_form(Request $request, $id){
        
        $job = Job::where(DB::raw('md5(id)'), $id)->first();
        
        if($job){
            $customer = Party::with('sales')->find($job->customer_id);
             if($request->has('submit') && $request->submit != ''){
               
               
                $job->customer_sign = $request->sign ;
                $job->sign = 1 ;
                $job->status = "Grid forms to be sent" ;
                $job->sign_date = date('d-m-Y');
                $job->ip = $request->ip() ;
                $job->latitude = $request->lat ;
                $job->longitude = $request->long ;
                $job->save();
                
                if($request->has('nmi') && $request->nmi != ''){
                   $customer->nmi = $request->nmi ;
                   $customer->save();
               }
               if($request->has('meter_number') && $request->meter_number != ''){
                   $customer->meter_number = $request->meter_number ;
                   $customer->save();
               }
               
               return redirect('/form-submit');
            }
            
            if($job->sign == 1){
                return redirect('/form-submit');
            }
            
             return view('email.order.complete-sign-form',compact('job','customer'));
        }else{
           // return redirect()->back();
        }
        
    }
}
